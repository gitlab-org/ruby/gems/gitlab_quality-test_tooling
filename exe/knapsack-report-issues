#!/usr/bin/env ruby
# frozen_string_literal: true

require "bundler/setup"
require "optparse"

require_relative "../lib/gitlab_quality/test_tooling"

params = {}

options = OptionParser.new do |opts|
  opts.banner = "Usage: #{$PROGRAM_NAME} [options]"

  opts.on("-i", "--input-file INPUT_FILE", String, "Knapsack actual run time report file path glob") do |input_file|
    params[:input_files] = input_file
  end

  opts.on("-e", "--expected-report EXPECTED_REPORT", String, "Knapsack expected report file path") do |report_path|
    params[:expected_report] = report_path.strip
  end

  opts.on('-p', '--project PROJECT', String, 'Can be an integer or a group/project string') do |project|
    params[:project] = project
  end

  opts.on('-t', '--token TOKEN', String, 'A valid access token with `api` scope and Maintainer permission in PROJECT') do |token|
    params[:token] = token
  end

  opts.on('--dry-run', "Perform a dry-run (don't create issues)") do
    params[:dry_run] = true
  end

  opts.on_tail('-v', '--version', 'Show the version') do
    require_relative "../lib/gitlab_quality/test_tooling/version"
    puts "#{$PROGRAM_NAME} : #{GitlabQuality::TestTooling::VERSION}"
    exit
  end

  opts.on_tail('-h', '--help', 'Show the usage') do
    puts "Purpose: Create spec run time issue when a spec file almost caused job timeout because it ran significantly longer than what Knapsack expected."
    puts opts
    exit
  end

  opts.parse(ARGV)
end

if params.any?
  GitlabQuality::TestTooling::Report::KnapsackReportIssue.new(**params).invoke!
else
  puts options
  exit 1
end
