# frozen_string_literal: true

RSpec.describe GitlabQuality::TestTooling::Runtime::Env do
  around do |example|
    # Reset any already defined env variables (e.g. on CI)
    ClimateControl.modify described_class::ENV_VARIABLES.keys.zip([nil]).to_h do
      example.run
    end
  end

  shared_examples 'boolean method' do |**kwargs|
    it_behaves_like 'boolean method with parameter', kwargs
  end

  shared_examples 'boolean method with parameter' do |method:, env_key:, default:, param: nil|
    context 'when there is an env variable set' do
      it 'returns false when falsey values specified' do
        ClimateControl.modify(env_key => 'false') do
          expect(described_class.public_send(method, *param)).to be_falsey
        end

        ClimateControl.modify(env_key => 'no') do
          expect(described_class.public_send(method, *param)).to be_falsey
        end

        ClimateControl.modify(env_key => '0') do
          expect(described_class.public_send(method, *param)).to be_falsey
        end
      end

      it 'returns true when anything else specified' do
        ClimateControl.modify(env_key => 'true') do
          expect(described_class.public_send(method, *param)).to be_truthy
        end

        ClimateControl.modify(env_key => '1') do
          expect(described_class.public_send(method, *param)).to be_truthy
        end

        ClimateControl.modify(env_key => 'anything') do
          expect(described_class.public_send(method, *param)).to be_truthy
        end
      end
    end

    context 'when there is no env variable set' do
      it "returns the default, #{default}" do
        ClimateControl.modify(env_key => nil) do
          expect(described_class.public_send(method, *param)).to be(default)
        end
      end
    end
  end

  describe '.run_id' do
    around do |example|
      described_class.instance_variable_set(:@run_id, nil)
      example.run
      described_class.instance_variable_set(:@run_id, nil)
    end

    it 'returns a unique run id' do
      now = Time.now
      allow(Time).to receive(:now).and_return(now)
      allow(SecureRandom).to receive(:hex).and_return('abc123')

      expect(described_class.run_id).to eq "gitlab-qa-run-#{now.strftime('%Y-%m-%d-%H-%M-%S')}-abc123"
      expect(described_class.run_id).to eq "gitlab-qa-run-#{now.strftime('%Y-%m-%d-%H-%M-%S')}-abc123"
    end
  end

  describe '.pipeline_from_project_name' do
    it 'returns the project name' do
      ClimateControl.modify(CI_PROJECT_NAME: 'nightly') do
        expect(described_class.pipeline_from_project_name).to eq('nightly')
      end
    end

    context 'when the pipeline runs in gitlab-qa' do
      around do |example|
        ClimateControl.modify(CI_PROJECT_NAME: 'gitlab-qa') { example.run }
      end

      it 'returns default branch if the pipeline was triggered from gitlab.com' do
        ClimateControl.modify(TOP_UPSTREAM_SOURCE_JOB: 'https://gitlab.com/gitlab-org/gitlab/-/jobs/832677609') do
          expect(described_class.pipeline_from_project_name).to eq(described_class.default_branch)
        end
      end
    end

    context 'when the pipeline runs in the gitaly project' do
      around do |example|
        ClimateControl.modify(CI_PROJECT_NAME: 'gitaly') { example.run }
      end

      it 'returns the default branch name' do
        expect(described_class.pipeline_from_project_name).to eq(described_class.default_branch)
      end
    end

    context 'when the pipeline runs in a generic project' do
      around do |example|
        ClimateControl.modify(CI_PROJECT_NAME: 'foobar') { example.run }
      end

      it 'returns the project name' do
        expect(described_class.pipeline_from_project_name).to eq(ENV.fetch('CI_PROJECT_NAME'))
      end
    end
  end
end
