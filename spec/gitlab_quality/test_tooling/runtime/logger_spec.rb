# frozen_string_literal: true

RSpec.describe GitlabQuality::TestTooling::Runtime::Logger do
  let(:log_path) { '/tmp/qa-log' }
  let(:logger) { instance_double(ActiveSupport::Logger, 'formatter=': true) }

  before do
    allow(ActiveSupport::Logger).to receive(:new).and_return(logger)

    allow(GitlabQuality::TestTooling::Runtime::Env).to receive(:log_path).and_return(log_path)
    allow(FileUtils).to receive(:mkdir_p).with(log_path)
  end

  around do |example|
    described_class.instance_variable_set(:@logger, nil)
    ClimateControl.modify(QA_LOG_LEVEL: nil) { example.run }
    described_class.instance_variable_set(:@logger, nil)
  end

  it 'returns logger' do
    described_class.logger

    expect(ActiveSupport::Logger).to have_received(:new).with($stdout, level: 'INFO',
      datetime_format: described_class::TIME_FORMAT).ordered
    expect(ActiveSupport::Logger).to have_received(:new).with("#{log_path}/gitlab-qa.log", level: :debug,
      datetime_format: described_class::TIME_FORMAT).ordered
  end
end
