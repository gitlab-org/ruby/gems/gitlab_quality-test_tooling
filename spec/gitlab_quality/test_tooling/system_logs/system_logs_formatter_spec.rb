# frozen_string_literal: true

RSpec.describe GitlabQuality::TestTooling::SystemLogs::SystemLogsFormatter do
  subject { described_class.new(['foo/bar'], 'foo123') }

  let(:rails_api_log_name) { 'Rails API' }
  let(:rails_exception_log_name) { 'Rails Exceptions' }
  let(:rails_application_log_name) { 'Rails Application' }
  let(:rails_graphql_log_name) { 'Rails GraphQL' }

  let(:rails_api_log_summary) { { message: 'Test API Summary' } }
  let(:rails_exception_log_summary) { { message: 'Test Exception Summary' } }
  let(:rails_application_log_summary) { { message: 'Test Application Summary' } }
  let(:rails_graphql_log_summary) { { message: 'Test GraphQL Summary' } }

  let(:rails_api_log) do
    instance_double(
      GitlabQuality::TestTooling::SystemLogs::LogTypes::Rails::ApiLog,
      name: rails_api_log_name,
      summary: rails_api_log_summary
    )
  end

  let(:rails_api_log_finder) do
    instance_double(
      GitlabQuality::TestTooling::SystemLogs::Finders::Rails::ApiLogFinder,
      find: found_api_logs
    )
  end

  let(:rails_exception_log) do
    instance_double(
      GitlabQuality::TestTooling::SystemLogs::LogTypes::Rails::ExceptionLog,
      name: rails_exception_log_name,
      summary: rails_exception_log_summary
    )
  end

  let(:rails_exception_log_finder) do
    instance_double(
      GitlabQuality::TestTooling::SystemLogs::Finders::Rails::ExceptionLogFinder,
      find: found_exception_logs
    )
  end

  let(:rails_application_log) do
    instance_double(
      GitlabQuality::TestTooling::SystemLogs::LogTypes::Rails::ApplicationLog,
      name: rails_application_log_name,
      summary: rails_application_log_summary
    )
  end

  let(:rails_application_log_finder) do
    instance_double(
      GitlabQuality::TestTooling::SystemLogs::Finders::Rails::ApplicationLogFinder,
      find: found_application_logs
    )
  end

  let(:rails_graphql_log) do
    instance_double(
      GitlabQuality::TestTooling::SystemLogs::LogTypes::Rails::GraphqlLog,
      name: rails_graphql_log_name,
      summary: rails_graphql_log_summary
    )
  end

  let(:rails_graphql_log_finder) do
    instance_double(
      GitlabQuality::TestTooling::SystemLogs::Finders::Rails::GraphqlLogFinder,
      find: found_graphql_logs
    )
  end

  before do
    allow(GitlabQuality::TestTooling::SystemLogs::LogTypes::Rails::ApiLog).to receive(:new).and_return(rails_api_log)
    allow(GitlabQuality::TestTooling::SystemLogs::Finders::Rails::ApiLogFinder).to receive(:new).and_return(rails_api_log_finder)

    allow(GitlabQuality::TestTooling::SystemLogs::LogTypes::Rails::ExceptionLog).to receive(:new).and_return(rails_exception_log)
    allow(GitlabQuality::TestTooling::SystemLogs::Finders::Rails::ExceptionLogFinder).to receive(:new).and_return(rails_exception_log_finder)

    allow(GitlabQuality::TestTooling::SystemLogs::LogTypes::Rails::ApplicationLog).to receive(:new).and_return(rails_application_log)
    allow(GitlabQuality::TestTooling::SystemLogs::Finders::Rails::ApplicationLogFinder).to receive(:new).and_return(rails_application_log_finder)

    allow(GitlabQuality::TestTooling::SystemLogs::LogTypes::Rails::GraphqlLog).to receive(:new).and_return(rails_graphql_log)
    allow(GitlabQuality::TestTooling::SystemLogs::Finders::Rails::GraphqlLogFinder).to receive(:new).and_return(rails_graphql_log_finder)
  end

  describe '#system_logs_summary_markdown' do
    shared_examples 'returns the expected summary markdown' do
      it 'returns the expected summary markdown' do
        expect(subject.system_logs_summary_markdown).to eq(expected_summary_markdown)
      end
    end

    context 'when all log finders return logs' do
      let(:found_api_logs) { [rails_api_log] }
      let(:found_exception_logs) { [rails_exception_log] }
      let(:found_application_logs) { [rails_application_log] }
      let(:found_graphql_logs) { [rails_graphql_log] }

      let(:expected_summary_markdown) do
        <<~MARKDOWN.chomp
          ### System Logs

          #### #{rails_api_log_name}
          <details><summary>Click to expand</summary>

          ```json
          #{JSON.pretty_generate(rails_api_log_summary)}
          ```
          </details>

          #### #{rails_exception_log_name}
          <details><summary>Click to expand</summary>

          ```json
          #{JSON.pretty_generate(rails_exception_log_summary)}
          ```
          </details>

          #### #{rails_application_log_name}
          <details><summary>Click to expand</summary>

          ```json
          #{JSON.pretty_generate(rails_application_log_summary)}
          ```
          </details>

          #### #{rails_graphql_log_name}
          <details><summary>Click to expand</summary>

          ```json
          #{JSON.pretty_generate(rails_graphql_log_summary)}
          ```
          </details>
        MARKDOWN
      end

      it_behaves_like 'returns the expected summary markdown'
    end

    context 'when all log finders return no logs' do
      let(:found_api_logs) { [] }
      let(:found_exception_logs) { [] }
      let(:found_application_logs) { [] }
      let(:found_graphql_logs) { [] }

      let(:expected_summary_markdown) { '' }

      it_behaves_like 'returns the expected summary markdown'
    end

    context 'when only some log finders return logs' do
      let(:found_api_logs) { [rails_api_log] }
      let(:found_exception_logs) { [] }
      let(:found_application_logs) { [rails_application_log] }
      let(:found_graphql_logs) { [] }

      let(:expected_summary_markdown) do
        <<~MARKDOWN.chomp
          ### System Logs

          #### #{rails_api_log_name}
          <details><summary>Click to expand</summary>

          ```json
          #{JSON.pretty_generate(rails_api_log_summary)}
          ```
          </details>


          #### #{rails_application_log_name}
          <details><summary>Click to expand</summary>

          ```json
          #{JSON.pretty_generate(rails_application_log_summary)}
          ```
          </details>
        MARKDOWN
      end

      it_behaves_like 'returns the expected summary markdown'
    end
  end
end
