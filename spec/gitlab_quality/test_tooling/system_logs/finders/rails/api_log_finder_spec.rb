# frozen_string_literal: true

require_relative '../../shared_examples/log_finder_shared_examples'

RSpec.describe GitlabQuality::TestTooling::SystemLogs::Finders::Rails::ApiLogFinder do
  let(:expected_log_name) { 'Rails API' }
  let(:expected_log_type) { GitlabQuality::TestTooling::SystemLogs::LogTypes::Rails::ApiLog }
  let(:log_data_file_name) do
    'spec/gitlab_quality/test_tooling/system_logs/fixtures/logs/gitlab-rails/api_log_data.json'
  end

  describe '#find' do
    it_behaves_like 'log finder'
  end
end
