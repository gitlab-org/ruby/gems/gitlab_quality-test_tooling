# frozen_string_literal: true

RSpec.shared_examples 'no logs found' do
  it 'returns no results' do
    expect(found_logs).to be_empty
  end
end

RSpec.shared_examples 'invalid correlation id' do
  context 'when correlation id is not provided' do
    let(:correlation_id) { nil }

    it_behaves_like 'no logs found'
  end

  context 'when correlation id does not exist in the logs' do
    let(:correlation_id) { 'foo456' }

    it_behaves_like 'no logs found'
  end
end

RSpec.shared_examples 'log finder' do
  subject { described_class.new(base_path) }

  let(:found_logs) { subject.find(correlation_id) }
  let(:expected_log_data) do
    JSON.parse(
      File.read(log_data_file_name),
      symbolize_names: true
    )
  end

  context 'when log files exist' do
    let(:base_path) { 'spec/gitlab_quality/test_tooling/system_logs/fixtures/logs' }

    it_behaves_like 'invalid correlation id'

    context 'when correlation id exists in the logs' do
      let(:correlation_id) { 'foo123' }

      it 'returns the corresponding log' do
        expect(found_logs.length).to eq(1)
        expect(found_logs.first).to be_instance_of(expected_log_type)
        expect(found_logs.first).to have_attributes(
          name: expected_log_name,
          data: expected_log_data
        )
      end
    end
  end

  context 'when log files do not exist' do
    let(:base_path) { 'foo/bar' }

    it_behaves_like 'invalid correlation id'
  end
end
