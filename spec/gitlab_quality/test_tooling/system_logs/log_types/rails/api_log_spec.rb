# frozen_string_literal: true

require_relative '../../shared_examples/log_type_shared_examples'

RSpec.describe GitlabQuality::TestTooling::SystemLogs::LogTypes::Rails::ApiLog do
  let(:log_data_file_name) do
    'spec/gitlab_quality/test_tooling/system_logs/fixtures/logs/gitlab-rails/api_log_data.json'
  end

  let(:expected_summary) do
    {
      severity: "INFO",
      correlation_id: "foo123",
      time: "2023-01-25T16:22:14.322Z",
      method: "POST",
      path: "/api/v4/foo/bar",
      status: 400,
      params: [
        { key: "private_token", value: "[FILTERED]" },
        { key: "key", value: "TEST_FILE" },
        { key: "value", value: "[FILTERED]" },
        { key: "masked", value: "false" },
        { key: "protected", value: "false" },
        { key: "variable_type", value: "file" }
      ],
      api_error: ["{\"message\":{\"value\":[\"is invalid\"]}}"],
      exception_class: "FooBarClass",
      exception_message: "Value is invalid",
      exception_backtrace: [
        "foo.rb:in `foo_method'",
        "foo.rb:43:in `block (2 levels) in \u003cmodule:FooModule\u003e",
        "bar.rb:16:in `bar_method'"
      ],
      meta_user: "root",
      meta_project: "gitlab-qa-sandbox-group/foo-group/bar-project",
      meta_caller_id: "POST /api/:version/foo/bar"
    }
  end

  describe '#summary' do
    it_behaves_like 'log summary'
  end
end
