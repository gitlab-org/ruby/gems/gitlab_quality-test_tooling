# frozen_string_literal: true

require_relative '../../shared_examples/log_type_shared_examples'

RSpec.describe GitlabQuality::TestTooling::SystemLogs::LogTypes::Rails::GraphqlLog do
  let(:log_data_file_name) do
    'spec/gitlab_quality/test_tooling/system_logs/fixtures/logs/gitlab-rails/graphql_log_data.json'
  end

  let(:expected_summary) do
    {
      severity: "INFO",
      time: "2023-01-25T15:57:03.957Z",
      correlation_id: "foo123",
      operation_name: "fooOperation",
      query_string: "query fooOperation($projectPath: ID!) {\n  project(fullPath: $projectPath) { \n __typename\n foo\n  bar {\n  __typename\n  foobar \n} \n}\n",
      variables: "{\"projectPath\"=\u003e\"gitlab-qa-sandbox-group/foo-group/bar-project\"}",
      meta_caller_id: "graphql:fooOperation",
      meta_user: "root"
    }
  end

  describe '#summary' do
    it_behaves_like 'log summary'
  end
end
