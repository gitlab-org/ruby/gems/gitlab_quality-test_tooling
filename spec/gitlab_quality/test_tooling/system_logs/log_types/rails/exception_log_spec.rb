# frozen_string_literal: true

require_relative '../../shared_examples/log_type_shared_examples'

RSpec.describe GitlabQuality::TestTooling::SystemLogs::LogTypes::Rails::ExceptionLog do
  let(:log_data_file_name) do
    'spec/gitlab_quality/test_tooling/system_logs/fixtures/logs/gitlab-rails/exception_log_data.json'
  end

  let(:expected_summary) do
    {
      severity: "ERROR",
      time: "2023-01-25T15:50:19.944Z",
      correlation_id: "foo123",
      exception_class: "FooClass",
      exception_message: "exceptions_json.log foo123 test exception message",
      exception_backtrace: [
        "foo.rb:in `foo_method'",
        "foo.rb:17:in `block (2 levels) in \u003cmodule:FooModule\u003e",
        "bar.rb:16:in `bar_method'"
      ]
    }
  end

  describe '#summary' do
    it_behaves_like 'log summary'
  end
end
