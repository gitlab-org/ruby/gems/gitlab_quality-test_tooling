# frozen_string_literal: true

RSpec.describe GitlabQuality::TestTooling::TestMetrics::JsonTestMetricCollection do
  subject(:json_test_metrics) { described_class.new(path) }

  let(:path) { 'path.json' }
  let(:metrics) do
    <<~JSON
      [
        {
          "name": "test-stats 1",
          "time": "2023-11-17T21:56:20.000+00:00",
          "tags": {
            "name": "Test Name",
            "file_path": "/path/to/test/file1.rb",
            "status": "failed",
            "smoke": "true",
            "reliable": "false",
            "quarantined": "false",
            "retried": "true",
            "job_name": "qa-browser_ui-8_monitor",
            "merge_request": "false",
            "run_type": "staging-canary-full",
            "stage": "monitor",
            "product_group": "respond",
            "testcase": "https://gitlab.com/gitlab-org/gitlab/-/quality/test_cases/123"
          },
          "fields": {
            "id": "./path/to/test/file1.rb[1:1:1:1:1]",
            "run_time": 201859,
            "api_fabrication": 0,
            "ui_fabrication": 0,
            "total_fabrication": 0,
            "retry_attempts": 2,
            "job_url": "https://ops.gitlab.net/gitlab-org/quality/staging-canary/-/jobs/123",
            "pipeline_url": "https://ops.gitlab.net/gitlab-org/quality/staging-canary/-/pipelines/456",
            "pipeline_id": null,
            "job_id": null,
            "merge_request_iid": null,
            "failure_exception": "navbar did not appear on QA::Page::Main::Menu as expected"
          }
        },
        {
          "name": "test-stats 2",
          "time": "2023-11-17T21:56:20.000+00:00",
          "tags": {
            "name": "Test Name",
            "file_path": "/path/to/test/file2.rb",
            "status": "failed",
            "smoke": "true",
            "reliable": "false",
            "quarantined": "false",
            "retried": "true",
            "job_name": "qa-browser_ui-8_monitor",
            "merge_request": "false",
            "run_type": "staging-canary-full",
            "stage": "monitor",
            "product_group": "respond",
            "testcase": "https://gitlab.com/gitlab-org/gitlab/-/quality/test_cases/124"
          },
          "fields": {
            "id": "./path/to/test/file.rb2[1:1:1:1:1]",
            "run_time": 201859,
            "api_fabrication": 0,
            "ui_fabrication": 0,
            "total_fabrication": 0,
            "retry_attempts": 2,
            "job_url": "https://ops.gitlab.net/gitlab-org/quality/staging-canary/-/jobs/124",
            "pipeline_url": "https://ops.gitlab.net/gitlab-org/quality/staging-canary/-/pipelines/457",
            "pipeline_id": null,
            "job_id": null,
            "merge_request_iid": null,
            "failure_exception": ""
          }
        }
      ]
    JSON
  end

  before do
    allow(File).to receive(:read).with(path).and_return(metrics)
  end

  describe '#write' do
    it 'writes to path' do
      expect(File).to receive(:write).with(path, metrics.strip)

      json_test_metrics.write
    end
  end

  describe '#metric_for_test_id' do
    it 'returns metric when found' do
      expect(json_test_metrics.metric_for_test_id('./path/to/test/file1.rb[1:1:1:1:1]').name)
        .to eq('test-stats 1')
    end

    it 'returns nil when not found' do
      expect(json_test_metrics.metric_for_test_id('./non/existent')).to be_nil
    end
  end
end
