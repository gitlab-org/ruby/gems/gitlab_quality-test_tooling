# frozen_string_literal: true

RSpec.describe GitlabQuality::TestTooling::Slack::PostToSlack do
  let(:default_attrs) do
    {
      slack_webhook_url: 'slack_webhook_url',
      channel: 'channel',
      message: 'message',
      username: 'username',
      icon_emoji: 'icon_emoji'
    }
  end

  describe '#invoke!' do
    %i[slack_webhook_url channel message username icon_emoji].each do |attr|
      it "requires #{attr}" do
        expect do
          described_class.new(**default_attrs.select { |key, _| key != attr })
        end.to raise_error(ArgumentError, "missing keyword: :#{attr}")
      end
    end

    it 'ends a message to Slack' do
      ClimateControl.modify(SLACK_QA_CHANNEL: 'abc', CI_SLACK_WEBHOOK_URL: 'def') do
        expect(GitlabQuality::TestTooling::Support::HttpRequest)
          .to receive(:make_http_request)
          .with(
            method: 'post',
            url: default_attrs[:slack_webhook_url],
            params: {
              'channel' => default_attrs[:channel],
              'username' => default_attrs[:username],
              'icon_emoji' => default_attrs[:icon_emoji],
              'text' => default_attrs[:message]
            },
            show_response: true
          )

        described_class.new(**default_attrs).invoke!
      end
    end
  end
end
