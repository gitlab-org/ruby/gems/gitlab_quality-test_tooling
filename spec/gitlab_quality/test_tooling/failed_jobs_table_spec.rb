# frozen_string_literal: true

RSpec.describe GitlabQuality::TestTooling::FailedJobsTable do
  subject(:table) { described_class.create(jobs: jobs) }

  let(:jobs) do
    [
      Gitlab::ObjectifiedHash.new(name: "rspec:other", stage: "test", failure_reason: "script_failure"),
      Gitlab::ObjectifiedHash.new(name: "_Quarantine", stage: "test", failure_reason: "script_failure")
    ]
  end

  it "returns a formatted table" do
    expect(table).to eq(<<~TABLE)
      ```
      JOB         | STAGE | FAILURE REASON
      ------------|-------|---------------
      rspec:other | test  | script_failure
      ```
    TABLE
  end
end
