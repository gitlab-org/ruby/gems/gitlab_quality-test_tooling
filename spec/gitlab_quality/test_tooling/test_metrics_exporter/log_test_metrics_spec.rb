# frozen_string_literal: true

require "influxdb-client"

RSpec.describe GitlabQuality::TestTooling::TestMetricsExporter::LogTestMetrics do
  include StubENV

  let(:influx_client) { instance_double(InfluxDB2::Client, create_write_api: influx_write_api) }
  let(:influx_write_api) { instance_double(InfluxDB2::WriteApi, write: nil) }

  let(:influxdb_url) { "http://influxdb.net" }
  let(:influxdb_token) { "token" }
  let(:influxdb_org) { "gitlab-qa" }
  let(:influxdb_bucket) { "e2e-test-stats" }

  let(:gcs_client) { double("Fog::Storage::GoogleJSON::Real", put_object: nil) } # rubocop:disable RSpec/VerifiedDoubles -- Class has `put_object` method but is not getting verified
  let(:gcs_client_options) { { force: true, content_type: 'application/json' } }

  let(:gcs_project_id) { "gcs-project-id" }
  let(:gcs_bucket) { "gcs-bucket" }
  let(:gcs_credentials) { "gcs-credentials" }
  let(:gcs_metrics_file_name) { "metrics.json" }

  let(:ci_timestamp) { "2021-02-23T20:58:41Z" }
  let(:ci_job_name) { "test-job" }
  let(:ci_job_url) { "https://url" }
  let(:ci_pipeline_url) { "url" }
  let(:ci_pipeline_id) { "123" }
  let(:ci_job_id) { "321" }

  let(:id) { "./qa/specs/features/1_manage/subfolder/some_spec.rb[1:2:1]" }
  let(:full_description) { "spec full description" }
  let(:file_path) { "./qa/specs/features/1_manage/subfolder/some_spec.rb" }
  let(:quarantine) { { only: { job: 'praefect' } } }
  let(:feature_category) { "fulfillment_infrastructure" }
  let(:product_group) { "fulfillment_platform" }
  let(:status) { :passed }
  let(:run_time) { 17.0019 }
  let(:exception) { nil }
  let(:execution_result) do
    instance_double(RSpec::Core::Example::ExecutionResult,
      status: status,
      exception: exception,
      run_time: run_time
    )
  end

  let(:time) { Time.parse(Time.strptime(ci_timestamp, '%Y-%m-%dT%H:%M:%S%z').strftime('%Y-%m-%dT%H:%M:%S%z')) }
  let(:quarantined) { "false" }
  let(:job_name) { "test-job" }
  let(:merge_request) { "false" }
  let(:run_type) { "staging-full" }
  let(:failure_exception) { "" }
  let(:custom_keys_tags) { [] }
  let(:custom_keys_fields) { [] }

  let(:influx_client_args) do
    {
      bucket: influxdb_bucket,
      org: influxdb_org,
      precision: InfluxDB2::WritePrecision::NANOSECOND
    }
  end

  let(:examples) do
    [
      instance_double(RSpec::Core::Example,
        id: id,
        full_description: full_description,
        metadata: {
          file_path: file_path,
          quarantine: quarantine,
          feature_category: feature_category,
          product_group: product_group
        },
        execution_result: execution_result
      )
    ]
  end

  let(:expected_data) do
    {
      name: "test-stats",
      time: time,
      tags: {
        name: full_description,
        file_path: file_path.sub(/\A./, ''),
        status: status,
        quarantined: quarantined,
        job_name: job_name,
        merge_request: merge_request,
        run_type: run_type,
        feature_category: feature_category,
        product_group: product_group
      },
      fields: {
        id: id,
        run_time: (run_time * 1000).round,
        job_url: ci_job_url,
        pipeline_url: ci_pipeline_url,
        pipeline_id: ci_pipeline_id,
        job_id: ci_job_id,
        failure_exception: failure_exception
      }
    }
  end

  subject do
    described_class.new(
      examples: examples,
      run_type: run_type
    )
  end

  before do
    allow(InfluxDB2::Client).to receive(:new).with(influxdb_url, influxdb_token, **influx_client_args) { influx_client }
    allow(Fog::Storage::Google).to receive(:new)
      .with(google_project: gcs_project_id,
        google_json_key_string: gcs_credentials)
      .and_return(gcs_client)

    stub_env("CI_PIPELINE_CREATED_AT", ci_timestamp)
    stub_env("CI_JOB_URL", ci_job_url)
    stub_env("CI_JOB_NAME", ci_job_name)
    stub_env("CI_PIPELINE_URL", ci_pipeline_url)
    stub_env("CI_PIPELINE_ID", ci_pipeline_id)
    stub_env("CI_JOB_ID", ci_job_id)
    stub_env("CI_MERGE_REQUEST_IID", nil)
    stub_env("TOP_UPSTREAM_MERGE_REQUEST_IID", nil)

    subject.configure_influxdb_client(
      influxdb_url: influxdb_url,
      influxdb_token: influxdb_token,
      influxdb_bucket: influxdb_bucket
    )

    subject.configure_gcs_client(
      gcs_bucket: gcs_bucket,
      gcs_project_id: gcs_project_id,
      gcs_credentials: gcs_credentials,
      gcs_metrics_file_name: gcs_metrics_file_name
    )
  end

  shared_examples "successful push to InfluxDB and GCS" do |description:, custom_tags: false, custom_fields: false|
    it description do
      aggregate_failures do
        expect(influx_write_api).to receive(:write).with(data: [expected_data])
        expect(gcs_client).to receive(:put_object).with(gcs_bucket, gcs_metrics_file_name, [expected_data].to_json,
          **gcs_client_options)
      end

      custom_keys = {}
      custom_keys[:custom_keys_tags] = custom_keys_tags if custom_tags
      custom_keys[:custom_keys_fields] = custom_keys_fields if custom_fields

      subject.push_test_metrics(**custom_keys)
    end
  end

  shared_context "when example contains custom keys" do
    let(:examples) do
      [
        instance_double(RSpec::Core::Example,
          id: id,
          full_description: full_description,
          metadata: {
            file_path: file_path,
            quarantine: quarantine,
            feature_category: feature_category,
            product_group: product_group,
            blocking: "true",
            do_not_include: "false"
          },
          execution_result: execution_result
        )
      ]
    end
  end

  shared_context "when the expected data contains custom keys in tags" do
    let(:custom_keys_tags) { %w[blocking] }
    let(:expected_data) do
      {
        name: "test-stats",
        time: time,
        tags: {
          name: full_description,
          file_path: file_path.sub(/\A./, ''),
          status: status,
          quarantined: quarantined,
          job_name: job_name,
          merge_request: merge_request,
          run_type: run_type,
          feature_category: feature_category,
          product_group: product_group,
          blocking: "true"
        },
        fields: {
          id: id,
          run_time: (run_time * 1000).round,
          job_url: ci_job_url,
          pipeline_url: ci_pipeline_url,
          pipeline_id: ci_pipeline_id,
          job_id: ci_job_id,
          failure_exception: failure_exception
        }
      }
    end
  end

  shared_context "when the expected data contains custom keys in fields" do
    let(:custom_keys_fields) { %w[blocking] }
    let(:expected_data) do
      {
        name: "test-stats",
        time: time,
        tags: {
          name: full_description,
          file_path: file_path.sub(/\A./, ''),
          status: status,
          quarantined: quarantined,
          job_name: job_name,
          merge_request: merge_request,
          run_type: run_type,
          feature_category: feature_category,
          product_group: product_group
        },
        fields: {
          id: id,
          run_time: (run_time * 1000).round,
          job_url: ci_job_url,
          pipeline_url: ci_pipeline_url,
          pipeline_id: ci_pipeline_id,
          job_id: ci_job_id,
          failure_exception: failure_exception,
          blocking: "true"
        }
      }
    end
  end

  context "when no influxdb_url is provided" do
    let(:influxdb_url) { nil }

    it "does not push metrics to InfluxDB" do
      expect(influx_write_api).not_to receive(:write)

      subject.push_test_metrics
    end
  end

  context "when no influxdb_token is provided" do
    let(:influxdb_token) { nil }

    it "does not push metrics to InfluxDB" do
      expect(influx_write_api).not_to receive(:write)

      subject.push_test_metrics
    end
  end

  context "when no influxdb_bucket is provided" do
    let(:influxdb_bucket) { nil }

    it "does not push metrics to InfluxDB" do
      expect(influx_write_api).not_to receive(:write)

      subject.push_test_metrics
    end
  end

  context "when no gcs_project_id is provided" do
    let(:gcs_project_id) { nil }

    it "does not push metrics to GCS" do
      expect(gcs_client).not_to receive(:put_object)

      subject.push_test_metrics
    end
  end

  context "when no gcs_credentials is provided" do
    let(:gcs_credentials) { nil }

    it "does not push metrics to GCS" do
      expect(gcs_client).not_to receive(:put_object)

      subject.push_test_metrics
    end
  end

  context "when no gcs_bucket is provided" do
    let(:gcs_bucket) { nil }

    it "does not push metrics to GCS" do
      expect(gcs_client).not_to receive(:put_object)

      subject.push_test_metrics
    end
  end

  describe "#push_test_metrics" do
    shared_context "when spec status is failed" do
      let(:status) { :failed }
      let(:exception) { instance_double(RSpec::Expectations::ExpectationNotMetError) }
      let(:exception_class) { "RSpec::Expectations::ExpectationNotMetError" }
      let(:failure_exception) { exception.to_s }
      let(:expected_data) do
        {
          name: "test-stats",
          time: time,
          tags: {
            name: full_description,
            file_path: file_path.sub(/\A./, ''),
            status: status,
            quarantined: quarantined,
            job_name: job_name,
            merge_request: merge_request,
            run_type: run_type,
            feature_category: feature_category,
            product_group: product_group,
            exception_class: exception_class
          },
          fields: {
            id: id,
            run_time: (run_time * 1000).round,
            job_url: ci_job_url,
            pipeline_url: ci_pipeline_url,
            pipeline_id: ci_pipeline_id,
            job_id: ci_job_id,
            failure_exception: failure_exception
          }
        }
      end

      before do
        allow(exception).to receive(:class).and_return(RSpec::Expectations::ExpectationNotMetError)
      end
    end

    shared_context "when spec status is pending" do
      let(:status) { :pending }
      let(:quarantined) { "true" }
    end

    context "when status is :pending" do
      include_context "when spec status is pending"

      it_behaves_like "successful push to InfluxDB and GCS", description: "sets status to :pending"
    end

    context "when status is :failed" do
      include_context "when spec status is failed"

      it_behaves_like "successful push to InfluxDB and GCS", description: "sets status to :failed"
    end

    context "when status is passed" do
      context "when retry_attempts is 0" do
        it_behaves_like "successful push to InfluxDB and GCS", description: "considers the spec passed"
      end

      context "when retry_attempts is > 0" do
        let(:status) { :flaky }
        let(:examples) do
          [
            instance_double(RSpec::Core::Example,
              id: id,
              full_description: full_description,
              metadata: {
                file_path: file_path,
                quarantine: quarantine,
                feature_category: feature_category,
                product_group: product_group,
                retry_attempts: 1
              },
              execution_result: execution_result
            )
          ]
        end

        it_behaves_like "successful push to InfluxDB and GCS", description: "considers the spec flaky"
      end
    end

    context "when spec is quarantined" do
      context "when status is :pending" do
        include_context "when spec status is pending"

        it_behaves_like "successful push to InfluxDB and GCS", description: "considers the spec quarantined"
      end
    end

    context "when ci_job_name contains parallelization suffix" do
      before do
        stub_env("CI_JOB_NAME", ci_job_name)
      end

      let(:ci_job_name) { "test-job 1/5" }

      it_behaves_like "successful push to InfluxDB and GCS", description: "strips the trailing parallelization suffix"
    end

    context "when spec is ran from a merge request" do
      let(:merge_request) { "true" }
      let(:merge_request_iid) { "123" }
      let(:expected_data) do
        {
          name: "test-stats",
          time: time,
          tags: {
            name: full_description,
            file_path: file_path.sub(/\A./, ''),
            status: status,
            quarantined: quarantined,
            job_name: job_name,
            merge_request: merge_request,
            run_type: run_type,
            feature_category: feature_category,
            product_group: product_group
          },
          fields: {
            id: id,
            run_time: (run_time * 1000).round,
            job_url: ci_job_url,
            pipeline_url: ci_pipeline_url,
            pipeline_id: ci_pipeline_id,
            job_id: ci_job_id,
            merge_request_iid: merge_request_iid,
            failure_exception: failure_exception
          }
        }
      end

      context "when CI_MERGE_REQUEST_IID is set" do
        before do
          stub_env("CI_MERGE_REQUEST_IID", merge_request_iid)
        end

        it_behaves_like "successful push to InfluxDB and GCS",
          description: "sets :merge_request to 'true' and merge_request_iid"
      end

      context "when TOP_UPSTREAM_MERGE_REQUEST_IID is set" do
        before do
          stub_env("TOP_UPSTREAM_MERGE_REQUEST_IID", 456)
        end

        let(:merge_request_iid) { "456" }

        it_behaves_like "successful push to InfluxDB and GCS",
          description: "sets :merge_request to 'true' and merge_request_iid"
      end
    end

    context "when example contains feature_category" do
      it_behaves_like "successful push to InfluxDB and GCS", description: "uses feature_category value"
    end

    context "when example contains an exception" do
      include_context "when spec status is failed"

      it_behaves_like "successful push to InfluxDB and GCS", description: "sets exception_class"
    end

    context "when example does not contain an exception" do
      it_behaves_like "successful push to InfluxDB and GCS", description: "does not set exception_class"
    end

    context "when spec has custom_metrics for tags" do
      include_context "when example contains custom keys"
      include_context "when the expected data contains custom keys in tags"

      it_behaves_like "successful push to InfluxDB and GCS",
        description: "sets the additional metrics specified by custom_keys_tags",
        custom_tags: true
    end

    context "when spec has custom_metrics for fields" do
      include_context "when example contains custom keys"
      include_context "when the expected data contains custom keys in fields"

      it_behaves_like "successful push to InfluxDB and GCS",
        description: "sets the additional metrics specified by custom_keys_fields",
        custom_fields: true
    end

    it_behaves_like "successful push to InfluxDB and GCS",
      description: "multiplies run_time by 1000 and rounds the result"
  end

  describe "#save_test_metrics" do
    let(:save_test_metrics_file) { 'test_metrics.json' }

    context "when no file_name is provided" do
      let(:save_test_metrics_file) { nil }

      it "does not save test metrics to a JSON file" do
        expect(File).not_to receive(:write)
      end
    end

    it "saves test metrics into a json file" do
      expect(File).to receive(:write).with("tmp/#{save_test_metrics_file}", [expected_data].to_json)

      subject.save_test_metrics(file_name: save_test_metrics_file)
    end

    context "when custom_keys_tags is specified" do
      include_context "when example contains custom keys"
      include_context "when the expected data contains custom keys in tags"

      it "includes custom_keys_tags in the json file" do
        expect(File).to receive(:write).with("tmp/#{save_test_metrics_file}", [expected_data].to_json)

        subject.save_test_metrics(file_name: save_test_metrics_file, custom_keys_tags: custom_keys_tags)
      end
    end

    context "when custom_keys_fields is specified" do
      include_context "when example contains custom keys"
      include_context "when the expected data contains custom keys in fields"

      it "includes custom_keys_tags in the json file" do
        expect(File).to receive(:write).with("tmp/#{save_test_metrics_file}", [expected_data].to_json)

        subject.save_test_metrics(file_name: save_test_metrics_file, custom_keys_fields: custom_keys_fields)
      end
    end
  end
end
