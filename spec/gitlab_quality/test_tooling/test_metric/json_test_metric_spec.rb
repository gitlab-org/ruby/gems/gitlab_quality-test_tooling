# frozen_string_literal: true

RSpec.describe GitlabQuality::TestTooling::TestMetric::JsonTestMetric do
  subject(:test_metric) { described_class.new(JSON.parse(metric)) }

  let(:metric) do
    <<~JSON
      {
        "name": "test-stats",
        "time": "2023-11-17T21:56:20.000+00:00",
        "tags": {
          "name": "Test Name",
          "file_path": "/path/to/test/file.rb",
          "status": "failed",
          "smoke": "true",
          "reliable": "false",
          "quarantined": "false",
          "retried": "true",
          "job_name": "qa-browser_ui-8_monitor",
          "merge_request": "false",
          "run_type": "staging-canary-full",
          "stage": "monitor",
          "product_group": "respond",
          "testcase": "https://gitlab.com/gitlab-org/gitlab/-/quality/test_cases/123"
        },
        "fields": {
          "id": ".//path/to/test/file.rb[1:1:1:1:1]",
          "run_time": 201859,
          "api_fabrication": 0,
          "ui_fabrication": 0,
          "total_fabrication": 0,
          "retry_attempts": 2,
          "job_url": "https://ops.gitlab.net/gitlab-org/quality/staging-canary/-/jobs/123",
          "pipeline_url": "https://ops.gitlab.net/gitlab-org/quality/staging-canary/-/pipelines/456",
          "pipeline_id": null,
          "job_id": null,
          "merge_request_iid": null,
          "failure_exception": "navbar did not appear on QA::Page::Main::Menu as expected"
        }
      }
    JSON
  end

  describe '#name' do
    it { expect(test_metric.name).to eq('test-stats') }
  end

  describe '#time' do
    it { expect(test_metric.time).to eq('2023-11-17T21:56:20.000+00:00') }
  end

  describe '#tags' do
    it { expect(test_metric.tags).to be_a Hash }
  end

  describe '#fields' do
    it { expect(test_metric.fields).to be_a Hash }
  end

  describe '#to_json' do
    it { expect(test_metric.to_json.gsub(/\s+/, '')).to eq(metric.gsub(/\s+/, '')) }
  end
end
