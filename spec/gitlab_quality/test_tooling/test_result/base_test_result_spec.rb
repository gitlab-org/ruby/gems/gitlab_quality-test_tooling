# frozen_string_literal: true

RSpec.describe GitlabQuality::TestTooling::TestResult::BaseTestResult do
  subject(:test_result) do
    Class.new(described_class) do
      def failures_from_exceptions; end
    end.new(report: report)
  end

  let(:report) { double }

  describe '#stage' do
    {
      nil => %w[qa/specs/features/sanity/version_spec.rb],
      'enablement' => %w[qa/specs/features/ee/browser_ui/enablement/elasticsearch/elasticsearch_reindexing_spec.rb],
      'monitor' => %w[qa/specs/features/ee/browser_ui/8_monitor/all_monitor_features_spec.rb],
      'geo' => %w[qa/specs/features/ee/api/geo/geo_nodes_spec.rb],
      'non_devops' => %w[qa/specs/features/browser_ui/non_devops/performance_bar_spec.rb],
      'manage' => %w[qa/specs/features/browser_ui/1_manage/project/dashboard_images_spec.rb],
      'create' => %w[qa/specs/features/api/3_create/gitaly/praefect_replication_queue_spec.rb]
    }.each do |expected_stage, file_paths|
      file_paths.each do |path|
        it "extracted #{expected_stage} from #{path}" do
          allow(test_result).to receive_messages(file: path, category: nil)

          expect(test_result.stage).to eq(expected_stage)
        end
      end
    end

    {
      saas: {
        file_path: 'qa/spec/ui/purchase/add_seats_spec.rb',
        category: 'saas'
      },
      'self-managed': {
        file_path: 'qa/spec/ui/self_managed/auto_renew_spec.rb',
        category: 'self-managed'
      }
    }.each do |expected_stage, values|
      it "uses #{values[:category]} when stage is not in file_path" do
        allow(test_result).to receive_messages(file: values[:file_path], category: values[:category])

        expect(test_result.stage).to eq(expected_stage.to_s)
      end
    end
  end

  describe '#full_stacktrace' do
    let(:message) { "StandardError: error" }
    let(:message_lines) { %w[foo bar] }
    let(:failures) do
      [
        {
          'message' => message,
          'message_lines' => message_lines,
          'stacktrace' => "foo\nbar\nfile1\nfile2",
          'correlation_id' => '42'
        }
      ]
    end

    before do
      allow(test_result).to receive(:failures).and_return(failures)
    end

    it 'returns the full stack trace' do
      expect(subject.full_stacktrace).to eq("foo\nbar")
    end

    context 'when message_lines is empty' do
      let(:message_lines) { [] }

      it 'returns message' do
        expect(subject.full_stacktrace).to eq(message)
      end

      context 'when message is nil' do
        let(:message) { nil }

        it 'returns ""' do
          expect(subject.full_stacktrace).to eq('')
        end
      end
    end

    described_class::IGNORED_FAILURES.each do |ignored_failure|
      context "when failures include the ignored failure '#{ignored_failure}'" do
        before do
          failures.unshift({ 'message' => ignored_failure })
        end

        it 'returns the full stack trace' do
          expect(subject.full_stacktrace).to eq("foo\nbar")
        end
      end
    end
  end
end
