# frozen_string_literal: true

RSpec.describe GitlabQuality::TestTooling::Report::GenerateTestSession do
  let(:test_data_path) { 'test_data_path' }
  let(:test_data) do
    <<~JSON
      {
        "examples": [
          {
            "full_description": "test-name-01",
            "file_path": "#{file_path_base}/test_01_spec.rb",
            "testcase": "#{testcase}/1",
            "ci_job_url": "/10",
            "status": "passed",
            "stage": "test",
            "quarantine": null
          },
          {
            "full_description": "test-name-02",
            "file_path": "#{file_path_base}/test_02_spec.rb",
            "testcase": "#{testcase}/2",
            "ci_job_url": "/20",
            "stage": "test",
            "status": "pending"
          },
          {
            "full_description": "test-name-03",
            "file_path": "#{file_path_base}/test_03_spec.rb",
            "testcase": "#{testcase}/3",
            "ci_job_url": "/30",
            "stage": "test",
            "status": "failed"
          },
          {
            "full_description": "test-name-04",
            "file_path": "#{file_path_base}/test_04_spec.rb",
            "testcase": "#{testcase}/4",
            "ci_job_url": "/40",
            "stage": "test",
            "status": "unknown"
          },
          {
            "full_description": "test-name-05",
            "file_path": "qa/specs/features/browser_ui/plan/test_05_spec.rb",
            "testcase": "#{testcase}/5",
            "ci_job_url": "/50",
            "stage": "test",
            "status": "passed",
            "quarantine": "https://gitlab.com/gitlab-org/gitlab/-/issues/12345"
          },
          {
            "full_description": "test-name-06",
            "file_path": "#{file_path_base}/test_01_spec.rb",
            "testcase": "#{testcase}/1",
            "ci_job_url": "/60",
            "stage": "test",
            "status": "passed"
          },
          {
            "full_description": "test-name-03",
            "file_path": "#{file_path_base}/test_03_spec.rb",
            "testcase": "#{testcase}/3",
            "ci_job_url": "/70",
            "stage": "test",
            "status": "failed",
            "quarantine": { "issue": "https://gitlab.com/gitlab-org/gitlab/-/issues/12345" }
          },
          {
            "full_description": "test-name-02",
            "file_path": "#{file_path_base}/test_02_spec.rb",
            "testcase": "#{testcase}/2",
            "ci_job_url": "/80",
            "stage": "test",
            "status": "crashed"
          },
          {
            "full_description": "test-name-07",
            "file_path": "#{file_path_base}/test_07_spec.rb",
            "testcase": "#{testcase}/7",
            "ci_job_url": "/90",
            "stage": "test",
            "status": "failed",
            "failure_issue": "/failures/90"
          },
          {
            "full_description": "test-name-07",
            "file_path": "#{file_path_base}/test_07_spec.rb",
            "testcase": "#{testcase}/7",
            "ci_job_url": "/90",
            "stage": "test",
            "status": "failed",
            "failure_issue": "/failures/90"
          },
          {
            "full_description": "test name 08 with spaces",
            "file_path": "#{file_path_base}/test_08_spec.rb",
            "testcase": "#{testcase}/8",
            "ci_job_url": "/81",
            "stage": "test",
            "status": "failed",
            "failure_issue": "/failures/81"
          },
          {
            "full_description": "test-name-09",
            "file_path": "#{file_path_base}/test_09_spec.rb",
            "testcase": "#{testcase}/9",
            "ci_job_url": "/95",
            "stage": "post-test",
            "status": "failed",
            "failure_issue": "/failures/95"
          },
          {
            "full_description": "test-name-10",
            "file_path": "#{file_path_base}/test_10_spec.rb",
            "testcase": "#{testcase}/10",
            "ci_job_url": "/12",
            "status": "passed",
            "stage": "post-test",
            "quarantine": null
          }
        ]
      }
    JSON
  end

  let(:file_path_base) { 'qa/specs/features/browser_ui/stage' }
  let(:testcase) { 'https://gitlab.com/gitlab-org/gitlab/-/quality/test_cases' }
  let(:pipeline_stages) { [] }

  let(:env) do
    {
      'CI_PROJECT_NAME' => 'staging',
      'CI_PIPELINE_URL' => 'ci_pipeline_url',
      'CI_PIPELINE_ID' => 'ci_pipeline_id',
      'CI_PROJECT_ID' => 'ci_project_id',
      'DEPLOY_VERSION' => 'deadbeef',
      'DEPLOY_ENVIRONMENT' => 'gstg',
      'GITLAB_QA_ISSUE_URL' => 'https://example.com/qa/issue',
      'QA_RUN_TYPE' => 'e2e-package-and-test'
    }
  end

  subject do
    described_class.new(
      token: 'token',
      ci_project_token: 'ci_project_token',
      project: 'project',
      input_files: [double],
      pipeline_stages: pipeline_stages,
      confidential: confidential
    )
  end

  shared_examples 'a new test session issue' do
    it 'creates an issue as test session report' do
      description = <<~MARKDOWN.chomp
      ## Session summary

      * Deploy version: deadbeef
      * Deploy environment: gstg
      * Pipeline: staging [ci_pipeline_id](ci_pipeline_url)
      * Total 13 tests
      * Passed 4 tests
      * Failed 6 tests
      * 3 other tests (usually skipped)

      ## Failed jobs

      * [A](AURL) (allowed to fail)
      * [B](BURL)
      * [C](CURL)

      ### Plan

      * Total 1 tests
      * Passed 1 tests
      * Failed 0 tests
      * 0 other tests (usually skipped)

      <details><summary>Passed tests:</summary>

      | Test | Job | Status | Action |
      | - | - | - | - |
      test-name-05 | [50](/50) ~"quarantine" | ~"passed" | -

      </details>

      ### Stage

      * Total 12 tests
      * Passed 3 tests
      * Failed 6 tests
      * 3 other tests (usually skipped)

      | Test | Job | Status | Action |
      | - | - | - | - |
      [test-name-03](https://gitlab.com/gitlab-org/gitlab/-/quality/test_cases/3) | [30](/30), [70](/70) ~"quarantine" | ~"failed" | <ul><li>[ ] failure issue exists or was created</li></ul>
      [test-name-07](https://gitlab.com/gitlab-org/gitlab/-/quality/test_cases/7) | [90](/90) | ~"failed" | <ul><li>[ ] [failure issue](/failures/90)</li></ul>
      [test name 08 with spaces](https://gitlab.com/gitlab-org/gitlab/-/quality/test_cases/8) | [81](/81) | ~"failed" | <ul><li>[ ] [failure issue](/failures/81)</li></ul>
      [test-name-09](https://gitlab.com/gitlab-org/gitlab/-/quality/test_cases/9) | [95](/95) | ~"failed" | <ul><li>[ ] [failure issue](/failures/95)</li></ul>

      <details><summary>Passed tests:</summary>

      | Test | Job | Status | Action |
      | - | - | - | - |
      test-name-01, test-name-06 | [10](/10), [60](/60) | ~"passed" | -
      test-name-10 | [12](/12) | ~"passed" | -

      </details>

      <details><summary>Other tests:</summary>

      | Test | Job | Status | Action |
      | - | - | - | - |
      [test-name-02](https://gitlab.com/gitlab-org/gitlab/-/quality/test_cases/2) | [20](/20), [80](/80) | ~"pending", ~"crashed" | -
      [test-name-04](https://gitlab.com/gitlab-org/gitlab/-/quality/test_cases/4) | [40](/40) | ~"unknown" | -

      </details>

      ## Release QA issue

      * https://example.com/qa/issue

      /relate https://example.com/qa/issue

      ## Link to Grafana dashboard for run-type of e2e-package-and-test

      * https://dashboards.quality.gitlab.net/d/tR_SmBDVk/main-runs?orgId=1&refresh=1m&var-run_type=e2e-package-and-test
      MARKDOWN

      stub_issues_request(description)

      expect { subject.invoke! }.to output.to_stdout
      expect(subject.invoke!).to eq('issue_url')
    end
  end

  before do
    allow(subject).to receive(:validate_input!)
    allow(Dir).to receive(:glob).and_return([test_data_path])
    allow(File).to receive(:read).and_return(test_data)

    stub_request(:get, 'https://gitlab.com/api/v4/projects/ci_project_id/pipelines/ci_pipeline_id/jobs?scope=failed')
      .with(headers:
      {
        'Private-Token' => 'ci_project_token'
      })
      .to_return(body:
      [
        { allow_failure: true, name: 'A', web_url: 'AURL', stage: 'test' },
        { allow_failure: false, name: 'B', web_url: 'BURL', stage: 'test' },
        { allow_failure: false, name: 'C', web_url: 'CURL', stage: 'post-test' }
      ].to_json)

    stub_request(:post, 'https://gitlab.com/api/v4/projects/project/issues/4/notes')
      .with(headers:
      {
        'Private-Token' => 'token'
      })
      .with(body:
      {
        'body' => '/relate https://example.com/qa/issue'
      })
  end

  around do |example|
    ClimateControl.modify(env) { example.run }
  end

  describe '#invoke!' do
    context 'when confidential is true' do
      it_behaves_like 'a new test session issue' do
        let(:confidential) { true }
      end
    end

    context 'when confidential is false' do
      it_behaves_like 'a new test session issue' do
        let(:confidential) { false }
      end
    end

    context 'without the --pipeline-stages flag set' do
      it_behaves_like 'a new test session issue' do
        let(:confidential) { false }
        let(:pipeline_stages) { nil }
      end
    end

    context 'with the --pipeline-stages flag set' do
      let(:confidential) { false }
      let(:pipeline_stages) { %w[post-test] }

      it 'creates a test session report filtered to post-test stage only' do
        filtered_description = <<~MARKDOWN.chomp
        ## Session summary

        * Deploy version: deadbeef
        * Deploy environment: gstg
        * Pipeline: staging [ci_pipeline_id](ci_pipeline_url)
        * Total 2 tests
        * Passed 1 tests
        * Failed 1 tests
        * 0 other tests (usually skipped)

        ## Failed jobs

        * [C](CURL)

        ### Stage

        * Total 2 tests
        * Passed 1 tests
        * Failed 1 tests
        * 0 other tests (usually skipped)

        | Test | Job | Status | Action |
        | - | - | - | - |
        [test-name-09](https://gitlab.com/gitlab-org/gitlab/-/quality/test_cases/9) | [95](/95) | ~"failed" | <ul><li>[ ] [failure issue](/failures/95)</li></ul>

        <details><summary>Passed tests:</summary>

        | Test | Job | Status | Action |
        | - | - | - | - |
        test-name-10 | [12](/12) | ~"passed" | -

        </details>

        ## Release QA issue

        * https://example.com/qa/issue

        /relate https://example.com/qa/issue

        ## Link to Grafana dashboard for run-type of e2e-package-and-test

        * https://dashboards.quality.gitlab.net/d/tR_SmBDVk/main-runs?orgId=1&refresh=1m&var-run_type=e2e-package-and-test
        MARKDOWN

        stub_issues_request(filtered_description)

        expect { subject.invoke! }.to output.to_stdout
        expect(subject.invoke!).to eq('issue_url')
      end
    end
  end

  def stub_issues_request(description)
    stub_request(:post, 'https://gitlab.com/api/v4/projects/project/issues')
      .with(headers:
      {
        'Private-Token' => 'token'
      })
      .with(body:
      {
        'title' => "#{Time.now.strftime('%Y-%m-%d')} Test session report | #{env['QA_RUN_TYPE']}",
        'description' => description,
        'issue_type' => 'issue',
        'labels' => ['automation:bot-authored', 'Quality', 'QA', 'triage report', 'found:staging.gitlab.com'],
        'confidential' => confidential
      })
      .to_return(body: { 'web_url' => 'issue_url', 'iid' => 4 }.to_json)
  end
end
