# frozen_string_literal: true

RSpec.describe GitlabQuality::TestTooling::Report::HealthProblemReporter do
  include_context 'with repository files client and content'

  let(:project)   { 'valid-project' }
  let(:instance)  { described_class.new(project: project, token: 'token') }

  describe '#most_recent_report_date_for_issue' do
    let(:issue_iid) { 1234 }

    context 'when the reports note is not found' do
      before do
        allow(instance).to receive(:existing_reports_note)
      end

      it 'returns nil' do
        expect(instance.most_recent_report_date_for_issue(issue_iid: issue_iid)).to be_nil
      end
    end

    context 'when the reports note is found' do
      before do
        allow(instance).to receive(:existing_reports_note).and_return(
          double('report note', body: body) # rubocop:disable RSpec/VerifiedDoubles -- Internal API
        )
      end

      context 'when the latest report is not found' do
        let(:body) do
          <<~BODY
            ### Flakiness reports (0)

              No reports in there

            Flaky tests were detected. Please refer to the [Flaky tests reproducibility instructions](https://docs.gitlab.com/ee/development/testing_guide/flaky_tests.html#how-to-reproduce-a-flaky-test-locally)
            to learn more about how to reproduce them.
          BODY
        end

        it 'returns nil' do
          expect(instance.most_recent_report_date_for_issue(issue_iid: issue_iid)).to be_nil
        end
      end

      context 'when the latest report is found' do
        let(:body) do
          <<~BODY
            ### Flakiness reports (2)

            1. 2024-05-14: https://gitlab.com/gitlab-org/gitlab/-/jobs/6842692836 (https://gitlab.com/gitlab-org/gitlab/-/pipelines/1289409405) ~"found:master"
            1. 2024-05-09: https://gitlab.com/gitlab-org/gitlab/-/jobs/6818528202 (https://gitlab.com/gitlab-org/gitlab/-/pipelines/1285044712) ~"found:master"

            Flaky tests were detected. Please refer to the [Flaky tests reproducibility instructions](https://docs.gitlab.com/ee/development/testing_guide/flaky_tests.html#how-to-reproduce-a-flaky-test-locally)
            to learn more about how to reproduce them.
          BODY
        end

        it 'returns the most recent report' do
          expect(instance.most_recent_report_date_for_issue(issue_iid: issue_iid)).to eq('2024-05-14')
        end
      end
    end
  end
end
