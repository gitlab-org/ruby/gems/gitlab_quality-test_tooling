# frozen_string_literal: true

RSpec.describe GitlabQuality::TestTooling::Report::FailedTestIssue, :aggregate_failures do
  include_context 'with repository files client and content'

  let(:project)           { 'valid-project' }
  let(:input_file)        { File.expand_path("fixtures/rspec-reports/flaky/rspec-retry-5258318483_pass_and_fail.json", __dir__) }
  let(:base_issue_labels) { nil }
  let(:related_issues_file) { nil }
  let(:expected_new_issue_description) do
    <<~DESCRIPTION.chomp
    ### Test metadata

    <!-- Don't modify this section as it's auto-generated -->
    | Field | Value |
    | ------ | ------ |
    | File URL | [`ee/spec/lib/gitlab/geo_spec.rb#L415`](https://gitlab.com/#{project}/-/blob/master/ee/spec/lib/gitlab/geo_spec.rb#L415) |
    | Filename | `ee/spec/lib/gitlab/geo_spec.rb` |
    | Description | `` Gitlab::Geo.proxied_site on a primary for a proxied request with a proxy extra data header for an existing site is expected to eq  `` |
    | Test level | `unit` |
    | Hash | `2a0eebeafc1f88982854f66bb82371cdb6011d7c4` |
    | Max expected duration | < 27.12 seconds |

    <!-- Don't modify this section as it's auto-generated -->

    DESCRIPTION
  end

  let(:instance) do
    described_class.new(
      base_issue_labels: base_issue_labels,
      input_files: [input_file],
      project: project,
      token: 'token',
      related_issues_file: related_issues_file
    )
  end

  subject(:report) { instance }

  around do |example|
    ClimateControl.modify(
      CI_PIPELINE_URL: 'pipeline-url',
      CI_MERGE_REQUEST_IID: '42',
      CI_COMMIT_REF_NAME: 'master') do
      example.run
    end
  end

  before do
    allow(report.__send__(:gitlab)).to receive(:assert_user_permission!)

    # Silence outputs to stdout by default
    allow(subject).to receive(:puts)
  end

  describe '#most_recent_report_date_for_issue' do
    let(:issue_iid) { 1234 }

    context 'when the reports discussion is not found' do
      before do
        allow(instance).to receive(:existing_reports_discussion)
      end

      it 'returns nil' do
        expect(instance.most_recent_report_date_for_issue(issue_iid: issue_iid)).to be_nil
      end
    end

    context 'when the reports discussion is found' do
      before do
        allow(instance).to receive(:existing_reports_discussion).and_return(
          double('report discussion', notes: reports_notes) # rubocop:disable RSpec/VerifiedDoubles -- Internal API
        )
      end

      context 'when no latest reports were found' do
        # rubocop:disable RSpec/VerifiedDoubles -- Internal API]
        let(:reports_notes) do
          [
            double('report note', body: 'Discussion header note'),
            double('report note', body: body)
          ]
        end
        # rubocop:enable RSpec/VerifiedDoubles

        let(:body) do
          <<~BODY
            ### Failure reports (0)

              No reports in there
          BODY
        end

        it 'returns nil' do
          expect(instance.most_recent_report_date_for_issue(issue_iid: issue_iid)).to be_nil
        end
      end

      context 'when latest reports were found' do
        # rubocop:disable RSpec/VerifiedDoubles -- Internal API]
        let(:reports_notes) do
          [
            double('first report note', body: 'Discussion header note'),
            double('first report note', body: first_body),
            double('second report note', body: second_body)
          ]
        end
        # rubocop:enable RSpec/VerifiedDoubles

        let(:first_body) do
          <<~BODY
            ### Failure reports (2)

            1. 2024-09-14: https://gitlab.com/gitlab-org/gitlab/-/jobs/6842692836 (https://gitlab.com/gitlab-org/gitlab/-/pipelines/1289409405) ~"found:master"
            1. 2024-05-09: https://gitlab.com/gitlab-org/gitlab/-/jobs/6818528202 (https://gitlab.com/gitlab-org/gitlab/-/pipelines/1285044712) ~"found:master"
          BODY
        end

        let(:second_body) do
          <<~BODY
            ### Failure reports (1)

            1. 2024-08-14: https://gitlab.com/gitlab-org/gitlab/-/jobs/6842692836 (https://gitlab.com/gitlab-org/gitlab/-/pipelines/1289409405) ~"found:master"
          BODY
        end

        it 'returns the most recent report' do
          expect(instance.most_recent_report_date_for_issue(issue_iid: issue_iid)).to eq('2024-09-14')
        end
      end
    end
  end

  describe '#invoke!' do
    context 'when there is a failed test' do
      let(:issue_class)            { Struct.new(:iid, :web_url, :state, :title, :description, :labels, keyword_init: true) }
      let(:issue_note_class)       { Struct.new(:id, :body, keyword_init: true) }
      let(:issue_discussion_class) { Struct.new(:id, :individual_note, :notes, keyword_init: true) }

      context 'with no existing test issue' do
        let(:new_issue)              { issue_class.new(iid: 0, web_url: 'http://new-issue.url', description: expected_new_issue_description) }
        let(:new_issue_discussion)   { issue_discussion_class.new(id: 1, individual_note: false, notes: []) }
        let(:labels_inference)       { instance_double(GitlabQuality::TestTooling::LabelsInference) }

        before do
          allow(report.__send__(:gitlab)).to receive_messages(
            find_issues: [],
            find_issue_discussions: [],
            find_issue_notes: [],
            create_issue: nil,
            create_issue_discussion: new_issue_discussion,
            add_note_to_issue_discussion_as_thread: nil
          )
          allow(GitlabQuality::TestTooling::LabelsInference).to receive(:new).and_return(labels_inference)
          allow(labels_inference).to receive_messages(infer_labels_from_product_group: Set.new, infer_labels_from_feature_category: Set.new)
        end

        RSpec.shared_examples 'creates new issue' do |expected_label_names|
          it 'creates an issue, a discussion and a note' do
            expect(report.__send__(:gitlab)).to receive(:create_issue)
              .with(
                title: "[Test] ee/spec/lib/gitlab/geo_spec.rb | Gitlab::Geo.proxied_site on a primary for a proxied request with a proxy " \
                       "extra data header for an existing site is expected to eq",
                description: expected_new_issue_description,
                labels: expected_label_names,
                confidential: false
              ).and_return(new_issue)

            expect(report.__send__(:gitlab)).to receive(:create_issue_discussion).with(
              iid: new_issue.iid,
              note: '#### Failure reports'
            ).and_return(new_issue_discussion)

            expect(report.__send__(:gitlab)).to receive(:add_note_to_issue_discussion_as_thread).with(
              iid: new_issue.iid,
              discussion_id: new_issue_discussion.id,
              note: <<~MESSAGE
              #### Failure reports (1)

              1. #{Time.new.utc.strftime('%F')}: https://gitlab.com/gitlab-org/gitlab/-/jobs/5258318483 (pipeline-url) ~"found:in MR"

              ##### Stack trace

              ```
              Failure/Error: Page: :Project: :Wiki: :Edit.perform(&:click_submit)

              Support: :Repeater: :WaitExceededError:
                Page did not fully load. This could be due to an unending async request or loading icon.
              ```
              /label ~"failed-tests::4"
              /label ~"test" ~"test-health:failures" ~"automation:bot-authored"
              MESSAGE
            )

            report.invoke!
          end
        end

        context 'with failed line number not calling any shared examples' do
          it_behaves_like 'creates new issue', described_class::NEW_ISSUE_LABELS.to_a
        end

        context 'with failed line number calling shared examples' do
          let(:repository_file_line_content) { "  it_behaves_like 'shared examples' do " }

          it_behaves_like 'creates new issue', [*described_class::NEW_ISSUE_LABELS.to_a, 'rspec-shared-examples']
        end

        describe '--base-issue-labels' do
          context 'with the --base-issue-labels flag set' do
            let(:base_issue_labels) { ['custom label', 'other custom label'] }

            it 'creates an issue with the additional flag' do
              expect(report.__send__(:gitlab)).to receive(:create_issue)
                .with(
                  title: "[Test] ee/spec/lib/gitlab/geo_spec.rb | Gitlab::Geo.proxied_site on a primary for a proxied request with a proxy " \
                         "extra data header for an existing site is expected to eq",
                  description: expected_new_issue_description,
                  labels: base_issue_labels + described_class::NEW_ISSUE_LABELS.to_a,
                  confidential: false
                )
                .and_return(new_issue)

              report.invoke!
            end
          end
        end

        describe '--related_issues_file' do
          context 'with the --related_issues_file flag set' do
            let(:related_issues_file) { 'tmp/related-issues-file.json' }

            before do
              allow(report.__send__(:gitlab)).to receive(:create_issue).and_return(new_issue)
            end

            it 'write the related issues path to file' do
              expect(File).to receive(:write).with(
                related_issues_file,
                JSON.pretty_generate({
                  "https://gitlab.com/gitlab-org/gitlab/-/jobs/5258318483" => ["http://new-issue.url"]
                })
              )

              report.invoke!
            end
          end
        end
      end

      context 'with existing test issues' do
        let(:existing_issue_labels) { ['automation:bot-authored', 'priority::3', 'severity::3'] }
        let(:existing_issue) do
          issue_class.new(
            iid: 0,
            description: expected_new_issue_description,
            web_url: 'http://existing-issue.url',
            labels: existing_issue_labels
          )
        end

        let(:first_discussion_note) do
          issue_note_class.new(
            id: 1,
            body: <<~DESCRIPTION
            #### Failure reports (1)
            DESCRIPTION
          )
        end

        let(:existing_second_discussion_note) do
          issue_note_class.new(
            id: 2,
            body: <<~DESCRIPTION.chomp
            #### Failure reports (1)

            1. 2024-02-01: https://gitlab.com/gitlab-org/gitlab/-/jobs/5258318483 (pipeline-url) ~"found:in MR"

            ##### Stack trace

            ```
            Failure/Error: Page: :Project: :Wiki: :Edit.perform(&:click_submit)

            Support: :Repeater: :WaitExceededError:
              Page did not fully load. This could be due to an unending async request or loading icon.
            ```
            /label ~"failed-tests::4"
            /label ~"test" ~"test-health:failures" ~"automation:bot-authored"
            /relate http://existing-issue.url
            DESCRIPTION
          )
        end

        let(:existing_issue_discussion) do
          issue_discussion_class.new(id: 4, individual_note: false, notes: [first_discussion_note, existing_second_discussion_note])
        end

        let(:new_issue_discussion) do
          issue_discussion_class.new(id: 5, individual_note: false, notes: [])
        end

        let(:related_issue_labels) { [] }

        let(:related_issue) do
          issue_class.new(
            iid: 6,
            description: expected_new_issue_description,
            web_url: 'http://related-issue.url',
            labels: related_issue_labels
          )
        end

        before do
          allow(report.__send__(:gitlab)).to receive_messages(
            find_issues: [existing_issue, related_issue]
          )
          allow(report.__send__(:gitlab)).to receive(:find_issue_discussions).with(iid: existing_issue.iid).and_return([existing_issue_discussion])
          allow(report.__send__(:gitlab)).to receive(:find_issue_discussions).with(iid: related_issue.iid).and_return([])
        end

        shared_examples 'reports notes update in existing issue and creation in related issue' do |found_in_label|
          context 'with one report' do
            let(:existing_report_list_and_stack_trace) do
              <<~MARKDOWN.chomp
                1. 2024-02-01: https://gitlab.com/gitlab-org/gitlab/-/jobs/5258318483 (pipeline-url) ~"found:in MR"

                ##### Stack trace

                ```
                Failure/Error: Page: :Project: :Wiki: :Edit.perform(&:click_submit)

                Support: :Repeater: :WaitExceededError:
                  Page did not fully load. This could be due to an unending async request or loading icon.
                ```
              MARKDOWN
            end

            it 'creates a discussion and note for the related issue' do
              expect(report.__send__(:gitlab)).not_to receive(:create_issue)

              # Edit existing note in existing_issue
              expected_updated_note =
                <<~DESCRIPTION.chomp
                #### Failure reports (2)

                1. #{Time.new.utc.strftime('%F')}: https://gitlab.com/gitlab-org/gitlab/-/jobs/5258318483 (pipeline-url) ~"#{found_in_label}"
                #{existing_report_list_and_stack_trace}
                /label ~"failed-tests::4"
                /label ~"test" ~"test-health:failures" ~"automation:bot-authored"
                /relate http://related-issue.url
                DESCRIPTION
              expect(report.__send__(:gitlab)).to receive(:edit_issue_note).with(
                issue_iid: existing_issue.iid,
                note_id: existing_second_discussion_note.id,
                note: expected_updated_note)

              # Create new discussion and note in related_issue
              expect(report.__send__(:gitlab)).to receive(:create_issue_discussion).with(
                iid: related_issue.iid,
                note: '#### Failure reports'
              )
              .and_return(new_issue_discussion)
              expect(report.__send__(:gitlab)).to receive(:add_note_to_issue_discussion_as_thread).with(
                iid: related_issue.iid,
                discussion_id: new_issue_discussion.id,
                note: <<~DESCRIPTION.chomp
                  #### Failure reports (1)

                  1. #{Time.new.utc.strftime('%F')}: https://gitlab.com/gitlab-org/gitlab/-/jobs/5258318483 (pipeline-url) ~"#{found_in_label}"

                  ##### Stack trace

                  ```
                  Failure/Error: Page: :Project: :Wiki: :Edit.perform(&:click_submit)

                  Support: :Repeater: :WaitExceededError:
                    Page did not fully load. This could be due to an unending async request or loading icon.
                  ```
                  /label ~"failed-tests::4"
                  /label ~"test" ~"test-health:failures" ~"automation:bot-authored"
                  /relate http://existing-issue.url
                DESCRIPTION
              )

              report.invoke!
            end
          end

          context 'with discussion note already has 9 reports from the same day' do
            around do |example|
              Timecop.freeze(Time.utc(2024, 2, 1, 1)) { example.run }
            end

            let(:existing_9_report_list_and_stack_trace) do
              list_item_string = '1. 2024-02-01: https://gitlab.com/gitlab-org/gitlab/-/jobs/5258318483 (pipeline-url) ~"found:in MR"'
              <<~MARKDOWN.chomp
                #{Array.new(9, list_item_string).join("\n")}

                ##### Stack trace

                ```
                Failure/Error: Page: :Project: :Wiki: :Edit.perform(&:click_submit)

                Support: :Repeater: :WaitExceededError:
                  Page did not fully load. This could be due to an unending async request or loading icon.
                ```
              MARKDOWN
            end

            let(:second_discussion_note_with_9_reports) do
              issue_note_class.new(
                id: 2,
                body: <<~DESCRIPTION.chomp
                #### Failure reports (1)

                #{existing_9_report_list_and_stack_trace}
                /label ~"failed-tests::4"
                /label ~"test" ~"test-health:failures" ~"automation:bot-authored"
                /relate http://existing-issue.url
                DESCRIPTION
              )
            end

            let(:issue_discussion_with_9_reports) do
              issue_discussion_class.new(id: 4, individual_note: false, notes: [first_discussion_note, second_discussion_note_with_9_reports])
            end

            before do
              allow(report.__send__(:gitlab)).to receive(:find_issue_discussions).with(iid: existing_issue.iid).and_return([issue_discussion_with_9_reports])
            end

            it 'edits discussion note and labels failed test issue with severity::1' do
              expect(report.__send__(:gitlab)).not_to receive(:create_issue)

              # Edit existing note in existing_issue
              expected_updated_note =
                <<~DESCRIPTION.chomp
                #### Failure reports (10)

                1. #{Time.new.utc.strftime('%F')}: https://gitlab.com/gitlab-org/gitlab/-/jobs/5258318483 (pipeline-url) ~"#{found_in_label}"
                #{existing_9_report_list_and_stack_trace}
                /label ~"priority::1" ~"severity::1"
                /label ~"failed-tests::4"
                /label ~"test" ~"test-health:failures" ~"automation:bot-authored"
                /relate http://related-issue.url
                DESCRIPTION
              expect(report.__send__(:gitlab)).to receive(:edit_issue_note).with(
                issue_iid: existing_issue.iid,
                note_id: existing_second_discussion_note.id,
                note: expected_updated_note)

              # Create new discussion and note in related_issue
              expect(report.__send__(:gitlab)).to receive(:create_issue_discussion).with(
                iid: related_issue.iid,
                note: '#### Failure reports'
              )
              .and_return(new_issue_discussion)
              expect(report.__send__(:gitlab)).to receive(:add_note_to_issue_discussion_as_thread).with(
                iid: related_issue.iid,
                discussion_id: new_issue_discussion.id,
                note: <<~DESCRIPTION.chomp
                  #### Failure reports (1)

                  1. #{Time.new.utc.strftime('%F')}: https://gitlab.com/gitlab-org/gitlab/-/jobs/5258318483 (pipeline-url) ~"#{found_in_label}"

                  ##### Stack trace

                  ```
                  Failure/Error: Page: :Project: :Wiki: :Edit.perform(&:click_submit)

                  Support: :Repeater: :WaitExceededError:
                    Page did not fully load. This could be due to an unending async request or loading icon.
                  ```
                  /label ~"failed-tests::4"
                  /label ~"test" ~"test-health:failures" ~"automation:bot-authored"
                  /relate http://existing-issue.url
                DESCRIPTION
              )

              report.invoke!
            end
          end
        end

        it_behaves_like 'reports notes update in existing issue and creation in related issue', 'found:in MR'

        context 'with a master pipeline' do
          around do |example|
            ClimateControl.modify(
              CI_MERGE_REQUEST_IID: nil
            ) do
              example.run
            end
          end

          it_behaves_like 'reports notes update in existing issue and creation in related issue', 'found:master'
        end

        context 'with failed test calling shared example and issue label missing rspec-shared-examples' do
          let(:repository_file_line_content) { "  it_behaves_like 'shared examples' do " }

          before do
            allow(subject.__send__(:gitlab))
            .to receive(:edit_issue).with(
              iid: existing_issue.iid,
              options: {
                add_labels: [*existing_issue_labels, 'rspec-shared-examples']
              }
            )

            allow(subject.__send__(:gitlab))
            .to receive(:edit_issue).with(
              iid: related_issue.iid,
              options: {
                add_labels: [*related_issue_labels, 'rspec-shared-examples']
              }
            )
          end

          it_behaves_like 'reports notes update in existing issue and creation in related issue', 'found:in MR'
        end
      end
    end
  end
end
