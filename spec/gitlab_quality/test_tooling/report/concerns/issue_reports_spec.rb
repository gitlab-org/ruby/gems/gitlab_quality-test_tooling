# frozen_string_literal: true

RSpec.describe GitlabQuality::TestTooling::Report::Concerns::IssueReports do
  include StubENV

  let(:instance) { Class.new { include GitlabQuality::TestTooling::Report::Concerns::IssueReports }.new }

  let(:input_file) { File.expand_path("../fixtures/rspec-reports/flaky/rspec-retry-5258318483_pass_and_fail.json", __dir__) }
  let(:tests)      { GitlabQuality::TestTooling::TestResults::JsonTestResults.new(path: input_file) }
  let(:test)       { tests.first }

  before do
    stub_env('CI_PIPELINE_URL', 'pipeline-url')
  end

  describe '#initial_reports_section' do
    subject { instance.initial_reports_section(test) }

    context 'when the CI_PIPELINE_URL env variable is set' do
      before do
        stub_env('CI_PIPELINE_URL', 'https://gitlab.test/gitlab-org/gitlab/-/pipelines/1234')
      end

      it 'returns the correct issue description' do
        expect(subject).to eq(<<~DESCRIPTION
          ### Reports (1)

          1. #{Time.new.utc.strftime('%F')}: #{test.ci_job_url} (https://gitlab.test/gitlab-org/gitlab/-/pipelines/1234)
        DESCRIPTION
                             )
      end
    end

    context 'when the CI_PIPELINE_URL env variable is not set' do
      before do
        stub_env('CI_PIPELINE_URL', nil)
      end

      it 'returns the correct issue description' do
        expect(subject).to eq(<<~DESCRIPTION
          ### Reports (1)

          1. #{Time.new.utc.strftime('%F')}: #{test.ci_job_url} (pipeline url is missing)
        DESCRIPTION
                             )
      end
    end
  end

  describe '#increment_reports' do
    let(:other_test) { tests.first(2).last }

    subject(:reports_list) { instance.increment_reports(current_reports_content: current_reports_content, test: test) }

    context 'with an empty current_reports_content' do
      let(:current_reports_content) { '' }

      it 'increments the number of reports' do
        expected_description = <<~DESCRIPTION.chomp
          ### Reports (1)

          1. #{Time.new.utc.strftime('%F')}: #{test.ci_job_url} (pipeline-url)
        DESCRIPTION

        expect(reports_list.to_s).to eq(expected_description)
      end
    end

    context 'with a current_reports_content that contains a CI job URL' do
      let(:current_reports_content) do
        <<~DESCRIPTION.chomp
          Issue description.

          ### Reports (1)

          1. 2024-02-01: #{other_test.ci_job_url} (pipeline-url)
        DESCRIPTION
      end

      it 'increments the number of reports' do
        expect(reports_list.to_s).to include('### Reports (2)')
      end

      it 'concatenates the test in the reports list' do
        expected_description = <<~DESCRIPTION.chomp
          Issue description.

          ### Reports (2)

          1. #{Time.new.utc.strftime('%F')}: #{test.ci_job_url} (pipeline-url)
          1. 2024-02-01: #{other_test.ci_job_url} (pipeline-url)
        DESCRIPTION

        expect(reports_list.to_s).to eq(expected_description)
      end
    end

    context 'with a current_reports_content that contains a lot of CI job URLs' do
      let(:current_reports_content) do
        <<~DESCRIPTION.chomp
          Issue description.

          ### Reports (10)

          1. 2024-02-01: #{other_test.ci_job_url} (pipeline-url)
          1. 2024-02-02: #{other_test.ci_job_url} (pipeline-url)
          1. 2024-02-03: #{other_test.ci_job_url} (pipeline-url)
          1. 2024-02-04: #{other_test.ci_job_url} (pipeline-url)
          1. 2024-02-05: #{other_test.ci_job_url} (pipeline-url)
          1. 2024-02-06: #{other_test.ci_job_url} (pipeline-url)
          1. 2024-02-07: #{other_test.ci_job_url} (pipeline-url)
          1. 2024-02-08: #{other_test.ci_job_url} (pipeline-url)
          1. 2024-02-09: #{other_test.ci_job_url} (pipeline-url)
          1. 2024-02-10: #{other_test.ci_job_url} (pipeline-url)
        DESCRIPTION
      end

      it 'put the extra reports into a <details></details> block' do
        expected_description = <<~DESCRIPTION.chomp
          Issue description.

          ### Reports (11)

          Last 10 reports:

          1. #{Time.new.utc.strftime('%F')}: #{test.ci_job_url} (pipeline-url)
          1. 2024-02-10: #{other_test.ci_job_url} (pipeline-url)
          1. 2024-02-09: #{other_test.ci_job_url} (pipeline-url)
          1. 2024-02-08: #{other_test.ci_job_url} (pipeline-url)
          1. 2024-02-07: #{other_test.ci_job_url} (pipeline-url)
          1. 2024-02-06: #{other_test.ci_job_url} (pipeline-url)
          1. 2024-02-05: #{other_test.ci_job_url} (pipeline-url)
          1. 2024-02-04: #{other_test.ci_job_url} (pipeline-url)
          1. 2024-02-03: #{other_test.ci_job_url} (pipeline-url)
          1. 2024-02-02: #{other_test.ci_job_url} (pipeline-url)

          <details><summary>With 1 more reports (displaying up to 500 reports) </summary>

          1. 2024-02-01: #{other_test.ci_job_url} (pipeline-url)

          </details>
        DESCRIPTION

        expect(reports_list.to_s).to eq(expected_description)
      end

      context 'with a current_reports_content that contains a report from a different project' do
        let(:current_reports_content) do
          <<~DESCRIPTION.chomp
            Issue description.

            ### Reports (1)

            1. #{Time.new.utc.strftime('%F')}: https://ops.gitlab.net/gitlab-org/quality/staging-canary/-/jobs/14615849 (pipeline-url)
          DESCRIPTION
        end

        it 'puts the reports in the correct order' do
          test.report["ci_job_url"] = "https://ops.gitlab.net/gitlab-org/quality/canary/-/jobs/14624493"

          expected_description = <<~DESCRIPTION.chomp
            Issue description.

            ### Reports (2)

            1. #{Time.new.utc.strftime('%F')}: https://ops.gitlab.net/gitlab-org/quality/canary/-/jobs/14624493 (pipeline-url)
            1. #{Time.new.utc.strftime('%F')}: https://ops.gitlab.net/gitlab-org/quality/staging-canary/-/jobs/14615849 (pipeline-url)
          DESCRIPTION

          expect(reports_list.to_s).to eq(expected_description)
        end
      end
    end

    context 'with the current report exceeding the displayed reports limit' do
      let(:previously_displayed_history_reports) do
        Array.new(500) { "1. 2024-02-01: #{other_test.ci_job_url} (pipeline-url)" }.join("\n")
      end

      let(:current_reports_content) do
        <<~DESCRIPTION.chomp
          Issue description.

          ### Reports (520)

          Last 10 reports:

          1. 2024-02-01: #{other_test.ci_job_url} (pipeline-url)
          1. 2024-02-02: #{other_test.ci_job_url} (pipeline-url)
          1. 2024-02-03: #{other_test.ci_job_url} (pipeline-url)
          1. 2024-02-04: #{other_test.ci_job_url} (pipeline-url)
          1. 2024-02-05: #{other_test.ci_job_url} (pipeline-url)
          1. 2024-02-06: #{other_test.ci_job_url} (pipeline-url)
          1. 2024-02-07: #{other_test.ci_job_url} (pipeline-url)
          1. 2024-02-08: #{other_test.ci_job_url} (pipeline-url)
          1. 2024-02-09: #{other_test.ci_job_url} (pipeline-url)
          1. 2024-02-10: #{other_test.ci_job_url} (pipeline-url)

          <details><summary>With 510 more reports (displaying up to 500 reports) </summary>

          #{previously_displayed_history_reports}

          </details>
        DESCRIPTION
      end

      let(:expected_history_reports_after_increment) do
        Array.new(500) { "1. 2024-02-01: #{other_test.ci_job_url} (pipeline-url)" }.join("\n")
      end

      let(:expected_reports_after_increment) do
        <<~DESCRIPTION.chomp
          Issue description.

          ### Reports (521)

          Last 10 reports:

          1. #{Time.new.utc.strftime('%F')}: #{test.ci_job_url} (pipeline-url)
          1. 2024-02-10: #{other_test.ci_job_url} (pipeline-url)
          1. 2024-02-09: #{other_test.ci_job_url} (pipeline-url)
          1. 2024-02-08: #{other_test.ci_job_url} (pipeline-url)
          1. 2024-02-07: #{other_test.ci_job_url} (pipeline-url)
          1. 2024-02-06: #{other_test.ci_job_url} (pipeline-url)
          1. 2024-02-05: #{other_test.ci_job_url} (pipeline-url)
          1. 2024-02-04: #{other_test.ci_job_url} (pipeline-url)
          1. 2024-02-03: #{other_test.ci_job_url} (pipeline-url)
          1. 2024-02-02: #{other_test.ci_job_url} (pipeline-url)

          <details><summary>With 511 more reports (displaying up to 500 reports) </summary>

          #{expected_history_reports_after_increment}

          </details>
        DESCRIPTION
      end

      it 'only displays the reports within threshold while keeping tracks of the total number of reports' do
        expect(reports_list.to_s).to include('### Reports (521)')
        expect(reports_list.to_s).to include('<details><summary>With 511 more reports (displaying up to 500 reports) </summary>')
        expect(reports_list.to_s).to eq(expected_reports_after_increment)
      end
    end

    context 'with a current_reports_content that does not contain a CI job URL' do
      let(:current_reports_content) do
        <<~DESCRIPTION.chomp
          ### Stack Trace

          ```shell
          test
          ```

          No failed CI job to link.
        DESCRIPTION
      end

      it 'creates a report section' do
        expected_description = <<~DESCRIPTION.chomp
          ### Stack Trace

          ```shell
          test
          ```

          No failed CI job to link.

          ### Reports (1)

          1. #{Time.new.utc.strftime('%F')}: #{test.ci_job_url} (pipeline-url)
        DESCRIPTION

        expect(reports_list.to_s).to eq(expected_description)
      end
    end

    context 'with reports_section_header given' do
      subject(:reports_list) { instance.increment_reports(current_reports_content: current_reports_content, test: test, reports_section_header: "### Section") }

      let(:current_reports_content) do
        <<~DESCRIPTION.chomp
          ### Section

        DESCRIPTION
      end

      it 'matches the expected section' do
        expected_description = <<~DESCRIPTION.chomp
          ### Section (1)

          1. #{Time.new.utc.strftime('%F')}: #{test.ci_job_url} (pipeline-url)
        DESCRIPTION

        expect(reports_list.to_s).to eq(expected_description)
      end
    end

    context 'with item_extra_content given' do
      subject(:reports_list) { instance.increment_reports(current_reports_content: current_reports_content, test: test, item_extra_content: "extra item content") }

      let(:current_reports_content) do
        <<~DESCRIPTION.chomp
          ### Reports

          1. 2024-02-01: #{test.ci_job_url} (pipeline-url)
        DESCRIPTION
      end

      it 'adds the extra item content' do
        expected_description = <<~DESCRIPTION.chomp
          ### Reports (2)

          1. #{Time.new.utc.strftime('%F')}: #{test.ci_job_url} (pipeline-url) extra item content
          1. 2024-02-01: #{test.ci_job_url} (pipeline-url)
        DESCRIPTION

        expect(reports_list.to_s).to eq(expected_description)
      end
    end

    context 'with reports_extra_content given' do
      subject(:reports_list) { instance.increment_reports(current_reports_content: current_reports_content, test: test, reports_extra_content: "report item content") }

      let(:current_reports_content) do
        <<~DESCRIPTION.chomp
          Issue description.

          ### Reports

          1. #{Time.new.utc.strftime('%F')}: #{test.ci_job_url} (pipeline-url)

          Extra content is removed.
        DESCRIPTION
      end

      it 'adds the extra report content' do
        expected_description = <<~DESCRIPTION.chomp
          Issue description.

          ### Reports (2)

          1. #{Time.new.utc.strftime('%F')}: #{test.ci_job_url} (pipeline-url)
          1. #{Time.new.utc.strftime('%F')}: #{test.ci_job_url} (pipeline-url)

          report item content
        DESCRIPTION

        expect(reports_list.to_s).to eq(expected_description)
      end
    end
  end

  describe '#failed_issue_job_url' do
    let(:issue) { Struct.new(:description).new(issue_description) }

    subject { instance.failed_issue_job_url(issue) }

    context 'with an up-to-date issue description' do
      let(:issue_description) do
        <<~DESCRIPTION
          ### Reports (1)

          1. #{Time.new.utc.strftime('%F')}: #{test.ci_job_url} (pipeline-url)
        DESCRIPTION
      end

      it 'returns the job URL' do
        expect(subject).to eq(test.ci_job_url)
      end
    end

    context 'with a legacy issue description' do
      let(:issue_description) do
        <<~DESCRIPTION
          ### Stack Trace

          ```shell
          test
          ```

          First happened in #{test.ci_job_url}.
        DESCRIPTION
      end

      it 'returns the job URL' do
        expect(subject).to eq(test.ci_job_url)
      end
    end

    context 'with an invalid issue description' do
      let(:issue_description) do
        <<~DESCRIPTION
          ### Some other title

          1. Job URL: #{test.ci_job_url} (wrong formatting)
        DESCRIPTION
      end

      it 'does not return any job URLs' do
        expect(subject).to be_nil
      end
    end
  end
end
