# frozen_string_literal: true

RSpec.describe GitlabQuality::TestTooling::Report::Concerns::Utils do
  let(:reporter) { Class.new { include GitlabQuality::TestTooling::Report::Concerns::Utils }.new }

  describe '#title_from_test' do
    context 'when title is less than 255 chars' do
      let(:test) do
        GitlabQuality::TestTooling::TestResult::JsonTestResult.new(
          report: {
            'file_path' => 'qa/specs/features/browser_ui/3_create/repository/add_new_branch_rule_spec.rb',
            'full_description' => 'A test "with quotes" inside'
          }
        )
      end

      it 'returns well-formed title' do
        expect(reporter.title_from_test(test)).to eq("[Test] browser_ui/3_create/repository/add_new_branch_rule_spec.rb | A test with quotes inside")
      end
    end

    context 'when title is more than 255 chars' do
      let(:test) do
        GitlabQuality::TestTooling::TestResult::JsonTestResult.new(
          report: {
            'file_path' => 'qa/specs/features/browser_ui/3_create/repository/add_new_branch_rule_spec.rb',
            'full_description' => "A test \"with quotes\" inside #{'a' * 255}"
          }
        )
      end

      it 'returns well-formed title' do
        expect(reporter.title_from_test(test)).to eq("[Test] #{"browser_ui/3_create/repository/add_new_branch_rule_spec.rb | A test with quotes inside #{'a' * 255}"[...245]}...")
      end
    end
  end

  describe '#new_issue_title' do
    let(:test) do
      GitlabQuality::TestTooling::TestResult::JsonTestResult.new(
        report: {
          'file_path' => 'qa/specs/features/browser_ui/3_create/repository/add_new_branch_rule_spec.rb',
          'full_description' => 'A test "with quotes" inside'
        }
      )
    end

    it 'returns well-formed title' do
      expect(reporter.new_issue_title(test)).to eq("[Test] browser_ui/3_create/repository/add_new_branch_rule_spec.rb | A test with quotes inside")
    end
  end

  describe '#partial_file_path' do
    context 'when path is a FOSS rails test' do
      it 'returns the path as-is' do
        file_path = 'spec/lib/gitlab/database/pg_depend_spec.rb'

        expect(reporter.partial_file_path(file_path)).to eq(file_path)
      end
    end

    context 'when path is a EE rails test' do
      it 'returns the path as-is' do
        file_path = 'ee/spec/lib/gitlab/database/pg_depend_spec.rb'

        expect(reporter.partial_file_path(file_path)).to eq(file_path)
      end
    end

    context 'when path is a FOSS E2E test' do
      it 'returns the path as-is' do
        file_path = 'browser_ui/1_manage/login/login_via_oauth_and_oidc_with_gitlab_as_idp_spec.rb'

        expect(reporter.partial_file_path(file_path)).to eq(file_path)
      end
    end

    context 'when path is a EE E2E test' do
      it 'returns the path as-is' do
        file_path = 'ee/browser_ui/1_manage/login/login_via_oauth_and_oidc_with_gitlab_as_idp_spec.rb'

        expect(reporter.partial_file_path(file_path)).to eq(file_path)
      end
    end

    context 'when path is a full-path FOSS E2E test' do
      it 'returns the path as-is' do
        file_path = 'qa/specs/features/browser_ui/3_create/repository/add_new_branch_rule_spec.rb'

        expect(reporter.partial_file_path(file_path)).to eq('browser_ui/3_create/repository/add_new_branch_rule_spec.rb')
      end
    end

    context 'when path is a full-path EE E2E test' do
      it 'returns the path as-is' do
        file_path = 'qa/specs/features/ee/browser_ui/3_create/repository/add_new_branch_rule_spec.rb'

        expect(reporter.partial_file_path(file_path)).to eq('ee/browser_ui/3_create/repository/add_new_branch_rule_spec.rb')
      end
    end

    context 'when the path is generic' do
      it 'returns the path as-is' do
        file_path = 'foobar/specs/this/is/my/test_spec.rb'

        expect(reporter.partial_file_path(file_path)).to eq(file_path)
      end
    end

    context 'when path is unset' do
      it 'handles nil values' do
        file_path = nil
        expect(reporter.partial_file_path(file_path)).to be_nil
      end

      it 'handles an empty string' do
        file_path = ''
        expect(reporter.partial_file_path(file_path)).to eq('')
      end
    end
  end

  describe '#readable_duration' do
    let(:result) { reporter.readable_duration(argument) }

    context 'when duration is less than 1 second' do
      let(:argument) { 0.11111 }

      it { expect(result).to eq('0.11 second') }
    end

    context 'when duration is less than 60 seconds' do
      let(:argument) { 5.2 }

      it { expect(result).to eq('5.2 seconds') }
    end

    context 'when duration is exactly 60 seconds' do
      let(:argument) { 60 }

      it { expect(result).to eq('1 minute') }
    end

    context 'when duration is 60.02 seconds' do
      let(:argument) { 60.02 }

      it { expect(result).to eq('1 minute 0.02 second') }
    end

    context 'when duration is 65.5 seconds' do
      let(:argument) { 65.5 }

      it { expect(result).to eq('1 minute 5.5 seconds') }
    end

    context 'when duration is more than 2 minutes' do
      let(:argument) { 120.5 }

      it { expect(result).to eq('2 minutes 0.5 second') }
    end
  end
end
