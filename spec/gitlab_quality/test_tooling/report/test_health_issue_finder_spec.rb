# frozen_string_literal: true

RSpec.describe GitlabQuality::TestTooling::Report::TestHealthIssueFinder, :aggregate_failures do
  include_context 'with repository files client and content'

  let(:project)             { 'valid-project' }
  let(:input_file)          { File.expand_path("fixtures/rspec-reports/flaky/rspec-retry-5258318483_pass_and_fail.json", __dir__) }
  let(:health_problem_type) { 'failures' }
  let(:issue_class)         { Struct.new(:iid, :web_url, :state, :title, :description, :labels, keyword_init: true) }

  subject(:report) do
    described_class.new(
      health_problem_type: health_problem_type,
      input_files: [input_file],
      project: project,
      token: 'token'
    )
  end

  around do |example|
    ClimateControl.modify(
      CI_PROJECT_NAME: 'staging',
      CI_PIPELINE_URL: 'pipeline-url',
      CI_MERGE_REQUEST_IID: '42',
      CI_COMMIT_REF_NAME: 'master') do
      example.run
    end
  end

  before do
    allow(report.__send__(:gitlab)).to receive(:assert_user_permission!)

    # Silence outputs to stdout
    allow(report).to receive(:puts)
  end

  describe '#found_existing_unhealthy_test_issue?' do
    context 'when invoke! returns an issue URL' do
      before do
        allow(report).to receive(:invoke!).and_return('http://existing-issue.url')
      end

      it 'returns true' do
        expect(report.found_existing_unhealthy_test_issue?).to be(true)
      end
    end

    context 'when invoke! does not return an issue URL' do
      before do
        allow(report).to receive(:invoke!).and_return(nil)
      end

      it 'returns false' do
        expect(report.found_existing_unhealthy_test_issue?).to be(false)
      end
    end
  end

  describe '#applicable_tests' do
    context 'when health_problem_type is failures' do
      let(:health_problem_type) { 'failures' }

      it 'only returns failed tests' do
        expect(report.applicable_tests.count).to eq(1)
        expect(report.applicable_tests.first.status).to eq('failed')
      end
    end

    context 'when health_problem_type is pass-after-retry' do
      let(:health_problem_type) { 'pass-after-retry' }

      it 'only returns successful tests' do
        expect(report.applicable_tests.count).to eq(1)
        expect(report.applicable_tests.first.status).to eq('passed')
      end
    end

    context 'when health_problem_type is slow' do
      let(:health_problem_type) { 'slow' }

      it 'only returns successful tests' do
        expect(report.applicable_tests.count).to eq(1)
        expect(report.applicable_tests.first.status).to eq('passed')
      end
    end
  end

  describe '#invoke!' do
    RSpec.shared_examples "a test health issue finder" do |health_problem_type, search_labels|
      before do
        allow(report.__send__(:gitlab)).to receive(:find_issues).with(
          hash_including(
            options: hash_including(
              labels: search_labels
            )
          )
        ).and_return(found_issues)
      end

      let(:health_problem_type) { health_problem_type }
      let(:search_labels)       { search_labels }

      context 'when no issues are found' do
        let(:found_issues) { [] }

        it 'outputs a message to STDOUT to inform the user' do
          allow(report).to receive(:puts).and_call_original

          expect { report.invoke! }.to output("Did not find an existing test health issue of type #{health_problem_type}.\n").to_stdout
        end

        it 'returns nil' do
          expect(report.invoke!).to be_nil
        end
      end

      context 'when issues are found' do
        let(:existing_issue) do
          issue_class.new(
            iid: 0,
            description: '',
            web_url: 'http://existing-issue.url',
            labels: search_labels
          )
        end

        let(:found_issues) { [existing_issue] }

        it 'outputs a message to STDOUT to inform the user' do
          allow(report).to receive(:puts).and_call_original

          expect { report.invoke! }.to output(
            /Found an existing test health issue of type #{health_problem_type} for test/
          ).to_stdout
        end

        it 'returns the issue URL' do
          expect(report.invoke!).to eq('http://existing-issue.url')
        end
      end
    end

    context 'when health_problem_type is failures' do
      include_examples 'a test health issue finder', 'failures', ['test', 'test-health:failures']
    end

    context 'when health_problem_type is pass-after-retry' do
      include_examples 'a test health issue finder', 'pass-after-retry', ['test', 'test-health:pass-after-retry']
    end

    context 'when health_problem_type is slow' do
      include_examples 'a test health issue finder', 'slow', ['test', 'test-health:slow']
    end
  end
end
