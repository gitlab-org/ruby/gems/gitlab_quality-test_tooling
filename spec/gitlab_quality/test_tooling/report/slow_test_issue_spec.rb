# frozen_string_literal: true

RSpec.describe GitlabQuality::TestTooling::Report::SlowTestIssue, :aggregate_failures do
  include_context 'with repository files client and content'

  let(:project) { 'valid-project' }
  let(:input_file) { File.expand_path("fixtures/rspec-reports/rspec-4509805924.json", __dir__) }
  let(:expected_new_issue_description) do
    <<~DESCRIPTION
    ### Test metadata

    <!-- Don't modify this section as it's auto-generated -->
    | Field | Value |
    | ------ | ------ |
    | File URL | [`ee/spec/requests/api/visual_review_discussions_spec.rb#L177`](https://gitlab.com/#{project}/-/blob/master/ee/spec/requests/api/visual_review_discussions_spec.rb#L177) |
    | Filename | `ee/spec/requests/api/visual_review_discussions_spec.rb` |
    | Description | `` API::VisualReviewDiscussions when project is public behaves like accepting request without authentication behaves like handling merge request feedback creates a new note `` |
    | Test level | `background_migration` |
    | Hash | `56d5cd809c01070837b452622e5a88d272a09201a` |
    | Max expected duration | < 19.2 seconds |

    <!-- Don't modify this section as it's auto-generated -->
    DESCRIPTION
  end

  subject(:report) do
    described_class.new(
      input_files: [input_file],
      project: project,
      token: 'token'
    )
  end

  around do |example|
    ClimateControl.modify(
      CI_PROJECT_NAME: 'staging',
      CI_PIPELINE_URL: 'pipeline-url',
      CI_MERGE_REQUEST_IID: '42',
      CI_COMMIT_REF_NAME: 'master') do
      example.run
    end
  end

  before do
    allow(report.__send__(:gitlab)).to receive(:assert_user_permission!)

    # Silence outputs to stdout by default
    allow(subject).to receive(:puts)
  end

  describe '#invoke!' do
    context 'when there are slow tests found' do
      let(:issue_class) { Struct.new(:iid, :web_url, :state, :title, :description, :labels, keyword_init: true) }
      let(:issue_note_class) { Struct.new(:id, :body, keyword_init: true) }
      let(:labels_inference) { instance_double(GitlabQuality::TestTooling::LabelsInference) }

      before do
        allow(GitlabQuality::TestTooling::LabelsInference).to receive(:new).and_return(labels_inference)
        allow(labels_inference).to receive_messages(
          infer_labels_from_product_group: Set.new(['group-label']),
          infer_labels_from_feature_category: Set.new(['category-label'])
        )
      end

      context 'with no existing test issue' do
        let(:new_issue) { issue_class.new(iid: 0, web_url: 'http://new-issue.url', description: expected_new_issue_description, labels: []) }

        before do
          allow(report.__send__(:gitlab)).to receive_messages(
            find_issues: [],
            find_issue_notes: [],
            create_issue: nil,
            create_issue_note: nil
          )
        end

        it 'creates an issue and a note' do
          expect(report.__send__(:gitlab)).to receive(:create_issue)
            .with(
              title: "[Test] ee/spec/requests/api/visual_review_discussions_spec.rb | " \
                     "API::VisualReviewDiscussions when project is public behaves like accepting request " \
                     "without authentication behaves like handling merge request feedback creates a new note",
              description: expected_new_issue_description,
              labels: described_class::NEW_ISSUE_LABELS.to_a,
              confidential: false
            )
            .and_return(new_issue)

          expected_note =
            <<~DESCRIPTION
            #{described_class::REPORT_SECTION_HEADER} (1)

            1. #{Time.new.utc.strftime('%F')}: https://gitlab.com/gitlab-org/gitlab/-/jobs/4509805924 (pipeline-url) (28.98 seconds) ~"found:in MR"

            Slow tests were detected, please see the [test speed best practices guide](https://docs.gitlab.com/ee/development/testing_guide/best_practices.html#test-speed)
            to improve them. More context available about this issue in the [top slow tests guide](https://docs.gitlab.com/ee/development/testing_guide/best_practices.html#top-slow-tests).

            Add `allowed_to_be_slow: true` to the RSpec test if this is a legit slow test and close the issue.

            /label ~"slowness::4"
            /label ~"test" ~"rspec:slow test" ~"test-health:slow" ~"rspec profiling" ~"automation:bot-authored"
            DESCRIPTION

          expect(report.__send__(:gitlab)).to receive(:create_issue_note).with(
            iid: new_issue.iid,
            note: expected_note)

          report.invoke!
        end
      end

      context 'with existing test issue' do
        let(:test_report_line)             { '1. 2024-02-01: https://gitlab.com/gitlab-org/gitlab/-/jobs/5258318483 (pipeline-url)' }
        let(:initial_number_of_reports)    { 8 }
        let(:new_number_of_reports)        { initial_number_of_reports + 1 }
        let(:new_number_of_reports_capped) { [new_number_of_reports, described_class::DISPLAYED_HISTORY_REPORTS_THRESHOLD].min }

        # Report format when we're below 10 test reports. See Concerns::IssueReports.
        let(:initial_reports_note) do
          <<~DESCRIPTION
          #{described_class::REPORT_SECTION_HEADER} (#{initial_number_of_reports})

          #{(([test_report_line] * initial_number_of_reports)).join("\n")}

          Slow tests were detected, please see the [test speed best practices guide](https://docs.gitlab.com/ee/development/testing_guide/best_practices.html#test-speed)
          to improve them. More context available about this issue in the [top slow tests guide](https://docs.gitlab.com/ee/development/testing_guide/best_practices.html#top-slow-tests).

          Add `allowed_to_be_slow: true` to the RSpec test if this is a legit slow test and close the issue.
          DESCRIPTION
        end

        let(:existing_issue) do
          issue_class.new(
            iid: 0,
            description: expected_new_issue_description,
            web_url: 'http://existing-issue.url',
            labels: []
          )
        end

        let(:existing_note) do
          issue_note_class.new(
            id: 1,
            body: initial_reports_note
          )
        end

        let(:related_issue) do
          issue_class.new(
            iid: 2,
            description: expected_new_issue_description,
            web_url: 'http://related-issue.url',
            labels: []
          )
        end

        before do
          allow(report.__send__(:gitlab)).to receive_messages(
            find_issues: [existing_issue, related_issue],
            find_issue_notes: [],
            create_issue_note: nil,
            edit_issue_note: nil
          )
          allow(report.__send__(:gitlab)).to receive(:find_issue_notes).with(iid: existing_issue.iid).and_return([existing_note])
        end

        it 'does not create an issue nor update it' do
          expect(report.__send__(:gitlab)).not_to receive(:create_issue)
          expect(report.__send__(:gitlab)).not_to receive(:edit_issue)
          expect(report.__send__(:gitlab)).to receive(:edit_issue_note)
          expect(report.__send__(:gitlab)).to receive(:create_issue_note)

          report.invoke!
        end

        context 'when existing issue description is not up-to-date' do
          let(:existing_issue) do
            issue_class.new(
              iid: 0,
              web_url: 'http://existing-issue.url',
              description: '',
              labels: []
            )
          end

          it 'updates the issue description' do
            expect(report.__send__(:gitlab)).not_to receive(:create_issue)
            expect(report.__send__(:gitlab)).to receive(:edit_issue)
              .with(iid: existing_issue.iid, options: { description: expected_new_issue_description })
            expect(report.__send__(:gitlab)).to receive(:edit_issue_note)
            expect(report.__send__(:gitlab)).to receive(:create_issue_note)

            report.invoke!
          end
        end

        it 'updates the reports note header' do
          expect(report.__send__(:gitlab)).to receive(:edit_issue_note).with(
            issue_iid: 0,
            note_id: 1,
            note: /#{described_class::REPORT_SECTION_HEADER} \(#{new_number_of_reports}\)/o)

          report.invoke!
        end

        describe 'slowness labels' do
          let(:expected_updated_note) do
            <<~DESCRIPTION.chomp
              #{described_class::REPORT_SECTION_HEADER} (#{new_number_of_reports})

              Last 10 reports:

              1. #{Time.new.utc.strftime('%F')}: https://gitlab.com/gitlab-org/gitlab/-/jobs/4509805924 (pipeline-url) (28.98 seconds) ~"found:in MR"
              #{([test_report_line] * 9).join("\n")}

              <details><summary>With #{new_number_of_reports - 10} more reports (displaying up to 500 reports) </summary>

              #{([test_report_line] * [new_number_of_reports_capped - 10, 500].min).join("\n")}

              </details>

              Slow tests were detected, please see the [test speed best practices guide](https://docs.gitlab.com/ee/development/testing_guide/best_practices.html#test-speed)
              to improve them. More context available about this issue in the [top slow tests guide](https://docs.gitlab.com/ee/development/testing_guide/best_practices.html#top-slow-tests).

              Add `allowed_to_be_slow: true` to the RSpec test if this is a legit slow test and close the issue.

              /label ~"slowness::#{expected_slowness_level}"#{expected_group_and_category_labels}
              /label ~"test" ~"rspec:slow test" ~"test-health:slow" ~"rspec profiling" ~"automation:bot-authored"
              /relate http://related-issue.url
            DESCRIPTION
          end

          context 'when we are in slowness::4' do
            let(:initial_number_of_reports) { 500 }
            let(:expected_slowness_level) { 4 }
            let(:expected_group_and_category_labels) { nil }

            it 'adds the report to the existing reports note' do
              expect(report.__send__(:gitlab)).to receive(:edit_issue_note).with(
                issue_iid: existing_issue.iid,
                note_id: 1,
                note: expected_updated_note)

              report.invoke!
            end
          end

          context 'when we are in slowness::3' do
            let(:initial_number_of_reports) { 1500 }
            let(:expected_slowness_level) { 3 }
            let(:expected_group_and_category_labels) { nil }

            it 'adds the report to the existing reports note' do
              expect(report.__send__(:gitlab)).to receive(:edit_issue_note).with(
                issue_iid: existing_issue.iid,
                note_id: 1,
                note: expected_updated_note)

              report.invoke!
            end
          end

          context 'when we are in slowness::2' do
            let(:initial_number_of_reports) { 4000 }
            let(:expected_slowness_level) { 2 }
            let(:expected_group_and_category_labels) { ' ~"group-label" ~"category-label"' }

            it 'adds the report to the existing reports note' do
              expect(report.__send__(:gitlab)).to receive(:edit_issue_note).with(
                issue_iid: existing_issue.iid,
                note_id: 1,
                note: expected_updated_note)

              report.invoke!
            end
          end

          context 'when we are in slowness::1' do
            let(:initial_number_of_reports) { 7000 }
            let(:expected_slowness_level) { 1 }
            let(:expected_group_and_category_labels) { ' ~"group-label" ~"category-label"' }

            it 'adds the report to the existing reports note' do
              expect(report.__send__(:gitlab)).to receive(:edit_issue_note).with(
                issue_iid: existing_issue.iid,
                note_id: 1,
                note: expected_updated_note)

              report.invoke!
            end
          end
        end

        shared_examples 'reports note creation in the related issue' do |found_in_label|
          it 'creates a reports note' do
            expected_updated_note =
              <<~DESCRIPTION.chomp
              #{described_class::REPORT_SECTION_HEADER} (1)

              1. #{Time.new.utc.strftime('%F')}: https://gitlab.com/gitlab-org/gitlab/-/jobs/4509805924 (pipeline-url) (28.98 seconds) ~"#{found_in_label}"

              Slow tests were detected, please see the [test speed best practices guide](https://docs.gitlab.com/ee/development/testing_guide/best_practices.html#test-speed)
              to improve them. More context available about this issue in the [top slow tests guide](https://docs.gitlab.com/ee/development/testing_guide/best_practices.html#top-slow-tests).

              Add `allowed_to_be_slow: true` to the RSpec test if this is a legit slow test and close the issue.

              /label ~"slowness::4"
              /label ~"test" ~"rspec:slow test" ~"test-health:slow" ~"rspec profiling" ~"automation:bot-authored"
              /relate http://existing-issue.url
              DESCRIPTION

            expect(report.__send__(:gitlab)).to receive(:create_issue_note).with(
              iid: related_issue.iid,
              note: expected_updated_note)

            report.invoke!
          end
        end

        it_behaves_like 'reports note creation in the related issue', 'found:in MR'

        context 'with a master pipeline' do
          around do |example|
            ClimateControl.modify(
              CI_MERGE_REQUEST_IID: nil
            ) do
              example.run
            end
          end

          it_behaves_like 'reports note creation in the related issue', 'found:master'
        end
      end
    end

    context 'when no slow test found' do
      let(:input_file) { File.expand_path("fixtures/rspec-reports/rspec-4519210692.json", __dir__) }
      let(:project) { 'valid-project' }

      it 'does not create issue' do
        expect(report.__send__(:gitlab)).not_to receive(:create_issue)

        report.invoke!
      end
    end
  end
end
