# frozen_string_literal: true

RSpec.describe GitlabQuality::TestTooling::Report::KnapsackReportIssue do
  include StubENV

  describe '#invoke!' do
    let(:project)     { 'valid-project' }
    let(:issue_class) { Struct.new(:iid, :web_url, :state, :title, :description, :labels) }
    let(:new_issue)   { issue_class.new(0, 'http://new-issue-url') }
    let(:spec_file)   { 'spec/lib/gitlab/relative_positioning/mover_spec.rb' }

    let(:expected_report_relative_path) { 'fixtures/knapsack-reports/node_specs_expected_duration.json' }
    let(:actual_report_relative_path)   { 'fixtures/knapsack-reports/rspec_actual_knapsack_slow_report.json' }

    let(:expected_report_path) { File.expand_path(expected_report_relative_path, __dir__) }
    let(:actual_report_path)   { File.expand_path(actual_report_relative_path, __dir__) }

    let(:report) { described_class.new(token: 'token', input_files: actual_report_path, expected_report: expected_report_path, project: project) }
    let(:gitlab) { report.__send__(:gitlab) }

    before do
      allow(gitlab).to receive(:assert_user_permission!)
    end

    around do |example|
      ClimateControl.modify(
        CI_PIPELINE_IID: 'pipeline123',
        CI_PIPELINE_URL: 'pipeline-url',
        CI_PIPELINE_CREATED_AT: '2022-01-31T16:47:55Z',
        CI_JOB_URL: 'job-url',
        CI_JOB_NAME_SLUG: 'job-name',
        CI_COMMIT_REF_NAME: 'master') do
        example.run
      end
    end

    context 'when no test exceeded Knapsack expectation' do
      let(:actual_report_relative_path) { 'fixtures/knapsack-reports/rspec_actual_knapsack_fast_report.json' }

      it 'does not create issue' do
        expect(gitlab).not_to receive(:create_issue)

        expect { report.invoke! }
            .to output(/Reporting 0 spec files exceeding Knapsack expectation/).to_stdout
      end
    end

    context 'when Knapsack reported unexpectly slow run time' do
      context 'with new spec file exceeding Knapsack expectation' do
        include_context 'with repository files client and content'

        let(:repository_file_content) do
          <<~CONTENT
          RSpec.describe 'FooBar', feature_category: :code_review do
            it 'does something' do
              sleep 10
            end
          end
          CONTENT
        end

        let(:expected_new_issue_description) do
          <<~DESCRIPTION.chomp
          /epic https://gitlab.com/groups/gitlab-org/quality/engineering-productivity/-/epics/19

          ### Why was this issue created?

          [spec/lib/gitlab/relative_positioning/mover_spec.rb](https://gitlab.com/#{project}/-/blob/master/spec/lib/gitlab/relative_positioning/mover_spec.rb) was reported to have:

          1. exceeded Knapsack's expected runtime by at least 50%, and
          2. been identified as a notable pipeline bottleneck and a job timeout risk

          ### Suggested steps for investigation

          1. To reproduce in CI by running test files in the same order, you can follow the steps listed [here](https://docs.gitlab.com/ee/development/testing_guide/flaky_tests.html#recreate-job-failure-in-ci-by-forcing-the-job-to-run-the-same-set-of-test-files).
          1. Identify if a specific test case is stalling the run time. Hint: You can search the job's log for `Starting example group spec/lib/gitlab/relative_positioning/mover_spec.rb` and view the elapsed time after each test case in the proceeding lines starting with `[RSpecRunTime]`.
          1. If the test file is large, consider refactoring it into multiple files to allow better test parallelization across runners.
          1. If the run time cannot be fixed in time, consider quarantine the spec(s) to restore performance.

          ### Run time details

          - Reported from pipeline [pipeline123](pipeline-url) created at `2022-01-31T16:47:55Z`

          | Field | Value |
          | ------ | ------ |
          | Job URL| [job-name](job-url) |
          | Job total RSpec suite run time | expected: `63 minutes 32.82 seconds`, actual: `71 minutes 52.82 seconds` |
          | Spec file run time | expected: `6 minutes 52.22 seconds`, actual: `15 minutes 12.22 seconds` |
          | Spec file weight | `21.15%` of total suite run time |
          DESCRIPTION
        end

        before do
          allow(gitlab).to receive(:find_issues)
            .with(options: { labels: described_class::SEARCH_LABELS, not: { labels: [] }, search: spec_file, in: 'title' })
            .and_return([])
          stub_request(:get, 'https://about.gitlab.com/categories.json')
            .to_return(body:
            {
              code_review: { label: 'Category:Code Review', group: 'code_review' }
            }.to_json)
          stub_request(:get, 'https://about.gitlab.com/groups.json')
            .to_return(body:
            {
              code_review: { label: 'group::code review' }
            }.to_json)
        end

        it 'creates issue' do
          expect(gitlab).to receive(:create_issue)
            .with(
              title: "Job timeout risk: #{spec_file} ran much longer than expected",
              description: expected_new_issue_description,
              labels: described_class::NEW_ISSUE_LABELS.to_a + ['Category:Code Review', 'group::code review'],
              confidential: false
            )
            .and_return(new_issue)

          expect { report.invoke! }
            .to output(%r{Created new : http://new-issue-url}).to_stdout
        end
      end

      context 'with existing issue reported for the same spec file' do
        let(:existing_issue_description) { 'existing description' }
        let(:existing_issue_state) { 'opened' }
        let(:existing_issue) do
          issue_class.new(
            0,
            'http://existing-issue-url',
            existing_issue_state,
            "Job timeout risk: #{spec_file} ran much longer than expected",
            existing_issue_description,
            described_class::SEARCH_LABELS)
        end

        let(:updated_issue_description) do
          <<~DESCRIPTION.chomp
          #{existing_issue_description}

          - Reported from pipeline [pipeline123](pipeline-url) created at `2022-01-31T16:47:55Z`

          | Field | Value |
          | ------ | ------ |
          | Job URL| [job-name](job-url) |
          | Job total RSpec suite run time | expected: `63 minutes 32.82 seconds`, actual: `71 minutes 52.82 seconds` |
          | Spec file run time | expected: `6 minutes 52.22 seconds`, actual: `15 minutes 12.22 seconds` |
          | Spec file weight | `21.15%` of total suite run time |
          DESCRIPTION
        end

        let(:updated_issue) do
          issue_class.new(
            0,
            'http://existing-issue-url',
            'opened',
            "Job timeout risk: #{spec_file} ran much longer than expected",
            updated_issue_description,
            described_class::SEARCH_LABELS
          )
        end

        before do
          allow(gitlab).to receive(:find_issues).and_return([existing_issue])
          allow(gitlab).to receive(:edit_issue)
          allow(report).to receive(:puts)
        end

        context 'when the existing issue is closed' do
          let(:existing_issue_state) { 'closed' }

          it 'does not create a new issue' do
            expect(gitlab).not_to receive(:create_issue)

            report.invoke!
          end

          it 'reopens the existing issue' do
            expect(gitlab).to receive(:edit_issue)
                  .with(iid: 0, options: { description: updated_issue_description, state_event: 'reopen' })

            report.invoke!
          end

          it 'adds the report to the existing issue' do
            allow(report).to receive(:puts).and_call_original

            expect { report.invoke! }
              .to output(%r{Added a report in http://existing-issue-url}).to_stdout
          end
        end

        context 'when the existing issue is opened' do
          let(:existing_issue_state) { 'opened' }

          it 'does not create a new issue' do
            expect(gitlab).not_to receive(:create_issue)

            report.invoke!
          end

          it 'adds the report to the existing issue' do
            allow(report).to receive(:puts).and_call_original

            expect { report.invoke! }
              .to output(%r{Added a report in http://existing-issue-url}).to_stdout
          end
        end
      end
    end
  end
end
