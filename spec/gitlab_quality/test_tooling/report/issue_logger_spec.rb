# frozen_string_literal: true

RSpec.describe GitlabQuality::TestTooling::Report::IssueLogger do
  describe '#write' do
    let(:file_path) { 'tmp/rspec/related_issues.json' }
    let(:issue_class) { Struct.new(:iid, :web_url, :state, :title, :description, :labels) }
    let(:issue) { issue_class.new(0, 'http://issue.url') }
    let(:another_issue) { issue_class.new(0, 'http://another-issue.url') }
    let(:test) do
      GitlabQuality::TestTooling::TestResult::JsonTestResult.new(
        report: {
          'testcase' => 'testcase_url',
          'file_path' => 'test_file_path',
          'full_description' => 'test_description',
          'ci_job_url' => 'http://ci_job.url'
        }
      )
    end

    around do |example|
      example.run
    ensure
      FileUtils.rm(file_path)
    end

    subject { described_class.new(file_path: file_path) }

    it 'writes the job url and related test in file from collected data' do
      subject.collect(test, [issue])
      subject.write

      expect(JSON.parse(File.read(file_path))).to eq(
        {
          test.ci_job_url => [issue.web_url]
        })
    end

    it 'writes the issue for the same job URL for existing entry' do
      subject.collect(test, [issue])
      subject.write

      expect(JSON.parse(File.read(file_path))).to eq(
        {
          test.ci_job_url => [issue.web_url]
        })

      subject.collect(test, [another_issue])
      subject.write

      expect(JSON.parse(File.read(file_path))).to eq(
        {
          test.ci_job_url => [issue.web_url, another_issue.web_url]
        })
    end

    it 'writes nothing if there is no data collected' do
      subject.write

      expect(JSON.parse(File.read(file_path))).to eq({})
    end
  end
end
