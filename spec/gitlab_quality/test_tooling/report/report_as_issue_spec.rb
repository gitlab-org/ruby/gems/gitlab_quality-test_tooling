# frozen_string_literal: true

RSpec.describe GitlabQuality::TestTooling::Report::ReportAsIssue do
  let(:klass) { Class.new(described_class) }

  describe '#invoke!' do
    let(:project) { 'valid-project' }
    let(:test_file_full) { 'qa/specs/features/browser_ui/stage/test_spec.rb' }
    let(:test_file_partial) { 'browser_ui/stage/test_spec.rb' }

    it 'checks that a project was provided' do
      subject = klass.new(token: 'token', input_files: 'file')

      expect { subject.invoke! }
        .to output(%r{Please provide a valid project ID or path with the `-p/--project` option!}).to_stderr
        .and raise_error(SystemExit)
    end

    it 'checks that input files exist' do
      subject = klass.new(token: 'token', input_files: 'no-file', project: project)

      expect { subject.invoke! }
        .to output(/Please provide valid JUnit report files. No files were found matching `no-file`/).to_stderr
        .and raise_error(SystemExit)
    end

    context 'when validating user permissions' do
      subject { klass.new(token: 'token', input_files: 'file', project: project) }

      before do
        allow(subject).to receive(:assert_input_files!)
        allow(subject).to receive(:run!)
      end

      it 'checks that the user has at least Reporter access to the project' do
        expect(subject.__send__(:gitlab)).to receive(:assert_user_permission!)

        subject.invoke!
      end
    end
  end
end
