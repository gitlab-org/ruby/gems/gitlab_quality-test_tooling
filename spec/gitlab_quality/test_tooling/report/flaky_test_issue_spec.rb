# frozen_string_literal: true

RSpec.describe GitlabQuality::TestTooling::Report::FlakyTestIssue, :aggregate_failures do
  include_context 'with repository files client and content'

  let(:project)           { 'valid-project' }
  let(:input_file)        { File.expand_path("fixtures/rspec-reports/flaky/rspec-retry-5258318483_pass_and_fail.json", __dir__) }
  let(:base_issue_labels) { nil }
  let(:expected_new_issue_description) do
    <<~DESCRIPTION
    ### Test metadata

    <!-- Don't modify this section as it's auto-generated -->
    | Field | Value |
    | ------ | ------ |
    | File URL | [`ee/spec/lib/gitlab/geo_spec.rb#L26`](https://gitlab.com/#{project}/-/blob/master/ee/spec/lib/gitlab/geo_spec.rb#L26) |
    | Filename | `ee/spec/lib/gitlab/geo_spec.rb` |
    | Description | `` Gitlab::Geo.current_node returns a GeoNode instance `` |
    | Test level | `unit` |
    | Hash | `4a083567bd2dcbaa5c6ffcd32a1d6c58a7327dc80` |
    | Max expected duration | < 27.12 seconds |

    <!-- Don't modify this section as it's auto-generated -->
    DESCRIPTION
  end

  subject(:report) do
    described_class.new(
      base_issue_labels: base_issue_labels,
      input_files: [input_file],
      project: project,
      token: 'token'
    )
  end

  around do |example|
    ClimateControl.modify(
      CI_PROJECT_NAME: 'staging',
      CI_PIPELINE_URL: 'pipeline-url',
      CI_MERGE_REQUEST_IID: '42',
      CI_COMMIT_REF_NAME: 'master') do
      example.run
    end
  end

  before do
    allow(report.__send__(:gitlab)).to receive(:assert_user_permission!)

    # Silence outputs to stdout by default
    allow(subject).to receive(:puts)
  end

  describe '#invoke!' do
    context 'when no tests passed' do
      let(:input_file) { File.expand_path("fixtures/rspec-reports/flaky/rspec-retry-5258318483_all_failed.json", __dir__) }

      it 'does not create an issue' do
        expect(report.__send__(:gitlab)).not_to receive(:create_issue)
        expect(report.__send__(:gitlab)).not_to receive(:edit_issue_note)

        report.invoke!
      end
    end

    context 'when there are flaky tests found' do
      let(:issue_class)       { Struct.new(:iid, :web_url, :state, :title, :description, :labels, keyword_init: true) }
      let(:issue_note_class)  { Struct.new(:id, :body, keyword_init: true) }
      let(:labels_inference)  { instance_double(GitlabQuality::TestTooling::LabelsInference) }
      let(:new_issue)         { issue_class.new(iid: 0, web_url: 'http://new-issue.url', labels: []) }

      before do
        allow(GitlabQuality::TestTooling::LabelsInference).to receive(:new).and_return(labels_inference)
        allow(labels_inference).to receive_messages(
          infer_labels_from_product_group: Set.new(['group-label']),
          infer_labels_from_feature_category: Set.new(['category-label'])
        )
      end

      context 'with no existing test issue' do
        before do
          allow(report.__send__(:gitlab)).to receive_messages(
            find_issues: [],
            find_issue_notes: [],
            create_issue: nil,
            create_issue_note: nil
          )
        end

        it 'creates an issue and a note' do
          expect(report.__send__(:gitlab)).to receive(:create_issue)
            .with(
              title: "[Test] ee/spec/lib/gitlab/geo_spec.rb | Gitlab::Geo.current_node returns a GeoNode instance",
              description: expected_new_issue_description,
              labels: described_class::NEW_ISSUE_LABELS.to_a,
              confidential: false
            )
            .and_return(new_issue)

          expected_note =
            <<~DESCRIPTION
            #{described_class::REPORT_SECTION_HEADER} (1)

            1. #{Time.new.utc.strftime('%F')}: https://gitlab.com/gitlab-org/gitlab/-/jobs/5258318483 (pipeline-url) ~"found:in MR"

            Flaky tests were detected. Please refer to the [Flaky tests reproducibility instructions](https://docs.gitlab.com/ee/development/testing_guide/flaky_tests.html#how-to-reproduce-a-flaky-test-locally)
            to learn more about how to reproduce them.

            /label ~"flakiness::4"
            /label ~"test" ~"failure::flaky-test" ~"test-health:pass-after-retry" ~"automation:bot-authored"
            DESCRIPTION

          expect(report.__send__(:gitlab)).to receive(:create_issue_note).with(
            iid: new_issue.iid,
            note: expected_note)

          report.invoke!
        end

        describe '--base-issue-labels' do
          context 'with the --base-issue-labels flag set' do
            let(:base_issue_labels) { ['custom label', 'other custom label'] }

            it 'creates an issue with the additional flag' do
              expect(report.__send__(:gitlab)).to receive(:create_issue)
                .with(
                  title: "[Test] ee/spec/lib/gitlab/geo_spec.rb | Gitlab::Geo.current_node returns a GeoNode instance",
                  description: expected_new_issue_description,
                  labels: base_issue_labels + described_class::NEW_ISSUE_LABELS.to_a,
                  confidential: false
                )
                .and_return(new_issue)

              report.invoke!
            end
          end

          context 'without the --base-issue-labels flag set' do
            let(:base_issue_labels) { nil }

            it 'creates an issue with the regular labels' do
              expect(report.__send__(:gitlab)).to receive(:create_issue)
                .with(
                  title: "[Test] ee/spec/lib/gitlab/geo_spec.rb | Gitlab::Geo.current_node returns a GeoNode instance",
                  description: expected_new_issue_description,
                  labels: described_class::NEW_ISSUE_LABELS.to_a,
                  confidential: false
                )
                .and_return(new_issue)

              report.invoke!
            end
          end
        end
      end

      context 'with existing test issues' do
        let(:test_report_line)             { '1. 2024-02-01: https://gitlab.com/gitlab-org/gitlab/-/jobs/5258318483 (pipeline-url)' }
        let(:initial_number_of_reports)    { 8 }
        let(:new_number_of_reports)        { initial_number_of_reports + 1 }
        let(:new_number_of_reports_capped) { [new_number_of_reports, described_class::DISPLAYED_HISTORY_REPORTS_THRESHOLD].min }

        # Report format when we're below 10 test reports. See Concerns::IssueReports.
        let(:initial_reports_note) do
          <<~DESCRIPTION
          #{described_class::REPORT_SECTION_HEADER} (#{initial_number_of_reports})

          #{(([test_report_line] * initial_number_of_reports)).join("\n")}

          Flaky tests were detected. Please refer to the [Flaky tests reproducibility instructions](https://docs.gitlab.com/ee/development/testing_guide/flaky_tests.html#how-to-reproduce-a-flaky-test-locally)
          to learn more about how to reproduce them.
          DESCRIPTION
        end

        let(:existing_issue) do
          issue_class.new(
            iid: 0,
            description: expected_new_issue_description,
            web_url: 'http://existing-issue.url',
            labels: ['quarantine']
          )
        end

        let(:existing_note) do
          issue_note_class.new(
            id: 1,
            body: initial_reports_note
          )
        end

        let(:related_issue) do
          issue_class.new(
            iid: 2,
            description: expected_new_issue_description,
            web_url: 'http://related-issue.url',
            labels: []
          )
        end

        before do
          allow(report.__send__(:gitlab)).to receive_messages(
            find_issues: [existing_issue, related_issue],
            find_issue_notes: [],
            create_issue_note: nil,
            edit_issue_note: nil
          )
          allow(report.__send__(:gitlab)).to receive(:find_issue_notes).with(iid: existing_issue.iid).and_return([existing_note])
        end

        it 'does not create an issue nor update it' do
          expect(report.__send__(:gitlab)).not_to receive(:create_issue)
          expect(report.__send__(:gitlab)).not_to receive(:edit_issue)
          expect(report.__send__(:gitlab)).to receive(:edit_issue_note)
          expect(report.__send__(:gitlab)).to receive(:create_issue_note)

          report.invoke!
        end

        context 'when existing issue description is not up-to-date' do
          let(:existing_issue) do
            issue_class.new(
              iid: 0,
              web_url: 'http://existing-issue.url',
              description: '',
              labels: []
            )
          end

          it 'updates the issue description' do
            expect(report.__send__(:gitlab)).not_to receive(:create_issue)
            expect(report.__send__(:gitlab)).to receive(:edit_issue)
              .with(iid: existing_issue.iid, options: { description: expected_new_issue_description })
            expect(report.__send__(:gitlab)).to receive(:edit_issue_note)
            expect(report.__send__(:gitlab)).to receive(:create_issue_note)

            report.invoke!
          end
        end

        it 'updates the reports note header' do
          expect(report.__send__(:gitlab)).to receive(:edit_issue_note).with(
            issue_iid: 0,
            note_id: 1,
            note: /#{described_class::REPORT_SECTION_HEADER} \(#{new_number_of_reports}\)/o)

          report.invoke!
        end

        describe 'flakiness labels' do
          let(:expected_updated_note) do
            <<~DESCRIPTION.chomp
              #{described_class::REPORT_SECTION_HEADER} (#{new_number_of_reports})

              Last 10 reports:

              1. #{Time.new.utc.strftime('%F')}: https://gitlab.com/gitlab-org/gitlab/-/jobs/5258318483 (pipeline-url) ~"found:in MR"
              #{([test_report_line] * 9).join("\n")}

              <details><summary>With #{new_number_of_reports - 10} more reports (displaying up to 500 reports) </summary>

              #{([test_report_line] * [new_number_of_reports_capped - 10, 500].min).join("\n")}

              </details>

              Flaky tests were detected. Please refer to the [Flaky tests reproducibility instructions](https://docs.gitlab.com/ee/development/testing_guide/flaky_tests.html#how-to-reproduce-a-flaky-test-locally)
              to learn more about how to reproduce them.

              /label ~"flakiness::#{expected_flakiness_level}"#{expected_group_and_category_labels}
              /label ~"test" ~"failure::flaky-test" ~"test-health:pass-after-retry" ~"automation:bot-authored"
              /relate http://related-issue.url
            DESCRIPTION
          end

          context 'when we are in flakiness::4' do
            let(:initial_number_of_reports)          { 11 }
            let(:expected_flakiness_level)           { 4 }
            let(:expected_group_and_category_labels) { nil }

            it 'adds the report to the existing reports note' do
              expect(report.__send__(:gitlab)).to receive(:edit_issue_note).with(
                issue_iid: existing_issue.iid,
                note_id: 1,
                note: expected_updated_note)

              report.invoke!
            end
          end

          context 'when we are in flakiness::3' do
            let(:initial_number_of_reports)          { 30 }
            let(:expected_flakiness_level)           { 3 }
            let(:expected_group_and_category_labels) { nil }

            it 'adds the report to the existing reports note' do
              expect(report.__send__(:gitlab)).to receive(:edit_issue_note).with(
                issue_iid: existing_issue.iid,
                note_id: 1,
                note: expected_updated_note)

              report.invoke!
            end
          end

          context 'when we are in flakiness::2' do
            let(:initial_number_of_reports)          { 350 }
            let(:expected_flakiness_level)           { 2 }
            let(:expected_group_and_category_labels) { ' ~"group-label" ~"category-label"' }

            it 'adds the report to the existing reports note' do
              expect(report.__send__(:gitlab)).to receive(:edit_issue_note).with(
                issue_iid: existing_issue.iid,
                note_id: 1,
                note: expected_updated_note)

              report.invoke!
            end
          end

          context 'when we are in flakiness::1' do
            let(:initial_number_of_reports)          { 1500 }
            let(:expected_flakiness_level)           { 1 }
            let(:expected_group_and_category_labels) { ' ~"group-label" ~"category-label"' }

            it 'adds the report to the existing reports note' do
              expect(report.__send__(:gitlab)).to receive(:edit_issue_note).with(
                issue_iid: existing_issue.iid,
                note_id: 1,
                note: expected_updated_note)

              report.invoke!
            end
          end
        end

        shared_examples 'reports note creation in the related issue' do |found_in_label|
          it 'creates a reports note' do
            expected_updated_note =
              <<~DESCRIPTION.chomp
              #{described_class::REPORT_SECTION_HEADER} (1)

              1. #{Time.new.utc.strftime('%F')}: https://gitlab.com/gitlab-org/gitlab/-/jobs/5258318483 (pipeline-url) ~"#{found_in_label}"

              Flaky tests were detected. Please refer to the [Flaky tests reproducibility instructions](https://docs.gitlab.com/ee/development/testing_guide/flaky_tests.html#how-to-reproduce-a-flaky-test-locally)
              to learn more about how to reproduce them.

              /label ~"flakiness::4"
              /label ~"test" ~"failure::flaky-test" ~"test-health:pass-after-retry" ~"automation:bot-authored"
              /relate http://existing-issue.url
              DESCRIPTION

            expect(report.__send__(:gitlab)).to receive(:create_issue_note).with(
              iid: related_issue.iid,
              note: expected_updated_note)

            report.invoke!
          end
        end

        it_behaves_like 'reports note creation in the related issue', 'found:in MR'

        context 'with a master pipeline' do
          around do |example|
            ClimateControl.modify(
              CI_MERGE_REQUEST_IID: nil
            ) do
              example.run
            end
          end

          it_behaves_like 'reports note creation in the related issue', 'found:master'
        end
      end
    end
  end
end
