# frozen_string_literal: true

RSpec.describe GitlabQuality::TestTooling::GitlabClient::CommitsClient do
  subject(:commits_client) { described_class.new(token: token, project: project_id) }

  let(:project_id) { 1 }
  let(:client) { instance_double(Gitlab::Client) }
  let(:token) { "token" }
  let(:endpoint) { GitlabQuality::TestTooling::Runtime::Env.gitlab_api_base }

  let(:branch_name) { 'branch_name' }
  let(:file_path) { '/path/to/file' }
  let(:new_content) { 'some content' }
  let(:message) { 'commit message' }
  let(:web_url) { 'http://web/url message' }
  let(:commit_id) { 'commit_id' }
  let(:commit) { Gitlab::ObjectifiedHash.new(id: commit_id, web_url: web_url) }

  describe '#create' do
    before do
      allow(Gitlab).to receive(:client).and_return(client)
      allow(client).to receive(:create_commit).and_return(commit)
    end

    it 'creates a commit' do
      expect(commits_client.create(branch_name, file_path, new_content, message)).to eq(commit)
      expect(Gitlab).to have_received(:client).with(endpoint: endpoint, private_token: token)
      expect(client).to have_received(:create_commit).with(project_id, branch_name, message, [
        { action: :update, file_path: file_path, content: new_content }
      ])
    end
  end
end
