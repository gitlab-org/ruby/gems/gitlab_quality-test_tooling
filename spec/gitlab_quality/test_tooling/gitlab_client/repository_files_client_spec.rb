# frozen_string_literal: true

RSpec.describe GitlabQuality::TestTooling::GitlabClient::RepositoryFilesClient do
  subject(:repository_files_client) { described_class.new(file_path: file_path, ref: ref, token: token, project: project_id) }

  let(:project_id) { 1 }
  let(:ref) { 'branch' }
  let(:client) { instance_double(Gitlab::Client) }
  let(:token) { "token" }
  let(:endpoint) { GitlabQuality::TestTooling::Runtime::Env.gitlab_api_base }

  let(:file_path) { '/path/to/file' }
  let(:file_contents) { 'file_contents' }

  before do
    allow(Gitlab).to receive(:client).and_return(client)
    allow(client).to receive(:file_contents).and_return(file_contents)
  end

  describe '#file_contents' do
    it 'returns file contents' do
      expect(repository_files_client.file_contents).to eq(file_contents)
      expect(Gitlab).to have_received(:client).with(endpoint: endpoint, private_token: token)

      expect(client).to have_received(:file_contents).with(project_id, file_path.gsub(%r{^/}, ""), ref)
    end
  end

  describe '#file_contents_at_line' do
    context 'when returned file content is nil' do
      let(:file_contents) { nil }

      it 'returns nil' do
        expect(repository_files_client.file_contents_at_line(0)).to be_nil
      end
    end

    context 'when returned file contents have 1 line' do
      it 'returns the content from line 1' do
        expect(repository_files_client.file_contents_at_line(1)).to eq(file_contents)
      end
    end

    context 'when returned file contents have multiple lines' do
      let(:file_contents) { "file_contents1\nfile_contents2\nfile_contents3" }

      it 'returns the content from line 2' do
        expect(repository_files_client.file_contents_at_line(2)).to eq('file_contents2')
      end
    end

    context 'with 404 response error' do
      before do
        allow(client).to receive(:file_contents).and_raise(
          Gitlab::Error::NotFound.new(response_double)
        )
      end

      # rubocop:disable RSpec/VerifiedDoubles
      let(:response_double) do
        double('Response',
          code: 404,
          request: double('Request', base_uri: '', path: ''),
          parsed_response: { message: 'error' })
      end
      # rubocop:enable RSpec/VerifiedDoubles

      it 'returns nil' do
        expect(repository_files_client.file_contents_at_line(2)).to be_nil
      end
    end
  end
end
