# frozen_string_literal: true

require 'gitlab'

RSpec.describe GitlabQuality::TestTooling::GitlabClient::IssuesClient do
  let(:project) { 'project' }

  context 'with valid input' do
    let(:gitlab_client) { described_class.new(token: 'token', project: project) }

    context 'when the GitLab client is configured' do
      it 'passes the token to the GitLab client' do
        expect(Gitlab).to receive(:client).with(endpoint: anything, private_token: 'token')

        gitlab_client.__send__(:client)
      end

      it 'uses the default base API URL' do
        expect(Gitlab).to receive(:client).with(endpoint: 'https://gitlab.com/api/v4', private_token: anything)

        gitlab_client.__send__(:client)
      end
    end

    context 'when the base API URL is specified as an environment variable' do
      around do |example|
        ClimateControl.modify(GITLAB_API_BASE: 'http://another.gitlab.url') { example.run }
      end

      it 'uses the specified URL' do
        expect(Gitlab).to receive(:client).with(endpoint: 'http://another.gitlab.url', private_token: 'token')

        gitlab_client.__send__(:client)
      end
    end

    describe '#assert_user_permission!' do
      let(:user_id) { 42 }
      let(:user) { Struct.new(:id, :username).new(user_id, 'john_doe') }
      let(:member) { Struct.new(:access_level).new(10) }

      before do
        allow(gitlab_client.__send__(:client)).to receive(:user).and_return(user)
      end

      it 'checks that the user has at least Reporter access to the project' do
        expect(gitlab_client.__send__(:client))
          .to receive(:team_member).with(project, user_id)
          .and_return(member)

        expect { gitlab_client.assert_user_permission! }
          .to output(
            "#{user.username} must have at least Reporter access to the project '#{project}' to use this feature. " \
            "Current access level: #{member.access_level}\n").to_stderr
          .and raise_error(SystemExit)
      end

      it 'checks that the user is a member of the project' do
        stub_const("Gitlab::Error::NotFound", RuntimeError)

        expect(gitlab_client.__send__(:client))
          .to receive(:team_member).with(project, user_id)
          .and_raise(Gitlab::Error::NotFound)

        expect { gitlab_client.assert_user_permission! }
          .to output("#{user.username} must be a member of the '#{project}' project.\n").to_stderr
          .and raise_error(SystemExit)
      end
    end

    describe '#find_issues' do
      let(:iid)     { nil }
      let(:options) { { first_option: true } }
      let(:gitlab_response) { instance_double(Gitlab::PaginatedResponse, auto_paginate: []) }

      subject { gitlab_client.find_issues(iid: iid, options: options) }

      context 'when an issue iid is given' do
        let(:iid) { 1234 }

        it 'calls the #issue GitLab method' do
          expect(gitlab_client.__send__(:client)).to receive(:issue).with(project, iid).and_return(gitlab_response)

          subject
        end
      end

      context 'when no issue iid is given' do
        let(:iid) { nil }

        it 'calls the #issues GitLab method' do
          expect(gitlab_client.__send__(:client)).to receive(:issues).with(project, options).and_return(gitlab_response)

          subject
        end
      end
    end

    describe '#find_issue_notes' do
      let(:gitlab_response) { instance_double(Gitlab::PaginatedResponse, auto_paginate: []) }

      it 'calls the #issue_notes GitLab method' do
        expect(gitlab_client.__send__(:client))
          .to receive(:issue_notes)
          .with(project, 1, order_by: 'created_at', sort: 'asc')
          .and_return(gitlab_response)

        gitlab_client.find_issue_notes(iid: 1)
      end
    end

    describe '#find_issue_discussions' do
      let(:gitlab_response) { instance_double(Gitlab::PaginatedResponse, auto_paginate: []) }

      it 'calls the #issue_notes GitLab method' do
        expect(gitlab_client.__send__(:client))
          .to receive(:issue_discussions)
          .with(project, 1, order_by: 'created_at', sort: 'asc')
          .and_return(gitlab_response)

        gitlab_client.find_issue_discussions(iid: 1)
      end
    end

    describe '#create_issue' do
      it 'calls the #issue_notes GitLab method' do
        expect(gitlab_client.__send__(:client))
          .to receive(:create_issue)
          .with(project, 'foo', { issue_type: 'issue', description: 'bar', labels: [], confidential: false })

        gitlab_client.create_issue(title: 'foo', description: 'bar', labels: [])
      end
    end

    describe '#edit_issue' do
      it 'calls the #edit_issue GitLab method' do
        expect(gitlab_client.__send__(:client))
          .to receive(:edit_issue)
          .with(project, 1, { title: 'bar' })

        gitlab_client.edit_issue(iid: 1, options: { title: 'bar' })
      end
    end

    describe '#create_issue_note' do
      it 'calls the #create_issue_note GitLab method' do
        expect(gitlab_client.__send__(:client)).to receive(:create_issue_note).with(project, 1, 'note')

        gitlab_client.create_issue_note(iid: 1, note: 'note')
      end
    end

    describe '#edit_issue_note' do
      it 'calls the #edit_issue_note GitLab method' do
        expect(gitlab_client.__send__(:client)).to receive(:edit_issue_note).with(project, 1, 2, 'note')

        gitlab_client.edit_issue_note(issue_iid: 1, note_id: 2, note: 'note')
      end
    end

    describe '#create_issue_discussion' do
      it 'calls the #create_issue_discussion GitLab method' do
        expect(gitlab_client.__send__(:client)).to receive(:create_issue_discussion).with(project, 1, body: 'note')

        gitlab_client.create_issue_discussion(iid: 1, note: 'note')
      end
    end

    describe '#add_note_to_issue_discussion_as_thread' do
      it 'calls the #add_note_to_issue_discussion_as_thread GitLab method' do
        expect(gitlab_client.__send__(:client)).to receive(:add_note_to_issue_discussion_as_thread).with(project, 1, 2, body: 'note')

        gitlab_client.add_note_to_issue_discussion_as_thread(iid: 1, discussion_id: 2, note: 'note')
      end
    end

    describe '#find_user_id' do
      it 'calls the #find_user_id GitLab method' do
        expect(gitlab_client.__send__(:client)).to receive(:users).with(username: 'foo').and_return([{ 'id' => 1 }])

        expect(gitlab_client.find_user_id(username: 'foo')).to eq(1)
      end

      context 'when user is not found' do
        it 'calls the #find_user_id GitLab method' do
          expect(gitlab_client.__send__(:client)).to receive(:users).with(username: 'foo').and_return([])

          expect(gitlab_client.find_user_id(username: 'foo')).to be_nil
        end
      end
    end

    describe '#upload_file' do
      it 'calls the #upload_file GitLab method' do
        expect(gitlab_client.__send__(:client)).to receive(:upload_file).with(project, 'foo')

        gitlab_client.upload_file(file_fullpath: 'foo')
      end
    end

    describe '#find_pipeline' do
      let(:gitlab_response) { instance_double(Gitlab::PaginatedResponse, auto_paginate: []) }

      it 'calls the #find_pipeline GitLab method' do
        expect(gitlab_client.__send__(:client)).to receive(:pipeline).with(project, 12).and_return(gitlab_response)

        gitlab_client.find_pipeline(project, 12)
      end
    end

    describe '#find_commit' do
      let(:gitlab_response) { instance_double(Gitlab::PaginatedResponse, auto_paginate: []) }

      it 'calls the #commit GitLab method' do
        expect(gitlab_client.__send__(:client))
          .to receive(:commit)
          .with(project, 'abc123')
          .and_return(gitlab_response)

        gitlab_client.find_commit(project, 'abc123')
      end
    end

    describe '#find_commit_parent' do
      # rubocop:disable RSpec/VerifiedDoubles
      let(:commit_response) { double(parent_ids: %w[bar baz]) }
      # rubocop:enable RSpec/VerifiedDoubles

      before do
        allow_any_instance_of(Gitlab::ObjectifiedHash).to receive(:method_missing) do |_, method, *|
          { parent_ids: %w[bar baz] }[method]
        end
      end

      it 'returns the last parent id' do
        allow(gitlab_client.__send__(:client)).to receive(:commit).with(project, 'foo').and_return(commit_response)

        result = gitlab_client.find_commit_parent(project, 'foo')
        expect(result).to eq('baz')
      end

      context 'when commit has no parents' do
        # rubocop:disable RSpec/VerifiedDoubles
        let(:commit_response) { double(parent_ids: []) }
        # rubocop:enable RSpec/VerifiedDoubles

        it 'returns nil' do
          allow(gitlab_client.__send__(:client)).to receive(:commit).with(project, 'foo').and_return(commit_response)

          result = gitlab_client.find_commit_parent(project, 'foo')
          expect(result).to be_nil
        end
      end
    end

    describe '#find_commit_diff' do
      let(:gitlab_response) { instance_double(Gitlab::PaginatedResponse, auto_paginate: []) }

      it 'calls the #commit_diff GitLab method' do
        expect(gitlab_client.__send__(:client))
          .to receive(:commit_diff)
          .with(project, 'abc123')
          .and_return(gitlab_response)

        gitlab_client.find_commit_diff(project, 'abc123')
      end
    end

    describe '#find_deployments' do
      let(:gitlab_response) { instance_double(Gitlab::PaginatedResponse, auto_paginate: []) }

      it 'calls the #deployments GitLab method with default parameters' do
        expect(gitlab_client.__send__(:client))
          .to receive(:deployments)
          .with(
            project,
            environment: 'production',
            status: 'success',
            order_by: 'id',
            sort: 'desc'
          )
          .and_return(gitlab_response)

        gitlab_client.find_deployments(
          project,
          environment: 'production',
          status: 'success'
        )
      end

      it 'calls the #deployments GitLab method with custom parameters' do
        expect(gitlab_client.__send__(:client))
          .to receive(:deployments)
          .with(
            project,
            environment: 'staging',
            status: 'failed',
            order_by: 'created_at',
            sort: 'asc'
          )
          .and_return(gitlab_response)

        gitlab_client.find_deployments(
          project,
          environment: 'staging',
          status: 'failed',
          order_by: 'created_at',
          sort: 'asc'
        )
      end
    end
  end
end
