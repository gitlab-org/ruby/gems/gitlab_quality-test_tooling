# frozen_string_literal: true

RSpec.describe GitlabQuality::TestTooling::GitlabClient::JobsClient do
  subject(:jobs_client) { described_class.new(token: token, project: project_id) }

  let(:client) { instance_double(Gitlab::Client) }
  let(:token) { "token" }
  let(:project_id) { 1 }
  let(:pipeline_id) { 10 }
  let(:jobs) { [Gitlab::ObjectifiedHash.new(name: "rspec:other", stage: "test", failure_reason: "script failure")] }
  let(:endpoint) { GitlabQuality::TestTooling::Runtime::Env.gitlab_api_base }

  describe '#pipeline_jobs' do
    before do
      allow(Gitlab).to receive(:client) { client }
      allow(client).to receive(:pipeline_jobs) { jobs }
    end

    it 'returns jobs' do
      expect(jobs_client.pipeline_jobs(pipeline_id: pipeline_id, scope: 'failed')).to eq(jobs)
      expect(Gitlab).to have_received(:client).with(endpoint: endpoint, private_token: token)
      expect(client).to have_received(:pipeline_jobs).with(project_id, pipeline_id, scope: "failed")
    end
  end
end
