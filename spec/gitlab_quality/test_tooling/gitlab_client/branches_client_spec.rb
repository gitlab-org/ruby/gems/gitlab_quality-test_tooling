# frozen_string_literal: true

RSpec.describe GitlabQuality::TestTooling::GitlabClient::BranchesClient do
  subject(:branches_client) { described_class.new(token: token, project: project_id) }

  let(:project_id) { 1 }
  let(:client) { instance_double(Gitlab::Client) }
  let(:token) { "token" }
  let(:branch_name) { 'branch_name' }
  let(:ref) { 'main' }
  let(:web_url) { 'http://web/url' }
  let(:branch) { Gitlab::ObjectifiedHash.new(name: branch_name, web_url: web_url) }
  let(:endpoint) { GitlabQuality::TestTooling::Runtime::Env.gitlab_api_base }

  describe '#create' do
    before do
      allow(Gitlab).to receive(:client).and_return(client)
      allow(client).to receive(:create_branch).and_return(branch)
    end

    it 'creates a branch' do
      expect(branches_client.create(branch_name, ref)).to eq(branch)
      expect(Gitlab).to have_received(:client).with(endpoint: endpoint, private_token: token)
      expect(client).to have_received(:create_branch).with(project_id, branch_name, ref)
    end
  end
end
