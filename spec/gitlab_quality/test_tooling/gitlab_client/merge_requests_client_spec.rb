# frozen_string_literal: true

RSpec.describe GitlabQuality::TestTooling::GitlabClient::MergeRequestsClient do
  let(:project) { 'project' }
  let(:merge_request_iid) { 10 }

  context 'with valid input' do
    let(:gitlab_client) { described_class.new(token: 'token', project: project) }

    context 'when the GitLab client is configured' do
      it 'passes the token to the GitLab client' do
        expect(Gitlab).to receive(:client).with(endpoint: anything, private_token: 'token')

        gitlab_client.__send__(:client)
      end

      it 'uses the default base API URL' do
        expect(Gitlab).to receive(:client).with(endpoint: 'https://gitlab.com/api/v4', private_token: anything)

        gitlab_client.__send__(:client)
      end
    end

    context 'when the base API URL is specified as an environment variable' do
      around do |example|
        ClimateControl.modify(GITLAB_API_BASE: 'http://another.gitlab.url') { example.run }
      end

      it 'uses the specified URL' do
        expect(Gitlab).to receive(:client).with(endpoint: 'http://another.gitlab.url', private_token: 'token')

        gitlab_client.__send__(:client)
      end
    end

    describe '#create_merge_request' do
      let(:title) { 'mr title' }
      let(:source_branch) { 'source-branch' }
      let(:target_branch) { 'target-branch' }
      let(:description) { 'mr description' }
      let(:labels) { 'QA,another_label' }
      let(:reviewer_ids) { %w[123 456] }

      it 'calls the #create_merge_request GitLab method' do
        expect(gitlab_client.__send__(:client)).to receive(:create_merge_request).with(project, title,
          { source_branch: source_branch,
            target_branch: target_branch,
            description: description,
            labels: labels,
            squash: true,
            reviewer_ids: reviewer_ids,
            remove_source_branch: true })

        gitlab_client.create_merge_request(title: title,
          source_branch: source_branch,
          target_branch: target_branch,
          description: description,
          reviewer_ids: reviewer_ids,
          labels: labels)
      end
    end

    describe '#find' do
      let(:gitlab_response) { double }

      before do
        allow(gitlab_response).to receive(:auto_paginate).and_return([])
      end

      context 'when an iid is provided' do
        it 'calls #merge_requests GitLab method with iid' do
          expect(gitlab_client.__send__(:client)).to receive(:merge_requests).with(project, merge_request_iid)
                                                                             .and_return(gitlab_response)

          gitlab_client.find(iid: merge_request_iid)
        end
      end

      context 'when an iid is not provided' do
        it 'calls #merge_requests GitLab method without iid' do
          expect(gitlab_client.__send__(:client)).to receive(:merge_requests).with(project, { state: 'opened' })
                                                                             .and_return(gitlab_response)

          gitlab_client.find(options: { state: 'opened' })
        end
      end
    end

    describe '#find_merge_request_changes' do
      it 'calls the #merge_request_changes GitLab method' do
        expect(gitlab_client.__send__(:client)).to receive(:merge_request_changes).with(project, merge_request_iid)

        gitlab_client.find_merge_request_changes(merge_request_iid: merge_request_iid)
      end
    end

    describe '#find_note' do
      let(:gitlab_response) { double }

      before do
        allow(gitlab_response).to receive(:auto_paginate).and_return([])
      end

      it 'calls #merge_request_notes GitLab method' do
        expect(gitlab_client.__send__(:client)).to receive(:merge_request_notes).with(project, merge_request_iid, per_page: 100)
                                                                                .and_return(gitlab_response)

        gitlab_client.find_note(body: 'note', merge_request_iid: merge_request_iid)
      end
    end

    describe '#create_note' do
      let(:note) { 'Slow test note' }

      it 'calls #create_merge_request_note GitLab method' do
        expect(gitlab_client.__send__(:client)).to receive(:create_merge_request_note).with(project, merge_request_iid, note)

        gitlab_client.create_note(note: note, merge_request_iid: merge_request_iid)
      end
    end

    describe '#update_note' do
      let(:note) { 'Slow test note' }

      it 'calls #create_merge_request_note GitLab method' do
        expect(gitlab_client.__send__(:client)).to receive(:edit_merge_request_note).with(project, merge_request_iid, 10, note)

        gitlab_client.update_note(id: 10, note: note, merge_request_iid: merge_request_iid)
      end
    end
  end
end
