# frozen_string_literal: true

RSpec.describe GitlabQuality::TestTooling::GitlabClient::JobClient do
  subject(:job_client) { described_class.new(token: token, project: project_id, job_id: job_id) }

  let(:client) { instance_double(Gitlab::Client) }
  let(:token) { "token" }
  let(:project_id) { 1 }
  let(:job_id) { 2 }
  let(:endpoint) { GitlabQuality::TestTooling::Runtime::Env.gitlab_api_base }
  let(:job_trace) { 'the requested url returned error: 503' }

  describe '#job_trace' do
    before do
      allow(Gitlab).to receive(:client).and_return(client)
    end

    context 'with a successful Gitlab API request' do
      before do
        allow(client).to receive(:job_trace).and_return(job_trace)
      end

      it 'returns job trace' do
        expect(job_client.job_trace).to eq(job_trace)
        expect(Gitlab).to have_received(:client).with(endpoint: endpoint, private_token: token)
        expect(client).to have_received(:job_trace).with(project_id, job_id)
      end
    end

    context 'with failed Gitlab API request' do
      before do
        allow(client).to receive(:job_trace).and_raise(Gitlab::Error::NotFound)
      end

      it 'returns empty string' do
        expect(job_client.job_trace).to eq('')
      end
    end
  end
end
