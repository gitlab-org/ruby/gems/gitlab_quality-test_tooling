# frozen_string_literal: true

RSpec.describe GitlabQuality::TestTooling::Concerns::FindSetDri do
  let(:reporter) { Class.new { include GitlabQuality::TestTooling::Concerns::FindSetDri }.new }

  before do
    stub_request(:get, "https://gitlab-org.gitlab.io/gitlab-roulette/roulette.json")
      .to_return(status: 200, body:
        [
          { username: 'not-a-set',
            role: '<a href="https://handbook.gitlab.com/job-families/engineering/manager/">Manager, Create:IDE' },
          { username: 'set1',
            role: '<a href="https://handbook.gitlab.com/job-families/engineering/software-engineer-in-test/">Software Engineer in Test, Create:Code Review' },
          { username: 'set2',
            role: '<a href="https://handbook.gitlab.com/job-families/engineering/software-engineer-in-test/">Software Engineer in Test, Create:IDE' },
          { username: 'set3',
            role: '<a href="https://handbook.gitlab.com/job-families/engineering/software-engineer-in-test/">Software Engineer in Test, Manage:Import and Integrate' },
          { username: 'set4',
            role: '<a href="https://handbook.gitlab.com/job-families/engineering/software-engineer-in-test/">Software Engineer in Test, Systems: Gitaly' },
          { username: 'set5',
            role: '<a href="https://handbook.gitlab.com/job-families/engineering/software-engineer-in-test/">Software Engineer in Test, Fulfillment section' }
        ].to_json)
  end

  describe '#test_dri' do
    let(:section) { nil }

    shared_examples 'SET finder' do
      it 'finds the single SET assigned to the product group' do
        expect(reporter.test_dri(product_group, test.stage, section)).to eq(expected_set)
        expect(reporter.instance_variable_get(:@group_sets).size).to eq(expected_group_sets_size)
      end
    end

    context 'when there are multiple stage SETs' do
      let(:product_group) { 'code_review' }
      let(:expected_group_sets_size) { 1 }
      let(:test) do
        GitlabQuality::TestTooling::TestResult::JsonTestResult.new(
          report: {
            'file_path' => 'qa/specs/features/browser_ui/3_create/repository/add_new_branch_rule_spec.rb'
          }
        )
      end

      let(:expected_set) { 'set1' }

      it_behaves_like 'SET finder'
    end

    context 'when group name is an acronym' do
      let(:product_group) { 'ide' }
      let(:expected_group_sets_size) { 1 }
      let(:test) do
        GitlabQuality::TestTooling::TestResult::JsonTestResult.new(
          report: { 'file_path' => 'qa/specs/features/browser_ui/3_create/ide/ide_spec.rb' }
        )
      end

      let(:expected_set) { 'set2' }

      it_behaves_like 'SET finder'
    end

    context 'when group name has spaces' do
      let(:product_group) { 'import_and_integrate' }
      let(:expected_group_sets_size) { 1 }
      let(:test) do
        GitlabQuality::TestTooling::TestResult::JsonTestResult.new(
          report: { 'file_path' => 'qa/specs/features/browser_ui/1_manage/rate_limits_spec.rb' }
        )
      end

      let(:expected_set) { 'set3' }

      it_behaves_like 'SET finder'
    end

    context 'when group name is a single word' do
      let(:product_group) { 'gitaly' }
      let(:expected_group_sets_size) { 1 }
      let(:test) do
        GitlabQuality::TestTooling::TestResult::JsonTestResult.new(
          report: { 'file_path' => 'qa/specs/features/browser_ui/12_systems/gitaly/gitaly_spec.rb' }
        )
      end

      let(:expected_set) { 'set4' }

      it_behaves_like 'SET finder'
    end

    context 'when there are no matching group or stage SETs' do
      let(:product_group) { 'provision' }
      let(:section) { 'fulfillment' }
      let(:expected_group_sets_size) { 0 }
      let(:test) do
        GitlabQuality::TestTooling::TestResult::JsonTestResult.new(
          report: {
            'file_path' => 'qa/spec/ui/purchase/add_seats_spec.rb',
            'category' => 'saas'
          }
        )
      end

      let(:expected_set) { 'set5' }

      it_behaves_like 'SET finder'
    end
  end
end
