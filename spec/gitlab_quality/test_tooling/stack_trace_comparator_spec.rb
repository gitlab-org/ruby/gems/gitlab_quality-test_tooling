# frozen_string_literal: true

RSpec.describe GitlabQuality::TestTooling::StackTraceComparator do
  let(:first_trace) do
    <<~TRACE
    foo bar
    TRACE
  end

  let(:second_trace) do
    <<~TRACE
    foo baz
    TRACE
  end

  let(:expected_diff_ratio) { 0.125 }

  subject(:comparator) { described_class.new(first_trace, second_trace) }

  describe '#diff_ratio' do
    it { expect(comparator.diff_ratio).to eq(expected_diff_ratio) }
  end

  describe '#diff_percent' do
    it { expect(comparator.diff_percent).to eq(12.5) }
  end

  describe '#lower_than_diff_ratio?' do
    context 'when given ratio is higher than diff_ratio' do
      it 'returns true' do
        expect(comparator.lower_than_diff_ratio?(0.5)).to be(true)
      end
    end

    context 'when given ratio is equal to diff_ratio' do
      it 'returns false' do
        expect(comparator.lower_than_diff_ratio?(expected_diff_ratio)).to be(false)
      end
    end

    context 'when given ratio is lower than diff_ratio' do
      it 'returns false' do
        expect(comparator.lower_than_diff_ratio?(0.05)).to be(false)
      end
    end
  end

  describe '#lower_or_equal_to_diff_ratio?' do
    context 'when given ratio is higher than diff_ratio' do
      it 'returns true' do
        expect(comparator.lower_or_equal_to_diff_ratio?(0.5)).to be(true)
      end
    end

    context 'when given ratio is equal to diff_ratio' do
      it 'returns true' do
        expect(comparator.lower_or_equal_to_diff_ratio?(expected_diff_ratio)).to be(true)
      end
    end

    context 'when given ratio is lower than diff_ratio' do
      it 'returns false' do
        expect(comparator.lower_or_equal_to_diff_ratio?(0.05)).to be(false)
      end
    end
  end
end
