# frozen_string_literal: true

require_relative 'shared_examples/test_results_shared_examples'
RSpec.describe GitlabQuality::TestTooling::TestResults::JsonTestResults do
  it_behaves_like 'test results' do
    let(:test_data) do
      <<~JSON
        {
          "examples": [
            {
              "full_description": "test-name1",
              "file_path": "test-file1",
              "status": "failed",
              "exceptions": [
                {
                  "class": "Error",
                  "message": "An Error Here",
                  "message_lines": [
                    "Failure/Error: raise \\"An Error Here\\"",
                    "",
                    "Error:",
                    "fail"
                  ],
                  "backtrace": [
                    "Test Stacktrace"
                  ]
                }
              ]
            },
            {
              "full_description": "test-name2",
              "file_path": "test-file2",
              "status": "failed",
              "exceptions": [
                {
                  "class": "Error",
                  "message": "An Error Here",
                  "message_lines": [
                    "Failure/Error: raise \\"An Error Here\\"",
                    "",
                    "Error:",
                    "fail"
                  ],
                  "backtrace": [
                    "test ends on line test-file2:10",
                    "test starts on line test-file2:1",
                    "rspec and other unimportant code"
                  ]
                },
                {
                  "class": "Error2",
                  "message": "Another Error",
                  "message_lines": [
                    "Failure/Error: raise \\"Another Error\\"",
                    "",
                    "Error2:",
                    "fail"
                  ],
                  "backtrace": [
                    "Test Stacktrace Again",
                    "but without the file path"
                  ]
                }
              ]
            },
            {
              "full_description": "test-name3",
              "file_path": "test-file3",
              "status": "pending"
            }
          ]
        }
      JSON
    end
  end

  context 'with empty test results artifact' do
    let(:test_data) { '' }

    subject { described_class.new(path: 'file') }

    before do
      allow(File).to receive(:read).with('file').and_return(test_data)
    end

    it 'returns an empty collection' do
      expect(subject.to_a).to be_empty
      expect(subject.map(&:testcase)).to be_empty
      expect(subject.map(&:name)).to be_empty
      expect(subject.map(&:file)).to be_empty
    end
  end
end
