# frozen_string_literal: true

require 'gitlab'

RSpec.describe GitlabQuality::TestTooling::JobTraceAnalyzer do
  let(:project) { 1 }
  let(:token)   { 'Gitlab API Token' }
  let(:job_id)  { 2 }

  subject(:job_trace_analyzer) { described_class.new(project: project, token: token, job_id: job_id) }

  describe '#found_infrastructure_error?' do
    before do
      allow(Gitlab).to receive_message_chain(:client, :job_trace).and_return(job_trace)
    end

    context 'when job trace is empty' do
      let(:job_trace) { '' }

      it 'returns false' do
        expect(job_trace_analyzer.found_infrastructure_error?).to be(false)
      end
    end

    context 'with no infrastructure error in job trace' do
      let(:job_trace) { 'foo bar' }

      it 'returns false' do
        expect(job_trace_analyzer.found_infrastructure_error?).to be(false)
      end
    end

    context 'with failure summary' do
      context 'with infrastructure error trace located inside the failure summary' do
        let(:job_trace) do
          File.read(File.expand_path("fixtures/infra_trace_in_failure_summary.txt", __dir__))
        end

        it 'returns true' do
          expect(job_trace_analyzer.found_infrastructure_error?).to be(true)
        end
      end

      context 'with infrastructure error trace located outside the failure summary' do
        let(:job_trace) do
          File.read(File.expand_path("fixtures/infra_trace_outside_failure_summary.txt", __dir__))
        end

        it 'returns false' do
          expect(job_trace_analyzer.found_infrastructure_error?).to be(false)
        end
      end
    end

    context 'without failure summary' do
      context 'with infrastructure error located in any part of the job trace' do
        let(:job_trace) { "500 Internal Server Error" }

        it 'returns true' do
          expect(job_trace_analyzer.found_infrastructure_error?).to be(true)
        end
      end
    end
  end
end
