# frozen_string_literal: true

RSpec.describe GitlabQuality::TestTooling::SummaryTable do
  shared_examples 'returns a summary table of results' do |expected_output|
    let(:expected_output) { expected_output }

    it 'returns a summary table of contents' do
      input_report1 = '<testsuite name="rspec" tests="2" failures="0" errors="0" skipped="0"/>'
      input_report2 = '<testsuite name="rspec" tests="4" failures="1" errors="1" skipped="1"/>'

      allow(Dir).to receive(:glob).and_return(%w[plan create])

      expect(File).to receive(:open).with('plan').and_return(input_report1).ordered
      expect(File).to receive(:open).with('create').and_return(input_report2).ordered

      expect(described_class.create(**create_params).gsub(/\s+/, "")).to eq expected_output.gsub(/\s+/, "")
    end
  end

  describe '#create' do
    it 'requires input files' do
      expect { described_class.create }.to raise_error(ArgumentError, "missing keyword: :input_files")
    end

    it 'accepts input files' do
      files = 'files'

      expect(described_class).to receive(:collect_results).with(files).and_return([])

      expect { described_class.create(input_files: files) }.not_to raise_error
    end

    describe 'input files' do
      context 'with input files and no options' do
        let(:create_params) do
          { input_files: 'files' }
        end

        it_behaves_like 'returns a summary table of results', <<~EXPECTED_OUTPUT
          ```\nDEV STAGE | TOTAL | FAILURES | ERRORS | SKIPPED | RESULT
               ----------|-------|----------|--------|---------|-------
               Plan      | 2     | 0        | 0      | 0       | ✅
               Create    | 4     | 1        | 1      | 1       | ❌
          ```\n
        EXPECTED_OUTPUT
      end

      context 'when sort_by option is provided' do
        let(:create_params) do
          { input_files: 'files', sort_by: 'Dev Stage' }
        end

        it_behaves_like 'returns a summary table of results', <<~EXPECTED_OUTPUT
          ```\nDEV STAGE | TOTAL | FAILURES | ERRORS | SKIPPED | RESULT
               ----------|-------|----------|--------|---------|-------
               Create    | 4     | 1        | 1      | 1       | ❌
               Plan      | 2     | 0        | 0      | 0       | ✅
          ```\n
        EXPECTED_OUTPUT
      end

      context 'when a sort_by and reverse_sort is provided' do
        let(:create_params) do
          { input_files: 'files', sort_by: 'Failures', sort_direction: :desc }
        end

        it_behaves_like 'returns a summary table of results', <<~EXPECTED_OUTPUT
          ```\nDEV STAGE | TOTAL | FAILURES | ERRORS | SKIPPED | RESULT
               ----------|-------|----------|--------|---------|-------
               Create    | 4     | 1        | 1      | 1       | ❌
               Plan      | 2     | 0        | 0      | 0       | ✅
          ```\n
        EXPECTED_OUTPUT
      end

      context 'when reverse_sort and sort_by is not provided' do
        let(:create_params) do
          { input_files: 'files', sort_direction: :desc }
        end

        it_behaves_like 'returns a summary table of results', <<~EXPECTED_OUTPUT
          ```\nDEV STAGE | TOTAL | FAILURES | ERRORS | SKIPPED | RESULT
               ----------|-------|----------|--------|---------|-------
               Create    | 4     | 1        | 1      | 1       | ❌
               Plan      | 2     | 0        | 0      | 0       | ✅
          ```\n
        EXPECTED_OUTPUT
      end

      context 'when hide_passed_tests is provided' do
        let(:create_params) do
          { input_files: 'files', hide_passed_tests: true }
        end

        it_behaves_like 'returns a summary table of results', <<~EXPECTED_OUTPUT
          ```\nDEV STAGE | TOTAL | FAILURES | ERRORS | SKIPPED | RESULT
                ----------|-------|----------|--------|---------|-------
                Create    | 4     | 1        | 1      | 1       | ❌
          ```\n
        EXPECTED_OUTPUT
      end
    end
  end
end
