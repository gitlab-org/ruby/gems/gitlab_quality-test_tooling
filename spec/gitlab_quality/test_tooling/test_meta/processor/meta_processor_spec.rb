# frozen_string_literal: true

RSpec.describe GitlabQuality::TestTooling::TestMeta::Processor::MetaProcessor do
  let(:processor) { described_class }

  describe '#end_of_description_index' do
    where(:description, :index) do
      [
        [%(it "exposes variable on protected branch"), 41],
        ["context 'exposes variable on protected branch', :reliable,", 46],
        ["description 'exposes variable on protected branch', :reliable, testcase: 'http://link/to/testcase'", 50]
      ]
    end

    with_them do
      it 'returns the correct index' do
        expect(processor.send(:end_of_description_index, description)).to eq(index)
      end
    end
  end
end
