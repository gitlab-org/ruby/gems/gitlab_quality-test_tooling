# frozen_string_literal: true

RSpec.describe GitlabQuality::TestTooling::TestMeta::Processor::AddToBlockingProcessor do
  let(:processor) { described_class }
  let(:test_meta_updater) do
    GitlabQuality::TestTooling::TestMeta::TestMetaUpdater.new(
      token: test_token,
      project: test_project,
      specs_file: specs_file_path,
      processor: processor,
      ref: ref,
      dry_run: false
    )
  end

  let(:example_name) { 'example_name' }
  let(:ref) { 'master' }
  let(:specs_file_path) { 'path/to/file_1.rb' }
  let(:test_token) { 'test_case_project_token' }
  let(:test_project) { 'valid-project' }
  let(:file) { 'log_in_spec.rb' }
  let(:file_path) { "/qa/qa/specs/features/browser_ui/10_govern/login/#{file}" }
  let(:test_file_contents) do
    <<~RUBY
      # frozen_string_literal: true
      module QA
        RSpec.describe 'Govern', :smoke, :mobile, product_group: :authentication do
          describe 'basic user login' do
            it 'user logs in using basic credentials and logs out',
              testcase: 'https://gitlab.com/gitlab-org/gitlab/-/quality/test_cases/347880' do
              Flow::Login.sign_in
            end
          end
        end
      end
    RUBY
  end

  let(:branch) do
    {
      name: 'branch-name',
      web_url: 'http://web/url'
    }
  end

  let(:mr_title) { format("%{prefix} %{file}", prefix: '[E2E] PROMOTE TO BLOCKING:', file: file).truncate(72, omission: '') }
  let(:reviewer_id) { 123 }
  let(:reviewer_handle) { 'reviewer_handle' }
  let(:assignee_id) { 234 }

  let(:labels_inference_double) { instance_double(GitlabQuality::TestTooling::LabelsInference) }
  let(:product_group) { 'source_code' }
  let(:group_label) { 'group::source code' }
  let(:branch_prefix) { /blocking-promotion-\S+/ }

  let(:spec) do
    <<~JSON
      {
        "name": "#{example_name}",
        "link": "https://gitlab.com/gitlab-org/gitlab/-/blob/master#{file_path}",
        "testcase": "https://gitlab.com/gitlab-org/gitlab/-/quality/test_cases/347880",
        "file_path": "#{file_path}",
        "file": "#{file}"
      }
    JSON
  end

  let(:changed_line_no) { "4" }
  let(:expected_processed_commits) do
    { file_path => { branch: branch, commits: { changed_line_no => JSON.parse(spec) } } }
  end

  describe '#create_commit' do
    shared_examples 'add to blocking processor' do
      it 'adds the blocking meta' do
        expect(test_meta_updater).to receive(:commit_changes).with(branch, anything, file_path, expected_test_file_contents)
        processor.create_commit(JSON.parse(spec), test_meta_updater)
        expect(test_meta_updater.processed_commits).to eq(expected_processed_commits)
      end
    end

    context 'when there are file changes' do
      before do
        allow(test_meta_updater).to receive(:get_file_contents).with(file_path: file_path, branch: nil).and_return(orignal_test_file_contents)
        allow(test_meta_updater).to receive(:create_branch).with(branch_prefix, file, ref).and_return(branch)
      end

      context 'with single spec' do
        context 'with comma on description line' do
          let(:example_name) { 'Govern basic user login user logs in using basic credentials and logs out' }
          let(:orignal_test_file_contents) do
            <<~RUBY
              # frozen_string_literal: true
              module QA
                RSpec.describe 'Govern', :smoke, :mobile, product_group: :authentication do
                  describe 'basic user login' do
                    it 'user logs in using basic credentials and logs out',
                      testcase: 'https://gitlab.com/gitlab-org/gitlab/-/quality/test_cases/347880' do
                      Flow::Login.sign_in
                    end
                  end
                end
              end
            RUBY
          end

          let(:expected_test_file_contents) do
            <<~RUBY
              # frozen_string_literal: true
              module QA
                RSpec.describe 'Govern', :smoke, :mobile, product_group: :authentication do
                  describe 'basic user login' do
                    it 'user logs in using basic credentials and logs out', :blocking,
                      testcase: 'https://gitlab.com/gitlab-org/gitlab/-/quality/test_cases/347880' do
                      Flow::Login.sign_in
                    end
                  end
                end
              end
            RUBY
          end

          it_behaves_like 'add to blocking processor'
        end

        context 'without comma on description line' do
          let(:example_name) { 'Govern basic user login user logs in using basic credentials and logs out' }
          let(:orignal_test_file_contents) do
            <<~RUBY
              # frozen_string_literal: true
              module QA
                RSpec.describe 'Govern', :smoke, :mobile, product_group: :authentication do
                  describe 'basic user login' do
                    it 'user logs in using basic credentials and logs out' do
                      Flow::Login.sign_in
                    end
                  end
                end
              end
            RUBY
          end

          let(:expected_test_file_contents) do
            <<~RUBY
              # frozen_string_literal: true
              module QA
                RSpec.describe 'Govern', :smoke, :mobile, product_group: :authentication do
                  describe 'basic user login' do
                    it 'user logs in using basic credentials and logs out', :blocking do
                      Flow::Login.sign_in
                    end
                  end
                end
              end
            RUBY
          end

          it_behaves_like 'add to blocking processor'
        end

        context 'with comma within the description but not outside it' do
          let(:example_name) { 'Package Conan Repository publishes, installs, and deletes a Conan package' }
          let(:orignal_test_file_contents) do
            <<~RUBY
              # frozen_string_literal: true
              module QA
                RSpec.describe 'Package', :smoke, :mobile, product_group: :authentication do
                  describe 'Conan Repository' do
                    it 'publishes, installs, and deletes a Conan package' do
                      Flow::Login.sign_in
                    end
                  end
                end
              end
            RUBY
          end

          let(:expected_test_file_contents) do
            <<~RUBY
              # frozen_string_literal: true
              module QA
                RSpec.describe 'Package', :smoke, :mobile, product_group: :authentication do
                  describe 'Conan Repository' do
                    it 'publishes, installs, and deletes a Conan package', :blocking do
                      Flow::Login.sign_in
                    end
                  end
                end
              end
            RUBY
          end

          it_behaves_like 'add to blocking processor'
        end

        context 'with comma within and also outside the description' do
          let(:example_name) { 'Package Conan Repository publishes, installs, and deletes a Conan package' }
          let(:orignal_test_file_contents) do
            <<~RUBY
              # frozen_string_literal: true
              module QA
                RSpec.describe 'Package', :smoke, :mobile, product_group: :authentication do
                  describe 'Conan Repository' do
                    it "publishes, installs, and deletes a Conan package",
                      testcase: 'https://gitlab.com/gitlab-org/gitlab/-/quality/test_cases/347880' do
                      Flow::Login.sign_in
                    end
                  end
                end
              end
            RUBY
          end

          let(:expected_test_file_contents) do
            <<~RUBY
              # frozen_string_literal: true
              module QA
                RSpec.describe 'Package', :smoke, :mobile, product_group: :authentication do
                  describe 'Conan Repository' do
                    it "publishes, installs, and deletes a Conan package", :blocking,
                      testcase: 'https://gitlab.com/gitlab-org/gitlab/-/quality/test_cases/347880' do
                      Flow::Login.sign_in
                    end
                  end
                end
              end
            RUBY
          end

          it_behaves_like 'add to blocking processor'
        end

        context 'with comma within the description and testcase on same line' do
          let(:example_name) { 'Package Conan Repository publishes, installs, and deletes a Conan package' }
          let(:orignal_test_file_contents) do
            <<~RUBY
              # frozen_string_literal: true
              module QA
                RSpec.describe 'Package', :smoke, :mobile, product_group: :authentication do
                  describe 'Conan Repository' do
                    it 'publishes, installs, and deletes a Conan package', testcase: 'https://link/to/testcase' do
                      Flow::Login.sign_in
                    end
                  end
                end
              end
            RUBY
          end

          let(:expected_test_file_contents) do
            <<~RUBY
              # frozen_string_literal: true
              module QA
                RSpec.describe 'Package', :smoke, :mobile, product_group: :authentication do
                  describe 'Conan Repository' do
                    it 'publishes, installs, and deletes a Conan package', :blocking, testcase: 'https://link/to/testcase' do
                      Flow::Login.sign_in
                    end
                  end
                end
              end
            RUBY
          end

          it_behaves_like 'add to blocking processor'
        end
      end

      context 'with multiple specs' do
        let(:orignal_test_file_contents) do
          <<~RUBY
            # frozen_string_literal: true

            module QA
              RSpec.describe 'Create' do
                describe 'Commit data', product_group: :source_code do

                  it 'user views raw email patch', testcase: 'https://gitlab.com/gitlab-org/gitlab/-/quality/test_cases/347755' do
                    view_commit
                  end

                  it 'user views raw commit diff', testcase: 'https://gitlab.com/gitlab-org/gitlab/-/quality/test_cases/347754' do
                    view_commit
                  end
                end
              end
            end
          RUBY
        end

        context 'when first spec has to be promoted' do
          let(:example_name) { 'Create Commit data user views raw email patch' }
          let(:expected_test_file_contents) do
            <<~RUBY
              # frozen_string_literal: true

              module QA
                RSpec.describe 'Create' do
                  describe 'Commit data', product_group: :source_code do

                    it 'user views raw email patch',
                      :blocking, testcase: 'https://gitlab.com/gitlab-org/gitlab/-/quality/test_cases/347755' do
                      view_commit
                    end

                    it 'user views raw commit diff', testcase: 'https://gitlab.com/gitlab-org/gitlab/-/quality/test_cases/347754' do
                      view_commit
                    end
                  end
                end
              end
            RUBY
          end

          let(:changed_line_no) { "6" }

          it_behaves_like 'add to blocking processor'
        end

        context 'when second spec has to be promoted' do
          let(:example_name) { 'Create Commit data user views raw commit diff' }
          let(:expected_test_file_contents) do
            <<~RUBY
              # frozen_string_literal: true

              module QA
                RSpec.describe 'Create' do
                  describe 'Commit data', product_group: :source_code do

                    it 'user views raw email patch', testcase: 'https://gitlab.com/gitlab-org/gitlab/-/quality/test_cases/347755' do
                      view_commit
                    end

                    it 'user views raw commit diff',
                      :blocking, testcase: 'https://gitlab.com/gitlab-org/gitlab/-/quality/test_cases/347754' do
                      view_commit
                    end
                  end
                end
              end
            RUBY
          end

          let(:changed_line_no) { "10" }

          it_behaves_like 'add to blocking processor'
        end

        context 'with similar it block within a different context' do
          let(:orignal_test_file_contents) do
            <<~RUBY
              # frozen_string_literal: true

              module QA
                RSpec.describe 'Create' do
                  describe 'Project templates', product_group: :source_code do
                    context 'when built-in', :requires_admin do
                      it 'successfully imports the project using template',
                        testcase: 'https://gitlab.com/gitlab-org/gitlab/-/quality/test_cases/347932' do
                        # ...
                      end
                    end

                    context 'when instance level', :requires_admin do
                      it 'successfully imports the project using template',
                        testcase: 'https://gitlab.com/gitlab-org/gitlab/-/quality/test_cases/347875' do
                        # ...
                      end
                    end

                    context 'when group level' do
                      it 'successfully imports the project using template',
                        testcase: 'https://gitlab.com/gitlab-org/gitlab/-/quality/test_cases/347933' do
                        # ...
                      end
                    end
                  end
                end
              end
            RUBY
          end

          let(:expected_test_file_contents) do
            <<~RUBY
              # frozen_string_literal: true

              module QA
                RSpec.describe 'Create' do
                  describe 'Project templates', product_group: :source_code do
                    context 'when built-in', :requires_admin do
                      it 'successfully imports the project using template',
                        testcase: 'https://gitlab.com/gitlab-org/gitlab/-/quality/test_cases/347932' do
                        # ...
                      end
                    end

                    context 'when instance level', :requires_admin do
                      it 'successfully imports the project using template', :blocking,
                        testcase: 'https://gitlab.com/gitlab-org/gitlab/-/quality/test_cases/347875' do
                        # ...
                      end
                    end

                    context 'when group level' do
                      it 'successfully imports the project using template',
                        testcase: 'https://gitlab.com/gitlab-org/gitlab/-/quality/test_cases/347933' do
                        # ...
                      end
                    end
                  end
                end
              end
            RUBY
          end

          let(:example_name) { 'Create Project templates when instance level successfully imports the project using template' }
          let(:changed_line_no) { "13" }

          it_behaves_like 'add to blocking processor'
        end
      end

      context 'with description on separate line' do
        let(:orignal_test_file_contents) do
          <<~RUBY
            # frozen_string_literal: true

            module QA
              RSpec.describe 'Monitor', :smoke, product_group: :respond do
                describe 'Alert settings' do
                  shared_examples 'sends test alert' do
                    it 'creates new alert' do
                      # ...
                    end
                  end

                  context(
                    'when using HTTP endpoint integration',
                    testcase: 'https://gitlab.com/gitlab-org/gitlab/-/quality/test_cases/382803'
                  ) do

                    it_behaves_like 'sends test alert'
                  end

                  context(
                    'when using Prometheus integration',
                    testcase: 'https://gitlab.com/gitlab-org/gitlab/-/quality/test_cases/385792'
                  ) do

                    it_behaves_like 'sends test alert'
                  end
                end
              end
            end
          RUBY
        end

        let(:expected_test_file_contents) do
          <<~RUBY
            # frozen_string_literal: true

            module QA
              RSpec.describe 'Monitor', :smoke, product_group: :respond do
                describe 'Alert settings' do
                  shared_examples 'sends test alert' do
                    it 'creates new alert' do
                      # ...
                    end
                  end

                  context(
                    'when using HTTP endpoint integration', :blocking,
                    testcase: 'https://gitlab.com/gitlab-org/gitlab/-/quality/test_cases/382803'
                  ) do

                    it_behaves_like 'sends test alert'
                  end

                  context(
                    'when using Prometheus integration',
                    testcase: 'https://gitlab.com/gitlab-org/gitlab/-/quality/test_cases/385792'
                  ) do

                    it_behaves_like 'sends test alert'
                  end
                end
              end
            end
          RUBY
        end

        let(:example_name) { 'Monitor Alert settings when using HTTP endpoint integration behaves like sends test alert creates new alert' }
        let(:changed_line_no) { "12" }

        it_behaves_like 'add to blocking processor'
      end

      context 'with shared_examples' do
        let(:orignal_test_file_contents) do
          <<~RUBY
            # frozen_string_literal: true

            module QA
              RSpec.describe 'Create' do
                describe 'License Detection', product_group: :source_code do
                  shared_examples 'project license detection' do
                    it 'displays the name' do
                      # ...
                    end
                  end

                  context 'on project A',
                    testcase: 'https://gitlab.com/gitlab-org/gitlab/-/quality/test_cases/366842' do
                    it_behaves_like 'project license detection'
                  end

                  context 'on project B',
                    testcase: 'https://gitlab.com/gitlab-org/gitlab/-/quality/test_cases/366843' do
                    it_behaves_like 'project license detection'
                  end
                end
              end
            end
          RUBY
        end

        let(:expected_test_file_contents) do
          <<~RUBY
            # frozen_string_literal: true

            module QA
              RSpec.describe 'Create' do
                describe 'License Detection', product_group: :source_code do
                  shared_examples 'project license detection' do
                    it 'displays the name' do
                      # ...
                    end
                  end

                  context 'on project A', :blocking,
                    testcase: 'https://gitlab.com/gitlab-org/gitlab/-/quality/test_cases/366842' do
                    it_behaves_like 'project license detection'
                  end

                  context 'on project B',
                    testcase: 'https://gitlab.com/gitlab-org/gitlab/-/quality/test_cases/366843' do
                    it_behaves_like 'project license detection'
                  end
                end
              end
            end
          RUBY
        end

        let(:example_name) { 'Create License Detection on project A behaves like project license detection displays the name' }
        let(:changed_line_no) { "11" }

        it_behaves_like 'add to blocking processor'
      end

      context 'with include_examples' do
        let(:orignal_test_file_contents) do
          <<~RUBY
            # frozen_string_literal: true

            module QA
              RSpec.shared_examples 'notifies on a pipeline' do |exit_code|
                it 'sends an email' do
                  # ...
                end
              end

              RSpec.describe 'Manage', :orchestrated, :runner, :requires_admin, :smtp, product_group: :import_and_integrate do
                describe 'Pipeline status emails' do
                  describe 'when pipeline passes', testcase: 'https://gitlab.com/gitlab-org/gitlab/-/quality/test_cases/366240' do
                    include_examples 'notifies on a pipeline', 0
                  end

                  describe 'when pipeline fails', testcase: 'https://gitlab.com/gitlab-org/gitlab/-/quality/test_cases/366241' do
                    include_examples 'notifies on a pipeline', 1
                  end
                end
              end
            end
          RUBY
        end

        let(:expected_test_file_contents) do
          <<~RUBY
            # frozen_string_literal: true

            module QA
              RSpec.shared_examples 'notifies on a pipeline' do |exit_code|
                it 'sends an email' do
                  # ...
                end
              end

              RSpec.describe 'Manage', :orchestrated, :runner, :requires_admin, :smtp, product_group: :import_and_integrate do
                describe 'Pipeline status emails' do
                  describe 'when pipeline passes',
                    :blocking, testcase: 'https://gitlab.com/gitlab-org/gitlab/-/quality/test_cases/366240' do
                    include_examples 'notifies on a pipeline', 0
                  end

                  describe 'when pipeline fails', testcase: 'https://gitlab.com/gitlab-org/gitlab/-/quality/test_cases/366241' do
                    include_examples 'notifies on a pipeline', 1
                  end
                end
              end
            end
          RUBY
        end

        let(:example_name) { 'Manage Pipeline status emails when pipeline passes sends an email' }
        let(:changed_line_no) { "11" }

        it_behaves_like 'add to blocking processor'
      end

      context 'with with_them' do
        let(:orignal_test_file_contents) do
          <<~RUBY
            # frozen_string_literal: true

            module QA
              RSpec.describe 'Verify', :runner, product_group: :pipeline_authoring do
                describe 'Pipeline with image:pull_policy' do
                  context 'when policy is allowed' do
                    where do
                      {
                        'with [if-not-present] policy' => { testcase: 'https://gitlab.com/gitlab-org/gitlab/-/quality/test_cases/368858'},
                        'with [always] policy' => {testcase: 'https://gitlab.com/gitlab-org/gitlab/-/quality/test_cases/367154'},
                      }
                    end

                    with_them do
                      it 'applies pull policy in job correctly', :some_tag, testcase: params[:testcase] do
                        visit_job
                      end
                    end
                  end
                end
              end
            end
          RUBY
        end

        let(:expected_test_file_contents) do
          <<~RUBY
            # frozen_string_literal: true

            module QA
              RSpec.describe 'Verify', :runner, product_group: :pipeline_authoring do
                describe 'Pipeline with image:pull_policy' do
                  context 'when policy is allowed' do
                    where do
                      {
                        'with [if-not-present] policy' => { testcase: 'https://gitlab.com/gitlab-org/gitlab/-/quality/test_cases/368858'},
                        'with [always] policy' => {testcase: 'https://gitlab.com/gitlab-org/gitlab/-/quality/test_cases/367154'},
                      }
                    end

                    with_them do
                      it 'applies pull policy in job correctly', :blocking, :some_tag, testcase: params[:testcase] do
                        visit_job
                      end
                    end
                  end
                end
              end
            end
          RUBY
        end

        let(:example_name) { 'Verify Pipeline with image:pull_policy when policy is allowed with [always] policy applies pull policy in job correctly' }
        let(:changed_line_no) { "14" }

        it_behaves_like 'add to blocking processor'
      end

      context 'with describe block towards end of file' do
        let(:orignal_test_file_contents) do
          <<~RUBY
            # frozen_string_literal: true

            RSpec.shared_examples 'validate dictionary' do |objects, directory_path, required_fields|
              context 'for each object' do
                it 'has a metadata file' do
                end

                it 'has a valid metadata file' do
                end
              end
            end

            RSpec.describe 'Views documentation', feature_category: :database do
              include_examples 'validate dictionary', views, directory_path, required_fields
            end

            RSpec.describe 'Deleted views documentation', feature_category: :database do
              include_examples 'validate dictionary', views, directory_path, required_fields
            end
          RUBY
        end

        let(:expected_test_file_contents) do
          <<~RUBY
            # frozen_string_literal: true

            RSpec.shared_examples 'validate dictionary' do |objects, directory_path, required_fields|
              context 'for each object' do
                it 'has a metadata file' do
                end

                it 'has a valid metadata file' do
                end
              end
            end

            RSpec.describe 'Views documentation', feature_category: :database do
              include_examples 'validate dictionary', views, directory_path, required_fields
            end

            RSpec.describe 'Deleted views documentation', :blocking, feature_category: :database do
              include_examples 'validate dictionary', views, directory_path, required_fields
            end
          RUBY
        end

        let(:example_name) { 'Deleted views documentation for each object has a valid metadata file' }
        let(:changed_line_no) { "16" }

        it_behaves_like 'add to blocking processor'
      end

      context 'with resulting line length more than 120 characters' do
        let(:orignal_test_file_contents) do
          <<~RUBY
            # frozen_string_literal: true

            module QA
              RSpec.describe 'Plan' do
                describe 'Issue', product_group: :project_management do
                  it 'closes via pushing a commit', testcase: 'https://gitlab.com/gitlab-org/gitlab/-/quality/test_cases/347947' do
                    # ...
                  end
                end
              end
            end
          RUBY
        end

        let(:expected_test_file_contents) do
          <<~RUBY
            # frozen_string_literal: true

            module QA
              RSpec.describe 'Plan' do
                describe 'Issue', product_group: :project_management do
                  it 'closes via pushing a commit',
                    :blocking, testcase: 'https://gitlab.com/gitlab-org/gitlab/-/quality/test_cases/347947' do
                    # ...
                  end
                end
              end
            end
          RUBY
        end

        let(:example_name) { 'Plan Issue closes via pushing a commit' }
        let(:changed_line_no) { '5' }

        it_behaves_like 'add to blocking processor'
      end

      context 'when a spec already has a blocking tag' do
        let(:example_name) { 'Govern basic user login user logs in using basic credentials and logs out' }
        let(:orignal_test_file_contents) do
          <<~RUBY
            # frozen_string_literal: true
            module QA
              RSpec.describe 'Govern', :smoke, :mobile, product_group: :authentication do
                describe 'basic user login' do
                  it 'user logs in using basic credentials and logs out', :blocking,
                    testcase: 'https://gitlab.com/gitlab-org/gitlab/-/quality/test_cases/347880' do
                    Flow::Login.sign_in
                  end
                end
              end
            end
          RUBY
        end

        it 'does not add the blocking meta' do
          expect(test_meta_updater).not_to receive(:commit_changes)
          processor.create_commit(JSON.parse(spec), test_meta_updater)
        end
      end

      context 'when the test is in quarantine' do
        let(:example_name) { 'Govern basic user login user logs in using basic credentials and logs out' }
        let(:orignal_test_file_contents) do
          <<~RUBY
              # frozen_string_literal: true
              module QA
                RSpec.describe 'Govern', :smoke, :mobile, product_group: :authentication do
                  describe 'basic user login' do
                    it 'user logs in using basic credentials and logs out',
                      testcase: 'https://gitlab.com/gitlab-org/gitlab/-/quality/test_cases/347880',
                      quarantine: {
                        issue: 'https://gitlab.com/gitlab-org/gitlab/-/issues/409541',
                        type: :bug
                      } do
                      Flow::Login.sign_in
                    end
                  end
                end
              end
          RUBY
        end

        it 'does not add the blocking meta' do
          expect(test_meta_updater).not_to receive(:commit_changes)
          processor.create_commit(JSON.parse(spec), test_meta_updater)
        end
      end
    end

    context 'when there are no file changes' do
      before do
        allow(processor).to receive(:add_metadata).and_return(['same_old_content', -1])
        allow(test_meta_updater).to receive(:get_file_contents).with(file_path: file_path, branch: nil).and_return(test_file_contents)
      end

      it 'does not add the blocking meta' do
        expect(test_meta_updater).not_to receive(:commit_changes)
        processor.create_commit(JSON.parse(spec), test_meta_updater)
      end
    end

    context 'when the record is already processed' do
      let(:changed_line_number) { "4" }
      let(:branch) { Gitlab::ObjectifiedHash.new(name: 'branch_name') }

      before do
        allow(processor).to receive(:add_metadata).and_return(['content', changed_line_number.to_i])
        allow(test_meta_updater).to receive(:processed_commits).and_return(expected_processed_commits)
        allow(test_meta_updater).to receive(:get_file_contents).with(file_path: file_path, branch: branch['name']).and_return(test_file_contents)
      end

      it 'does not add the blocking meta' do
        expect(test_meta_updater).not_to receive(:commit_changes)
        processor.create_commit(JSON.parse(spec), test_meta_updater)
      end
    end
  end

  describe '#create_merge_requests' do
    let(:merge_request_client_double) { instance_double(GitlabQuality::TestTooling::GitlabClient::MergeRequestsClient) }
    let(:merge_request) { Gitlab::ObjectifiedHash.new(iid: 123, web_url: 'http://web/url') }

    before do
      allow(test_meta_updater).to receive_messages(processed_commits: processed_commits, fetch_dri_id: [reviewer_id, reviewer_handle],
        merge_request_client: merge_request_client_double)
      allow(test_meta_updater).to receive(:post_note_on_merge_request).with(anything, merge_request.iid)
      allow(test_meta_updater).to receive_messages(user_id_for_username: assignee_id, labels_inference: labels_inference_double)
      allow(labels_inference_double).to receive(:infer_labels_from_product_group).with(product_group).and_return(Set.new([group_label]))
    end

    context 'with single file_path in processed commits' do
      context 'with single commit per file path' do
        let(:processed_commits) do
          {
            file_path => { branch: branch, commits: { changed_line_no => JSON.parse(spec) } }
          }
        end

        it 'creates a single MR' do
          expect(test_meta_updater).to receive(:create_merge_request).with(mr_title, branch, assignee_id, [reviewer_id]).and_return(merge_request)
          processor.create_merge_requests(test_meta_updater)
        end
      end

      context 'with multiple commits per file path' do
        changed_line_no_2 = "20"

        let(:processed_commits) do
          {
            file_path => { branch: branch, commits: {
              changed_line_no => JSON.parse(spec),
              changed_line_no_2 => JSON.parse(spec)
            } }
          }
        end

        it 'creates a single MR' do
          expect(test_meta_updater).to receive(:create_merge_request).with(mr_title, branch, assignee_id, [reviewer_id]).and_return(merge_request)
          expect(test_meta_updater).not_to receive(:create_merge_request)
          processor.create_merge_requests(test_meta_updater)
        end
      end
    end

    context 'with multiple file_paths in processed commits' do
      let(:example_name_2) { 'example_name_2' }
      let(:file_2) { 'file_2.rb' }
      let(:file_path_2) { "/qa/qa/specs/features/browser_ui/10_govern/login/#{file_2}" }
      let(:spec_2) do
        <<~JSON
          {
            "name": "#{example_name_2}",
            "link": "https://gitlab.com/gitlab-org/gitlab/-/blob/master#{file_path_2}",
            "testcase": "https://gitlab.com/gitlab-org/gitlab/-/quality/test_cases/347880",
            "file_path": "#{file_path_2}",
            "file": "#{file_2}"
          }
        JSON
      end

      let(:mr_title_2) { format("%{prefix} %{file}", prefix: '[E2E] PROMOTE TO BLOCKING:', file: file_2).truncate(72, omission: '') }

      let(:processed_commits) do
        {
          file_path => { branch: branch, commits: { changed_line_no => JSON.parse(spec) } },
          file_path_2 => { branch: branch, commits: { changed_line_no => JSON.parse(spec_2) } }
        }
      end

      it 'creates as many MRs' do
        expect(test_meta_updater).to receive(:create_merge_request).with(mr_title, branch, assignee_id, [reviewer_id]).and_return(merge_request)
        expect(test_meta_updater).to receive(:create_merge_request).with(mr_title_2, branch, assignee_id, [reviewer_id]).and_return(merge_request)
        processor.create_merge_requests(test_meta_updater)
      end
    end
  end
end
