# frozen_string_literal: true

RSpec.describe GitlabQuality::TestTooling::TestMeta::TestMetaUpdater do
  let(:test_token) { 'test_case_project_token' }
  let(:test_project) { 'valid-project' }
  let(:test_file_path_one) { 'path/to/file_1.rb' }
  let(:test_file_path_two) { 'path/to/file_2.rb' }
  let(:file_path_one) { '/qa/qa/specs/features/browser_ui/10_govern/login/log_in_spec.rb' }
  let(:file_path_two) { '/qa/qa/specs/features/browser_ui/3_create/repository/user_views_commit_diff_patch_spec.rb' }
  let(:example_name_one) { 'Govern basic user login user logs in using basic credentials and logs out' }
  let(:example_name_two) { 'Create Commit data user views raw commit diff' }
  let(:failure_issue_one) { 'https://gitlab.com/gitlab-org/gitlab/-/issues/1234' }
  let(:failure_issue_two) { 'https://gitlab.com/gitlab-org/gitlab/-/issues/1235' }
  let(:ref) { 'master' }
  let(:branch_one) { Gitlab::ObjectifiedHash.new(name: branch_name_one, web_url: 'http://web/url') }

  let(:branch_prefix) { 'branch_prefix' }
  let(:branch_name_one) do
    [branch_prefix, example_name_one.gsub(/\W/, '-')].join('-')
  end

  let(:opened_issues) { [Gitlab::ObjectifiedHash.new(state: 'opened')] }
  let(:add_to_blocking_processor) { class_double(GitlabQuality::TestTooling::TestMeta::Processor::AddToBlockingProcessor) }

  let(:unstable_specs_file_contents) do
    <<~JSON
      {
        "type": "Unstable Specs",
        "report_issue": "https://gitlab.com/gitlab-org/gitlab/-/issues/435321",
        "specs": [
          #{unstable_spec_one},
          #{unstable_spec_two}
        ]
      }
    JSON
  end

  let(:unstable_spec_one) do
    <<~JSON
      {
        "name": "#{example_name_one}",
        "link": "https://gitlab.com/gitlab-org/gitlab/-/blob/master#{file_path_one}",
        "failure_issue": "#{failure_issue_one}",
        "testcase": "https://gitlab.com/gitlab-org/gitlab/-/quality/test_cases/347880",
        "file_path": "#{file_path_one}"
      }
    JSON
  end

  let(:unstable_spec_two) do
    <<~JSON
      {
        "name": "#{example_name_two}",
        "link": "https://gitlab.com/gitlab-org/gitlab/-/blob/master#{file_path_two}",
        "failure_issue": "#{failure_issue_two}",
        "testcase": "https://gitlab.com/gitlab-org/gitlab/-/quality/test_cases/347880",
        "file_path": "#{file_path_two}"
      }
    JSON
  end

  let(:test_file_contents) do
    <<~RUBY
      # frozen_string_literal: true
      module QA
        RSpec.describe 'Govern', :smoke, :mobile, product_group: :authentication do
          describe 'basic user login' do
            it 'user logs in using basic credentials and logs out',
              testcase: 'https://gitlab.com/gitlab-org/gitlab/-/quality/test_cases/347880' do
              Flow::Login.sign_in
            end
          end
        end
      end
    RUBY
  end

  subject do
    described_class.new(
      token: test_token,
      project: test_project,
      specs_file: test_file_path_one,
      processor: add_to_blocking_processor,
      ref: ref,
      dry_run: false
    )
  end

  before do
    allow(File).to receive(:read).and_return(unstable_specs_file_contents)
  end

  describe '#invoke!' do
    it 'executes the processor' do
      expect(subject.__send__(:processor)).to receive(:create_commit).with(JSON.parse(unstable_spec_one), subject)
      expect(subject.__send__(:processor)).to receive(:create_commit).with(JSON.parse(unstable_spec_two), subject)
      expect(subject.__send__(:processor)).to receive(:create_merge_requests)
      expect(subject.__send__(:processor)).to receive(:post_process)
      subject.invoke!
    end
  end

  describe '#add_processed_commit' do
    let(:file_path) { '/path/to/file.rb' }
    let(:changed_line_no) { 42 }
    let(:branch) { { name: 'branch-name' } }
    let(:spec) { { foo: 'bar' } }

    it 'adds commit if file path does not exist yet' do
      expect do
        subject.add_processed_commit(file_path, changed_line_no, branch, spec)
      end.to change { subject.processed_commits[file_path] }.from(nil).to(
        commits: { '42' => spec },
        branch: branch
      )
    end

    context 'when file path already exists' do
      before do
        subject.add_processed_commit(file_path, changed_line_no, branch, spec)
      end

      it 'adds new commit if line number does not exist yet' do
        expect do
          subject.add_processed_commit(file_path, 43, branch, spec)
        end.to change { subject.processed_commits[file_path][:commits].count }.from(1).to(2)
      end

      it 'does not add commit if line number already exists' do
        expect do
          subject.add_processed_commit(file_path, changed_line_no, branch, spec)
        end.not_to change { subject.processed_commits[file_path][:commits].count }
      end
    end
  end

  describe '#commit_processed?' do
    let(:file_path) { '/path/to/file.rb' }

    context 'when there are no processed commits' do
      it 'returns false' do
        expect(subject.commit_processed?(file_path, 1)).to be_falsey
      end
    end

    context 'when there are processed commits for the file path' do
      let(:processed_commits) do
        {
          file_path => {
            commits: { '1' => {} }
          }
        }
      end

      before do
        allow(subject).to receive(:processed_commits).and_return(processed_commits)
      end

      it 'returns true if the line number has been processed' do
        expect(subject.commit_processed?(file_path, 1)).to be_truthy
      end

      it 'returns false if the line number has not been processed' do
        expect(subject.commit_processed?(file_path, 2)).to be_falsey
      end
    end
  end

  describe '#branch_for_file_path' do
    let(:file_path) { '/some/file/path' }

    before do
      allow(subject).to receive(:processed_commits)
                          .and_return(processed_commits)
    end

    context 'when processed_commits has an entry for the file path' do
      let(:branch) { { name: 'branch-name' } }
      let(:processed_commits) { { file_path => { branch: branch } } }

      it 'returns the branch' do
        expect(subject.branch_for_file_path(file_path)).to eq(branch)
      end
    end

    context 'when processed_commits does not have an entry for the file path' do
      let(:processed_commits) { {} }

      it 'returns nil' do
        expect(subject.branch_for_file_path(file_path)).to be_nil
      end
    end
  end

  describe '#get_file_contents' do
    let(:ref) { 'master' }
    let(:repository_files_client) do
      GitlabQuality::TestTooling::GitlabClient::RepositoryFilesClient.new(token: test_token, project: test_project, file_path: file_path_one, ref: ref)
    end

    before do
      allow(GitlabQuality::TestTooling::GitlabClient::RepositoryFilesClient).to receive(:new).and_return(repository_files_client)
    end

    it 'fetches files from repository' do
      expect(repository_files_client.__send__(:client)).to receive(:file_contents).and_return(test_file_contents)
      expect(subject.get_file_contents(file_path: file_path_one, branch: ref)).to eq(test_file_contents)
    end
  end

  describe '#find_example_match_lines' do
    context 'without shared_examples' do
      let(:matched_lines) do
        [["  RSpec.describe 'Govern', :smoke, :mobile, product_group: :authentication do", 2],
          ["    describe 'basic user login' do", 3],
          ["      it 'user logs in using basic credentials and logs out',", 4]]
      end

      it 'returns all lines that match the example' do
        expect(subject.find_example_match_lines(test_file_contents, example_name_one)).to eq(matched_lines)
      end
    end

    context 'with shared_examples' do
      let(:example_name_with_behaves_like) { 'Govern while LDAP is enabled behaves like registration and login allows the user to register and login' }
      let(:test_file_contents_with_shared_examples) do
        <<~RUBY
          # frozen_string_literal: true
          module QA
            RSpec.shared_examples 'registration and login' do
              it 'allows the user to register and login' do
                # ...
              end
            end
            RSpec.describe 'Govern', :skip_signup_disabled, :requires_admin, product_group: :authentication do
              describe 'while LDAP is enabled', :orchestrated, :ldap_no_tls,
                testcase: 'https://gitlab.com/gitlab-org/gitlab/-/quality/test_cases/347934' do

                it_behaves_like 'registration and login'
              end
            end
          end
        RUBY
      end

      let(:matched_lines) do
        [["  RSpec.describe 'Govern', :skip_signup_disabled, :requires_admin, product_group: :authentication do", 7],
          ["    describe 'while LDAP is enabled', :orchestrated, :ldap_no_tls,", 8]]
      end

      it 'does not return matched lines with it_behaves_like and shared_examples tokens' do
        expect(subject.find_example_match_lines(test_file_contents_with_shared_examples,
          example_name_with_behaves_like)).to eq(matched_lines)
      end
    end
  end

  describe '#update_matched_line' do
    let(:matched_line) do
      ["      it 'user logs in using basic credentials and logs out',", 4]
    end

    context 'when a block is not provided' do
      it 'returns the original content' do
        expect(subject.update_matched_line(matched_line, test_file_contents)).to eq([test_file_contents, 4])
      end
    end

    context 'when a block is provided' do
      let(:updated_test_file_contents) do
        <<~RUBY
          # frozen_string_literal: true
          module QA
            RSpec.describe 'Govern', :smoke, :mobile, product_group: :authentication do
              describe 'basic user login' do
                it 'user logs in using basic credentials and logs out', :reliable,
                  testcase: 'https://gitlab.com/gitlab-org/gitlab/-/quality/test_cases/347880' do
                  Flow::Login.sign_in
                end
              end
            end
          end
        RUBY
      end

      let(:block) do
        lambda { |line|
          modified_line = line.dup
          modified_line[line.index(',')] = ", :reliable,"

          modified_line
        }
      end

      it 'updates the content by evaluating the block' do
        expect(subject.update_matched_line(matched_line, test_file_contents.dup, &block)[0]).to eq(updated_test_file_contents)
      end
    end
  end

  describe '#create_branch' do
    let(:branches_client) { GitlabQuality::TestTooling::GitlabClient::BranchesClient.new(token: test_token, project: test_project) }

    before do
      allow(GitlabQuality::TestTooling::GitlabClient::BranchesClient).to receive(:new).and_return(branches_client)
    end

    it 'creates a branch' do
      expect(branches_client.__send__(:client)).to receive(:create_branch).with(test_project, branch_name_one, ref).and_return(branch_one)
      expect(subject.create_branch(branch_prefix, example_name_one, ref)).to eq(branch_one)
    end
  end

  describe '#commit_changes' do
    let(:commits_client) { GitlabQuality::TestTooling::GitlabClient::CommitsClient.new(token: test_token, project: test_project) }
    let(:commit) { 'commit' }
    let(:commit_message) { 'commit message' }

    before do
      allow(GitlabQuality::TestTooling::GitlabClient::CommitsClient).to receive(:new).and_return(commits_client)
    end

    it 'creates a commit' do
      expect(commits_client.__send__(:client)).to receive(:create_commit)
                                                    .with(test_project,
                                                      branch_one['name'],
                                                      commit_message, [
                                                        { action: :update, file_path: file_path_one, content: test_file_contents }
                                                      ]).and_return(commit)

      expect(subject.commit_changes(branch_one, commit_message, file_path_one, test_file_contents)).to eq(commit)
    end
  end

  describe '#create_merge_request' do
    let(:merge_request_client) { GitlabQuality::TestTooling::GitlabClient::MergeRequestsClient.new(token: test_token, project: test_project) }
    let(:created_merge_requests) { { 'web_url' => 'http://web.com/url', 'iid' => 123 } }
    let(:title) { "title_prefix #{example_name_one}" }
    let(:description) { 'Merge request description' }
    let(:description_block) { -> { description } }

    before do
      allow(GitlabQuality::TestTooling::GitlabClient::MergeRequestsClient).to receive(:new).and_return(merge_request_client)
    end

    it 'creates a merge request' do
      expect(merge_request_client.__send__(:client)).to receive(:create_merge_request)
                                                          .with(test_project,
                                                            title,
                                                            { source_branch: branch_one['name'],
                                                              target_branch: ref,
                                                              description: description,
                                                              labels: '',
                                                              reviewer_ids: [],
                                                              squash: true,
                                                              remove_source_branch: true }).and_return(created_merge_requests)

      expect(subject.create_merge_request(title, branch_one, &description_block)).to be_truthy
    end
  end

  describe '#post_note_on_issue' do
    let(:issue_client) { GitlabQuality::TestTooling::GitlabClient::IssuesClient.new(token: test_token, project: test_project) }
    let(:issue_iid) { '1234' }
    let(:failure_issue) { "https://gitlab.com/gitlab-org/gitlab/-/issues/#{issue_iid}" }

    before do
      allow(GitlabQuality::TestTooling::GitlabClient::IssuesClient).to receive(:new).and_return(issue_client)
    end

    context 'when report_issue is not nil' do
      before do
        allow(subject).to receive(:report_issue).and_return(failure_issue)
      end

      it 'posts note on issue' do
        expect(issue_client.__send__(:client)).to receive(:create_issue_note)
                                                    .with(test_project, issue_iid, 'note')
        subject.post_note_on_issue('note', subject.report_issue)
      end
    end

    context 'when report_issue is nil' do
      before do
        allow(subject).to receive(:report_issue).and_return(nil)
      end

      it 'does not posts note on issue' do
        expect(issue_client.__send__(:client)).not_to receive(:create_issue_note)

        subject.post_note_on_issue('note', subject.report_issue)
      end
    end
  end

  describe '#post_message_on_slack' do
    let(:post_to_slack) { instance_double(GitlabQuality::TestTooling::Slack::PostToSlack) }
    let(:slack_webhook_url) { 'http://slack.webhook.url' }
    let(:slack_options) do
      {
        slack_webhook_url: slack_webhook_url,
        channel: GitlabQuality::TestTooling::TestMeta::TestMetaUpdater::TEST_PLATFORM_MAINTAINERS_SLACK_CHANNEL_ID,
        username: "GitLab Quality Test Tooling",
        icon_emoji: ':warning:',
        message: message
      }
    end

    let(:message) { 'message posted to slack' }

    around do |example|
      ClimateControl.modify(CI_SLACK_WEBHOOK_URL: slack_webhook_url) { example.run }
    end

    before do
      allow(GitlabQuality::TestTooling::Slack::PostToSlack).to receive(:new).with(**slack_options).and_return(post_to_slack)
    end

    it 'posts message to slack' do
      expect(post_to_slack).to receive(:invoke!)
      subject.post_message_on_slack(message)
    end
  end

  describe '#issue_is_closed?' do
    let(:issue_client) { GitlabQuality::TestTooling::GitlabClient::IssuesClient.new(token: test_token, project: test_project) }
    let(:issue_iid) { 123 }
    let(:issue) { Gitlab::ObjectifiedHash.new(iid: issue_iid, state: state) }

    before do
      allow(GitlabQuality::TestTooling::GitlabClient::IssuesClient).to receive(:new).and_return(issue_client)
      allow(issue_client.__send__(:client)).to receive(:issue)
                                                 .with(test_project, issue_iid).and_return(issue)
    end

    context 'when issue is open' do
      let(:state) { 'opened' }

      it 'returns false' do
        expect(subject.issue_is_closed?(issue)).to be_falsy
      end
    end

    context 'when issue is closed' do
      let(:state) { 'closed' }

      it 'returns true' do
        expect(subject.issue_is_closed?(issue)).to be_truthy
      end
    end
  end

  describe '#issue_scoped_label' do
    let(:issue_client) { GitlabQuality::TestTooling::GitlabClient::IssuesClient.new(token: test_token, project: test_project) }
    let(:issue_iid) { 123 }
    let(:issue) { Gitlab::ObjectifiedHash.new(iid: issue_iid, labels: ['failure::investigating', 'devops::manage']) }

    it 'returns the scoped label' do
      expect(subject.issue_scoped_label(issue, 'failure')).to eq('failure::investigating')
    end

    it 'returns nil when no scoped label is found' do
      expect(subject.issue_scoped_label(issue, 'abc')).to be_nil
    end
  end

  describe '#indentation' do
    let(:leading_spaces) { " " * 6 }

    context 'when the first non space char on line is a quote' do
      let(:line) { "#{leading_spaces}'user logs in using basic credentials and logs out" }

      it 'indents by the same leading spaces as in the line' do
        expect(subject.indentation(line)).to eq(leading_spaces)
      end
    end

    context 'when the first non space char on line is not a quote' do
      let(:line) { "#{leading_spaces}it 'user logs in using basic credentials and logs out" }

      it 'indents by two additional spaces' do
        expect(subject.indentation(line)).to eq(leading_spaces + (" " * 2))
      end
    end
  end

  describe '#spec_desc_string_within_quotes' do
    context "when quoted string is preceded by 'context', 'describe' or 'it'" do
      where(:line, :match) do
        [
          ["RSpec.describe 'Govern', :reliable", "Govern"],
          ["it 'allows 2FA code recovery via ssh',", "allows 2FA code recovery via ssh"],
          ["context 'when latest parent pipeline failed' do', :reliable", "when latest parent pipeline failed"]
        ]
      end

      with_them do
        it "matches the description string" do
          expect(subject.send(:spec_desc_string_within_quotes, line)).to eq(match)
        end
      end
    end

    context "when quoted string is not preceded by 'context', 'describe' or 'it'" do
      where(:line, :match) do
        [
          ["'Govern', :reliable, describe", nil],
          ["it_behaves_like 'allows 2FA code recovery via ssh',", nil],
          ["shared_examples 'repository storage move'", nil]
        ]
      end

      with_them do
        it "does not match the description string" do
          expect(subject.send(:spec_desc_string_within_quotes, line)).to eq(match)
        end
      end
    end
  end

  describe '#existing_merge_requests' do
    let(:merge_request_client) { GitlabQuality::TestTooling::GitlabClient::MergeRequestsClient.new(token: test_token, project: test_project) }
    let(:title) { 'merge request title' }
    let(:gitlab_response) { instance_double(Gitlab::PaginatedResponse, auto_paginate: []) }

    before do
      allow(GitlabQuality::TestTooling::GitlabClient::MergeRequestsClient).to receive(:new).and_return(merge_request_client)
    end

    it 'calls #merge_requests GitLab method' do
      expect(merge_request_client.__send__(:client)).to receive(:merge_requests)
                                                          .with(test_project,
                                                            { in: 'title', search: 'merge request title', state: 'opened' })
                                                          .and_return(gitlab_response)

      subject.send(:existing_merge_requests, title: title)
    end
  end

  describe '#label_from_product_group' do
    before do
      stub_request(:get, 'https://about.gitlab.com/groups.json')
        .to_return(body:
          {
            source_code: { label: 'group::source code' }
          }.to_json)
    end

    context 'when the product group is found' do
      it 'returns the label for the product group' do
        expect(subject.label_from_product_group('source_code')).to eq('/label ~"group::source code"')
      end
    end

    context 'when the product group is not found' do
      it 'returns an empty string' do
        expect(subject.label_from_product_group('model_ops')).to eq('')
      end
    end
  end

  describe '#single_spec_metrics_link' do
    it 'returns encoded url' do
      expect(subject.single_spec_metrics_link('example name with spaces'))
        .to eq('https://dashboards.quality.gitlab.net/d/cW0UMgv7k/single-spec-metrics?orgId=1&var-run_type=All&var-name=example+name+with+spaces')
    end
  end

  describe '#quarantined?' do
    let(:matched_lines) do
      [["  RSpec.describe 'Govern', :smoke, :mobile, product_group: :authentication do", 2],
        ["    describe 'basic user login' do", 3],
        ["      it 'user logs in using basic credentials and logs out',", 4]]
    end

    context 'when test is not quarantined' do
      it 'returns false' do
        expect(subject.quarantined?(matched_lines, test_file_contents))
          .to be_falsy
      end
    end

    context 'when test is quarantined' do
      shared_examples 'quarantine' do
        it 'returns true' do
          expect(subject.quarantined?(matched_lines, test_file_contents))
            .to be_truthy
        end
      end

      context 'with quarantine tag on same line as match' do
        let(:test_file_contents) do
          <<~RUBY
            # frozen_string_literal: true
            module QA
              RSpec.describe 'Govern', :smoke, :mobile, product_group: :authentication , quarantine: {
                  only: { job: 'relative-url' },
                  issue: 'https://gitlab.com/gitlab-org/gitlab/-/issues/409541',
                  type: :bug
                } do
                describe 'basic user login' do
                  it 'user logs in using basic credentials and logs out',
                    testcase: 'https://gitlab.com/gitlab-org/gitlab/-/quality/test_cases/347880'
                    Flow::Login.sign_in
                  end
                end
              end
            end
          RUBY
        end

        it_behaves_like 'quarantine'
      end

      context 'with quarantine tag two lines down' do
        let(:test_file_contents) do
          <<~RUBY
            # frozen_string_literal: true
            module QA
              RSpec.describe 'Govern', :smoke, :mobile, product_group: :authentication do
                describe 'basic user login' do
                  it 'user logs in using basic credentials and logs out',
                    testcase: 'https://gitlab.com/gitlab-org/gitlab/-/quality/test_cases/347880',
                    quarantine: {
                      only: { job: 'relative-url' },
                      issue: 'https://gitlab.com/gitlab-org/gitlab/-/issues/409541',
                      type: :bug
                    } do
                    Flow::Login.sign_in
                  end
                end
              end
            end
          RUBY
        end

        it_behaves_like 'quarantine'
      end
    end
  end
end
