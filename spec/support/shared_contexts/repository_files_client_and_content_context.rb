# frozen_string_literal: true

RSpec.shared_context 'with repository files client and content' do
  let(:client) { instance_double(Gitlab::Client) }
  let(:repository_file_content) { '' }
  let(:repository_file_line_content) { '' }

  before do
    allow(
      GitlabQuality::TestTooling::GitlabClient::RepositoryFilesClient
    ).to receive_message_chain(:new, :file_contents).and_return(repository_file_content)

    allow(
      GitlabQuality::TestTooling::GitlabClient::RepositoryFilesClient
    ).to receive_message_chain(:new, :file_contents_at_line).and_return(repository_file_line_content)
  end
end
