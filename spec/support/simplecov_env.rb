# frozen_string_literal: true

require 'simplecov'
require 'simplecov-cobertura'

module SimpleCovEnv
  extend self

  def start!
    if SimpleCov.running
      puts "Simplecov is already running!"
      return
    end

    configure_profile
    configure_formatters

    SimpleCov.start
  end

  def configure_profile
    SimpleCov.configure do
      load_profile 'test_frameworks'
    end
  end

  def configure_formatters
    SimpleCov.formatters = SimpleCov::Formatter::MultiFormatter.new(
      [
        SimpleCov::Formatter::SimpleFormatter,
        SimpleCov::Formatter::HTMLFormatter,
        SimpleCov::Formatter::CoberturaFormatter
      ]
    )
  end
end
