# frozen_string_literal: true

module GitlabQuality
  module TestTooling
    VERSION = "2.8.0"
  end
end
