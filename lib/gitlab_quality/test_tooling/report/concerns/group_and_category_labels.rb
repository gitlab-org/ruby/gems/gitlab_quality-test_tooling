# frozen_string_literal: true

module GitlabQuality
  module TestTooling
    module Report
      module Concerns
        module GroupAndCategoryLabels
          def group_and_category_labels_for_test(test)
            labels_inference = GitlabQuality::TestTooling::LabelsInference.new
            new_labels       = Set.new

            new_labels += labels_inference.infer_labels_from_product_group(test.product_group) if test.respond_to?(:product_group)
            new_labels += labels_inference.infer_labels_from_feature_category(test.feature_category) if test.respond_to?(:feature_category)

            new_labels
          end
        end
      end
    end
  end
end
