## What does this MR do and why?

_Describe in detail what your merge request does and why._

<!--
Please keep this description updated with any discussion that takes place so
that reviewers can understand your intent. Keeping the description updated is
especially important if they didn't participate in the discussion.
-->

## Screenshots or screen recordings

_These are strongly recommended to assist reviewers and reduce the time to merge your change._

## How to set up and validate locally

_Numbered steps to set up and validate the change are strongly suggested._

## MR acceptance checklist

This checklist encourages us to confirm any changes have been analyzed to reduce risks in quality, performance, reliability, security, and maintainability.

* [ ] I have evaluated the [MR acceptance checklist](https://docs.gitlab.com/ee/development/code_review.html#acceptance-checklist) for this MR.

/label ~"type::maintenance" ~Quality

<!-- template sourced from https://gitlab.com/gitlab-org/ruby/gems/gitlab_quality-test_tooling/-/blob/master/.gitlab/merge_request_templates/Default.md -->
